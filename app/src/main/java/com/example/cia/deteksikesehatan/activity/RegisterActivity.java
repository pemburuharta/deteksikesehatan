package com.example.cia.deteksikesehatan.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.MainActivity;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class RegisterActivity extends AppCompatActivity {

    private ImageView img;
    private LinearLayout ly;
    private EditText registerEdtUsername;
    private EditText registerEdtFullname;
    private EditText registerEdtEmail;
    private EditText registerEdtTelpon;
    private EditText registerEdtTempatlahir;
    private LinearLayout registerDivTanggal;
    private TextView registerTvTanggalLahir;
    private EditText registerEdtPassword;
    private Button registerBtnDaftar;
    private TextView btnPunyaAkun;

    Calendar myCalendar;
    ProgressDialog pd;
    SharedPref pref;

    String TanggalLahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();
        initView();

        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");

        registerDivTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        myCalendar.set(Calendar.YEAR,i);
                        myCalendar.set(Calendar.MONTH,i1);
                        myCalendar.set(Calendar.DAY_OF_MONTH,i2);

                        String fromTanggal = "yyyy-MM-dd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                        registerTvTanggalLahir.setText(dateFormat.format(myCalendar.getTime()));

                        TanggalLahir = dateFormat.format(myCalendar.getTime());
                    }
                },
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnPunyaAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        registerBtnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                register();
            }
        });

    }

    private void register() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.register(registerEdtUsername.getText().toString(),
                registerEdtFullname.getText().toString(),
                registerEdtEmail.getText().toString(),
                registerEdtTelpon.getText().toString(),
                registerEdtTempatlahir.getText().toString(),
                TanggalLahir,
                registerEdtPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");

                        if (error.equals("false")){
                            String id = jsonObject.optString("userId");

                            pref.savePrefBoolean(SharedPref.STATUS_LOGIN, true);
                            pref.savePrefString(SharedPref.ID_USER, id);
                            pref.savePrefString(SharedPref.NAME, registerEdtUsername.getText().toString());
                            pref.savePrefString(SharedPref.FULLNAME, registerEdtUsername.getText().toString());
                            pref.savePrefString(SharedPref.EMAIL, registerEdtEmail.getText().toString());
                            pref.savePrefString(SharedPref.NOTELP, registerEdtTelpon.getText().toString());
                            pref.savePrefString(SharedPref.TEMPAT_LAHIR, registerEdtTempatlahir.getText().toString());
                            pref.savePrefString(SharedPref.TANGGAL_LAHIR, TanggalLahir);
                            pref.savePrefString(SharedPref.RULE, "USER");

                            Intent intent = new Intent(getApplicationContext(), BiodataActivity.class);
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

                        } else {

                            String msg = jsonObject.optString("msg");

                            Toast.makeText(RegisterActivity.this, ""+msg, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(RegisterActivity.this, "Periksa Koneksi Anda ...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        img = findViewById(R.id.img);
        ly = findViewById(R.id.ly);
        registerEdtUsername = findViewById(R.id.register_edt_username);
        registerEdtFullname = findViewById(R.id.register_edt_fullname);
        registerEdtEmail = findViewById(R.id.register_edt_email);
        registerEdtTelpon = findViewById(R.id.register_edt_telpon);
        registerEdtTempatlahir = findViewById(R.id.register_edt_tempatlahir);
        registerDivTanggal = findViewById(R.id.register_div_tanggal);
        registerTvTanggalLahir = findViewById(R.id.register_tv_tanggal_lahir);
        registerEdtPassword = findViewById(R.id.register_edt_password);
        registerBtnDaftar = findViewById(R.id.register_btn_daftar);
        btnPunyaAkun = findViewById(R.id.btn_punya_akun);
    }
}
