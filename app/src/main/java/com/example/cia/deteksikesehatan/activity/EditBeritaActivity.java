package com.example.cia.deteksikesehatan.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.fragment.AddBeritaFragment;
import com.example.cia.deteksikesehatan.fragment.AddVideoFragment;

public class EditBeritaActivity extends AppCompatActivity {


    private ImageView addberitaIvClose;
    private TextView addberitaTvActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_berita);
        getSupportActionBar().hide();
        initView();

        addberitaIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().getStringExtra("status").equals("EDIT_BERITA")){

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_editberita, new AddBeritaFragment())
                    .commit();

        } else {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_editberita, new AddVideoFragment())
                    .commit();

        }


    }

    private void initView() {
        addberitaIvClose = findViewById(R.id.addberita_iv_close);
        addberitaTvActionbar = findViewById(R.id.addberita_tv_actionbar);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        final AlertDialog.Builder dialog = new AlertDialog.Builder(EditBeritaActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Apakah anda sudah menyimpan perubahan ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });

        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
    }
}
