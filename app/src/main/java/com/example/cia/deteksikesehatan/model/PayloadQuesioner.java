package com.example.cia.deteksikesehatan.model;

import com.example.cia.deteksikesehatan.room.Quesioner;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PayloadQuesioner {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("payload")
    @Expose
    private List<QuesionerModel> payload = null;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<QuesionerModel> getPayload() {
        return payload;
    }

    public void setPayload(List<QuesionerModel> payload) {
        this.payload = payload;
    }
}
