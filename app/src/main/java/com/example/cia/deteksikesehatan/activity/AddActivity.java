package com.example.cia.deteksikesehatan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.R;

public class AddActivity extends AppCompatActivity {

    private ImageView addIvClose;
    private TextView beritaTvTitle;
    private CardView addCvKategori;
    private CardView addCvMateri;
    private CardView addCvSoal;
    private CardView addCvVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        getSupportActionBar().hide();

        initView();

        addIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });

        addCvKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahKategoriActivity.class);
                intent.putExtra("value", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        addCvMateri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahMateriActivity.class);
                intent.putExtra("status", "ADD");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        addCvVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahVideoActivity.class);
                intent.putExtra("status", "ADD_VIDEO");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        addCvSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TambahSoalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    private void initView() {
        addIvClose = findViewById(R.id.add_iv_close);
        beritaTvTitle = findViewById(R.id.berita_tv_title);
        addCvKategori = findViewById(R.id.add_cv_kategori);
        addCvMateri = findViewById(R.id.add_cv_materi);
        addCvSoal = findViewById(R.id.add_cv_soal);
        addCvVideo = findViewById(R.id.add_cv_video);
    }
}
