package com.example.cia.deteksikesehatan.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.BeritaActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddVideoFragment extends Fragment {


    private EditText addvideoEdtNama;
    private EditText addvideoEdtLink;
    private LinearLayout addvideoDivSimpan;

    SharedPref pref;
    ProgressDialog pd;

    String idVideo;

    public AddVideoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_video, container, false);
        initView(view);

        pref = new SharedPref(getActivity());
        pd = new ProgressDialog(getActivity());

        if (getActivity().getIntent().getStringExtra("status").equals("ADD_BERITA")){

            addvideoDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addVideo();
                }
            });

        } else {

            addvideoEdtNama.setText(getActivity().getIntent().getStringExtra("judul"));
            addvideoEdtLink.setText(getActivity().getIntent().getStringExtra("file"));
            idVideo = getActivity().getIntent().getStringExtra("id_video");

            addvideoDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editVideo();
                }
            });

        }


        return view;
    }

    private void editVideo() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.editVideoApi("edit",
                idVideo,
                addvideoEdtNama.getText().toString(),
                addvideoEdtLink.getText().toString(),
                pref.getIdUser()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){

                            Toast.makeText(getActivity(), "Berhasil Menambahkan Video", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            Toast.makeText(getActivity(), "Gagal Menambahkan Video", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addVideo() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.videoApi("add",
                addvideoEdtNama.getText().toString(),
                addvideoEdtLink.getText().toString(),
                pref.getIdUser()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            
                            Toast.makeText(getActivity(), "Berhasil Menambahkan Video", Toast.LENGTH_SHORT).show();Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();

                        } else {
                            Toast.makeText(getActivity(), "Gagal Menambahkan Video", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView(View view) {
        addvideoEdtNama = view.findViewById(R.id.addvideo_edt_nama);
        addvideoEdtLink = view.findViewById(R.id.addvideo_edt_link);
        addvideoDivSimpan = view.findViewById(R.id.addvideo_div_simpan);
    }
}
