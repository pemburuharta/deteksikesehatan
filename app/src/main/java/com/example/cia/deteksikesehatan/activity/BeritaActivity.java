package com.example.cia.deteksikesehatan.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.fragment.KategoriFragment;
import com.example.cia.deteksikesehatan.fragment.VideoFragment;

import java.util.ArrayList;
import java.util.List;

public class BeritaActivity extends AppCompatActivity {

    private ImageView beritaIvClose;
    private TabLayout beritaTablayout;
    private ViewPager beritaViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_berita);
        getSupportActionBar().hide();
        initView();

        setupViewPager(beritaViewPager);
        beritaTablayout.setupWithViewPager(beritaViewPager);

        beritaIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void setupViewPager(ViewPager beritaViewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new KategoriFragment(), "Berita");
        adapter.addFragment(new VideoFragment(), "Video");
        beritaViewPager.setAdapter(adapter);
    }

    private void initView() {
        beritaIvClose = findViewById(R.id.berita_iv_close);
        beritaTablayout = findViewById(R.id.berita_tablayout);
        beritaViewPager = findViewById(R.id.berita_viewPager);
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public void addFragment(Fragment fragment, String detail) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(detail);
        }

    }
}
