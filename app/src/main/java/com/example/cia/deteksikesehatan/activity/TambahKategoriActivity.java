package com.example.cia.deteksikesehatan.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahKategoriActivity extends AppCompatActivity {

    private ImageView addkategoriIvClose;
    private TextView addkategoriTvActionbar;
    private EditText addkategoriEdtNama;
    private EditText addkategoriEdtDeskripsi;
    private ImageView addkategoriIvThumbnail;
    private TextView addkategoriTvThumbnail;
    private LinearLayout addkategoriDivSimpan;

    File imageGambar;
    SharedPref pref;
    ProgressDialog pd;

    String STATUS;
    String NamaFoto;
    String idKategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_kategori);
        getSupportActionBar().hide();
        initView();

        STATUS = getIntent().getStringExtra("value");
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        addkategoriIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        addkategoriIvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(TambahKategoriActivity.this, 3);
            }
        });

        addkategoriTvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(TambahKategoriActivity.this, 3);
            }
        });

        if (STATUS.equals("ADD")){

            addkategoriDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addKategori();
                }
            });

        } else {

            addkategoriEdtNama.setText(getIntent().getStringExtra("judul"));
            addkategoriEdtDeskripsi.setText(getIntent().getStringExtra("isi"));
            NamaFoto = getIntent().getStringExtra("gambar");
            idKategori = getIntent().getStringExtra("id_kategori");

            Picasso.with(this)
                    .load("http://dk.mitraredex.com/assets/gambar/"+getIntent().getStringExtra("gambar"))
                    .into(addkategoriIvThumbnail);

            addkategoriDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editKategori();
                }
            });
        }


    }

    private void editKategori() {

        pd.show();

        APIService apiService =APIClient.getRetrofit().create(APIService.class);
        apiService.editKategori(idKategori,
                addkategoriEdtNama.getText().toString(),
                addkategoriEdtDeskripsi.getText().toString(),
                NamaFoto).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            onBackPressed();
                            Toast.makeText(getApplicationContext(), "Kategori Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(TambahKategoriActivity.this, ""+error, Toast.LENGTH_SHORT).show();
                            Toast.makeText(getApplicationContext(), "Kategori Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addKategori() {

        pd.show();

        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageGambar);
        MultipartBody.Part bodyGambar = MultipartBody.Part.createFormData("image", imageGambar.getName(), requestImage);

        RequestBody nama_kategori = RequestBody.create(MediaType.parse("text/plain"), addkategoriEdtNama.getText().toString());
        RequestBody deskripsi_kategori = RequestBody.create(MediaType.parse("text/plain"), addkategoriEdtDeskripsi.getText().toString());
        RequestBody id_user = RequestBody.create(MediaType.parse("text/plain"), pref.getIdUser());

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.addKategori(nama_kategori,
                deskripsi_kategori,
                id_user,
                bodyGambar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");

                        if (error.equals("false")){
                            Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            Toast.makeText(TambahKategoriActivity.this, "Berhasil Menambahkan Kategori", Toast.LENGTH_SHORT).show();
                        } else {

                            String msg = jsonObject.optString("msg");
                            Toast.makeText(TambahKategoriActivity.this, ""+msg, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahKategoriActivity.this, "Periksa Koneksi Anda ...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                if (STATUS.equals("ADD")){
                    Glide.with(TambahKategoriActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(addkategoriIvThumbnail);
                    imageGambar = new File(imageFile.getPath());
                } else {
                    Glide.with(TambahKategoriActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(addkategoriIvThumbnail);
                    imageGambar = new File(imageFile.getPath());
                    uploadGambar();
                }
            }

        });

    }

    private void uploadGambar() {

        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageGambar);
        MultipartBody.Part bodyGambar = MultipartBody.Part.createFormData("image", imageGambar.getName(), requestImage);

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.materiUploadGambar(bodyGambar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            NamaFoto = jsonObject.optString("image_name");
                            Toast.makeText(getApplicationContext(), "Gambar Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Gambar Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        addkategoriIvClose = findViewById(R.id.addkategori_iv_close);
        addkategoriTvActionbar = findViewById(R.id.addkategori_tv_actionbar);
        addkategoriEdtNama = findViewById(R.id.addkategori_edt_nama);
        addkategoriEdtDeskripsi = findViewById(R.id.addkategori_edt_deskripsi);
        addkategoriIvThumbnail = findViewById(R.id.addkategori_iv_thumbnail);
        addkategoriTvThumbnail = findViewById(R.id.addkategori_tv_thumbnail);
        addkategoriDivSimpan = findViewById(R.id.addkategori_div_simpan);
    }
}
