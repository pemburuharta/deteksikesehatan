package com.example.cia.deteksikesehatan.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.fragment.AkunFragment;
import com.example.cia.deteksikesehatan.fragment.KategoriFragment;
import com.example.cia.deteksikesehatan.fragment.VideoFragment;

public class MainUserActivity extends AppCompatActivity {

    private TextView mTextMessage;
    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, new KategoriFragment())
                            .commit();
                    getSupportActionBar().setTitle("Berita");
                    return true;
                case R.id.navigation_dashboard:
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, new VideoFragment())
                            .commit();
                    getSupportActionBar().setTitle("Video");
                    return true;
                case R.id.navigation_notifications:
                    fragmentManager.beginTransaction()
                            .replace(R.id.frame, new AkunFragment())
                            .commit();
                    getSupportActionBar().setTitle("Akun");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        fragmentManager.beginTransaction()
                .replace(R.id.frame, new KategoriFragment())
                .commit();
        getSupportActionBar().setTitle("Berita");
    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainUserActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Apakah anda yakin ingin keluar ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
            }
        });

        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
    }
}
