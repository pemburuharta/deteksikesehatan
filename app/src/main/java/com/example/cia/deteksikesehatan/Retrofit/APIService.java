package com.example.cia.deteksikesehatan.Retrofit;

import com.example.cia.deteksikesehatan.model.PayLoadListUser;
import com.example.cia.deteksikesehatan.model.PayLoadSoalModel;
import com.example.cia.deteksikesehatan.model.PayloadQuesioner;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {

    @FormUrlEncoded
    @POST("login-api")
    Call<ResponseBody> login(
            @Field("loginName") String loginName,
            @Field("loginPassword") String loginPassword);

    @FormUrlEncoded
    @POST("register-api")
    Call<ResponseBody> register(
            @Field("username") String username,
            @Field("fullname") String fullname,
            @Field("email") String email,
            @Field("telp") String telp,
            @Field("tempat") String tempat,
            @Field("tglLahir") String tglLahir,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("profile-api?acces=biodata")
    Call<ResponseBody> biodata(
            @Query("value") String id_user,
            @Field("fullname") String fullname,
            @Field("noTelp") String noTelp,
            @Field("email") String email,
            @Field("umur") String umur,
            @Field("tempat") String tempat,
            @Field("tglLahir") String tglLahir,
            @Field("kota") String kota,
            @Field("kecamatan") String kecamatan,
            @Field("kelurahan") String kelurahan,
            @Field("rt") String rt,
            @Field("rw") String rw,
            @Field("sekolah") String sekolah,
            @Field("kelas") String kelas,
            @Field("pndk_ayah") String pndk_ayah,
            @Field("prkj_ayah") String prkj_ayah,
            @Field("pndk_ibu") String pndk_ibu,
            @Field("prkj_ibu") String prkj_ibu,
            @Field("pendapatan") String pendapatan,
            @Field("kegiatan[]") List<String> kegiatan,
            @Field("apps[]") List<String> apps,
            @Field("event[]") List<String> event,
            @Field("inform[]") List<String> inform,
            @Field("turunan[]") List<String> turunan,
            @Field("periksa[]") List<String> periksa,
            @Field("perokok[]") List<String> perokok,
            @Field("lingkungan[]") List<String> lingkungan,
            @Field("miras[]") List<String> miras,
            @Field("olahraga[]") List<String> olahraga,
            @Field("snack[]") List<String> snack,
            @Field("softdrink[]") List<String> softdrink,
            @Field("tidur[]") List<String> tidur,
            @Field("dekat[]") List<String> dekat,
            @Field("berat") String berat,
            @Field("tinggi") String tinggi,
            @Field("lingkar") String lingkar,
            @Field("sistolik") String sistolik,
            @Field("distolik") String distolik,
            @Field("gula") String gula);

    @FormUrlEncoded
    @POST("quisioner?acces=quisioner")
    Call<ResponseBody> posQuesioner(
            @Query("value") String value,
            @Field("asap") String asap,
            @Field("perokok") String perokok,
            @Field("alkohol") String alkohol,
            @Field("aktifitas") String aktifitas,
            @Field("olahraga") String olahraga,
            @Field("minum") String minum,
            @Field("makan[]") List<String> makan,
            @Field("lauk[]") List<String> lauk,
            @Field("buah[]") List<String> buah,
            @Field("sayur[]") List<String> sayur,
            @Field("tidur") String tidur,
            @Field("stress") String stress);



    @Multipart
    @POST("materi-api?acces=addkategori")
    Call<ResponseBody> addKategori(
            @Part("judul") RequestBody judul,
            @Part("subject") RequestBody isi,
            @Part("created") RequestBody createdBy,
            @Part MultipartBody.Part gambar
    );

    @FormUrlEncoded
    @POST("materi-api?acces=editkategori")
    Call<ResponseBody> editKategori(
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("isi") String isi,
            @Field("image") String gambar);

    @FormUrlEncoded
    @POST("quisioner?acces=perhari")
    Call<ResponseBody> historiPerHari(
            @Field("tanggal") String tanggal,
            @Field("userId") String userId);

    @FormUrlEncoded
    @POST("quisioner?acces=perminggu")
    Call<ResponseBody> historiPerMinggu(
            @Field("tanggalAwal") String tanggal1,
            @Field("tanggalAkhir") String tanggal2,
            @Field("userId") String userId);

    @FormUrlEncoded
    @POST("quisioner?acces=perbulan")
    Call<ResponseBody> historiPerBulan(
            @Field("bulan") String bulan,
            @Field("tahun") String tahun,
            @Field("userId") String userId);



    @FormUrlEncoded
    @POST("quisioner?acces=perhari")
    Call<PayloadQuesioner> qPerHari(
            @Field("tanggal") String tanggal,
            @Field("userId") String userId);

    @FormUrlEncoded
    @POST("quisioner?acces=perminggu")
    Call<PayloadQuesioner> qPerMinggu(
            @Field("tanggalAwal") String tanggal1,
            @Field("tanggalAkhir") String tanggal2,
            @Field("userId") String userId);

    @FormUrlEncoded
    @POST("quisioner?acces=perbulan")
    Call<PayloadQuesioner> qPerBulan(
            @Field("bulan") String bulan,
            @Field("tahun") String tahun,
            @Field("userId") String userId);

    @Multipart
    @POST("materi-api?acces=addmateri")
    Call<ResponseBody> addMateri(
            @Part("judul") RequestBody judul,
            @Part("subject") RequestBody isi,
            @Part("created") RequestBody createdBy,
            @Part("katId") RequestBody kategori,
            @Part MultipartBody.Part gambar
    );

    @Multipart
    @POST("materi-api?acces=upload")
    Call<ResponseBody> materiUploadGambar(
            @Part MultipartBody.Part gambar
    );

    @GET("materi-api")
    Call<ResponseBody> deleteKategoriMateri(
            @Query("acces") String acces,
            @Query("value") String value);


    @FormUrlEncoded
    @POST("materi-api?acces=editmateri")
    Call<ResponseBody> editMateri(
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("subject") String isi,
            @Field("image") String gambar);

    @FormUrlEncoded
    @POST("soal-api?acces=addsoal")
    Call<ResponseBody> addSoal(
            @Field("kategoriId") String kategoriId,
            @Field("materiId") String materiId,
            @Field("soal") String soal,
            @Field("jawab_a") String jawab_a,
            @Field("jawab_b") String jawab_b,
            @Field("jawab_c") String jawab_c,
            @Field("jawab_d") String jawab_d,
            @Field("jawabanbenar") String jawabanbenar,
            @Field("created") String createdBy);

    @GET("soal-api")
    Call<ResponseBody> deleteSoal(
            @Query("acces") String acces,
            @Query("value") String value);

    @FormUrlEncoded
    @POST("video-api")
    Call<ResponseBody> videoApi(
            @Query("acces") String acces,
            @Field("judul") String judul,
            @Field("embed") String embed,
            @Field("createdBy") String createdBy);

    @FormUrlEncoded
    @POST("jawab-api")
    Call<ResponseBody> jawabSoal(
            @Query("acces") String acces,
            @Query("userId") String id,
            @Query("katId") String katId,
            @Query("materiId") String materiId,
            @Field("pilihan[]") List<String> pilih,
            @Field("soalId[]") List<String> soalId);


    @FormUrlEncoded
    @POST("video-api")
    Call<ResponseBody> editVideoApi(
            @Query("acces") String acces,
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("embed") String embed,
            @Field("createdBy") String createdBy);

    @GET("video-api")
    Call<ResponseBody> deleteVideo(
            @Query("acces") String acces,
            @Query("value") String value);

    @Multipart
    @POST("news-api?acces=add")
    Call<ResponseBody> newsApi(
            @Part("judul") RequestBody judul,
            @Part("isi") RequestBody isi,
            @Part("createdBy") RequestBody createdBy,
            @Part MultipartBody.Part gambar
    );

    @Multipart
    @POST("news-api?acces=upload")
    Call<ResponseBody> newsUploadGambar(
            @Part MultipartBody.Part gambar
    );

    @FormUrlEncoded
    @POST("news-api?acces=edit")
    Call<ResponseBody> editNews(
            @Query("value") String value,
            @Field("judul") String judul,
            @Field("isi") String isi,
            @Field("image") String gambar);

    //   API GET
    @GET("news-api")
    Call<ResponseBody> deleteBerita(
            @Query("acces") String acces,
            @Query("value") String value);

    @GET("level-api")
    Call<ResponseBody> cekLevel(
            @Query("userId") String idU,
            @Query("katId") String katId);


    @GET("materi-api?acces=kategori")
    Call<ResponseBody> getKategori();

    @GET("materi-api?acces=materi")
    Call<ResponseBody> getMateri(
            @Query("value") String value
    );

    @GET("video-api")
    Call<ResponseBody> getVideo();

    @GET("soal-api?acces=soal")
    Call<PayLoadSoalModel> getSoal(@Query("value") String kategori,
                                   @Query("key") String materi);

    @GET("soal-api?acces=soalkat")
    Call<PayLoadSoalModel> getSoalDua(@Query("value") String kategori);


    @GET("biodata-api?acces=user")
    Call<PayLoadListUser> getUser();

}
