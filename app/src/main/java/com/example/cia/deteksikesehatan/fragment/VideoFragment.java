package com.example.cia.deteksikesehatan.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.BeritaActivity;
import com.example.cia.deteksikesehatan.activity.EditBeritaActivity;
import com.example.cia.deteksikesehatan.activity.TambahVideoActivity;
import com.jaedongchicken.ytplayer.YoutubePlayerView;
import com.jaedongchicken.ytplayer.model.PlaybackQuality;
import com.jaedongchicken.ytplayer.model.YTParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {


    private SwipeRefreshLayout swipe;
    private LinearLayout div;

    SharedPref pref;
    ProgressDialog pd;
    String fileAsli;

    public VideoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        initView(view);

        pref = new SharedPref(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        getData(true);
        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
        return view;
    }

    private void getData(Boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }

        swipe.setRefreshing(true);
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getVideo().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("Data List Video")){
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String judul = jsonObject1.optString("judul");
                                final String idVideo = jsonObject1.optString("bigId");
                                String file = jsonObject1.optString("embed_video");

                                if (file.contains("https://www.youtube.com/embed/")){
                                    fileAsli = file.replace("https://www.youtube.com/embed/","");
                                }

                                LayoutInflater layoutInflater =(LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_video,null);

                                final YoutubePlayerView video = view.findViewById(R.id.youtubePlayerView);
                                final TextView tvJudul = view.findViewById(R.id.tv_judul);
                                final ImageView imgFullScreen = view.findViewById(R.id.img_full);

                                tvJudul.setText(""+judul);

                                final String fileYoutube = fileAsli;
                                final String idFile = idVideo;

                                YTParams params = new YTParams();
                                params.setPlaybackQuality(PlaybackQuality.small);
                                params.setControls(1);
                                params.setShowinfo(0);
                                video.setAutoPlayerHeight(getActivity());
                                // initialize YoutubePlayerCallBackListener and VideoID
                                video.initialize(""+fileAsli,params, new YoutubePlayerView.YouTubeListener() {
                                    @Override
                                    public void onReady() {

                                    }

                                    @Override
                                    public void onStateChange(YoutubePlayerView.STATE state) {

                                    }

                                    @Override
                                    public void onPlaybackQualityChange(String arg) {

                                    }

                                    @Override
                                    public void onPlaybackRateChange(String arg) {

                                    }

                                    @Override
                                    public void onError(String arg) {

                                    }

                                    @Override
                                    public void onApiChange(String arg) {

                                    }

                                    @Override
                                    public void onCurrentSecond(double second) {
//                Toast.makeText(context, ""+second, Toast.LENGTH_SHORT).show();
                                    }

                                    @Override
                                    public void onDuration(double duration) {

                                    }

                                    @Override
                                    public void logs(String log) {

                                    }
                                });

                                if (pref.getRule().equals("ADMIN")){
                                    imgFullScreen.setVisibility(View.VISIBLE);
                                    imgFullScreen.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(final View view) {
                                            final CharSequence[] dialogItem = {"Edit", "Hapus"};
                                            final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                            builder.setTitle("Tentukan Pilihan Anda");
                                            builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    switch (i) {
                                                        case 0 :
//                                                            Toast.makeText(getActivity(), ""+judul+fileAsli, Toast.LENGTH_SHORT).show();
                                                            Intent intent = new Intent(getActivity(), TambahVideoActivity.class);
                                                            intent.putExtra("status", "EDIT_VIDEO");
                                                            intent.putExtra("judul", judul);
                                                            intent.putExtra("id_video", idFile);
                                                            intent.putExtra("file", fileYoutube);
                                                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent);

                                                            break;
                                                        case 1 :

                                                            new AlertDialog.Builder(view.getContext())
                                                                    .setMessage("Apakah anda akan menghapus berita ini ?")
                                                                    .setCancelable(false)
                                                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                                                        public void onClick(DialogInterface dialog, int id) {
                                                                            HapusVideo(idVideo);
                                                                        }
                                                                    })
                                                                    .setNegativeButton("Tidak", null)
                                                                    .show();
                                                            break;
                                                    }
                                                }
                                            });
                                            builder.create().show();
                                        }
                                    });
                                }

                                div.addView(view);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void HapusVideo(String idVideo) {

        pd.dismiss();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.deleteVideo("delete",
                idVideo).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Video Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Video Gagal Dihapus", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView(View view) {
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        div = (LinearLayout) view.findViewById(R.id.div);
    }
}
