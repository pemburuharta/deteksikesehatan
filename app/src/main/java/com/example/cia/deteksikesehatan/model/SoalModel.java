package com.example.cia.deteksikesehatan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SoalModel {
    @SerializedName("TQ_BIGID")
    @Expose
    private String tQBIGID;
    @SerializedName("TQ_KATID")
    @Expose
    private String tQKATID;
    @SerializedName("TQ_SUBKATID")
    @Expose
    private String tQSUBKATID;
    @SerializedName("TQ_SOAL")
    @Expose
    private String tQSOAL;
    @SerializedName("TQ_A")
    @Expose
    private String tQA;
    @SerializedName("TQ_B")
    @Expose
    private String tQB;
    @SerializedName("TQ_C")
    @Expose
    private String tQC;
    @SerializedName("TQ_D")
    @Expose
    private String tQD;
    @SerializedName("TQ_TRUE")
    @Expose
    private String tQTRUE;
    @SerializedName("TQ_STATUS")
    @Expose
    private String tQSTATUS;
    @SerializedName("TN_CREATED_BY")
    @Expose
    private String tNCREATEDBY;
    @SerializedName("TQ_CREATED_AT")
    @Expose
    private String tQCREATEDAT;

    public String getTQBIGID() {
        return tQBIGID;
    }

    public void setTQBIGID(String tQBIGID) {
        this.tQBIGID = tQBIGID;
    }

    public String getTQKATID() {
        return tQKATID;
    }

    public void setTQKATID(String tQKATID) {
        this.tQKATID = tQKATID;
    }

    public String getTQSUBKATID() {
        return tQSUBKATID;
    }

    public void setTQSUBKATID(String tQSUBKATID) {
        this.tQSUBKATID = tQSUBKATID;
    }

    public String getTQSOAL() {
        return tQSOAL;
    }

    public void setTQSOAL(String tQSOAL) {
        this.tQSOAL = tQSOAL;
    }

    public String getTQA() {
        return tQA;
    }

    public void setTQA(String tQA) {
        this.tQA = tQA;
    }

    public String getTQB() {
        return tQB;
    }

    public void setTQB(String tQB) {
        this.tQB = tQB;
    }

    public String getTQC() {
        return tQC;
    }

    public void setTQC(String tQC) {
        this.tQC = tQC;
    }

    public String getTQD() {
        return tQD;
    }

    public void setTQD(String tQD) {
        this.tQD = tQD;
    }

    public String getTQTRUE() {
        return tQTRUE;
    }

    public void setTQTRUE(String tQTRUE) {
        this.tQTRUE = tQTRUE;
    }

    public String getTQSTATUS() {
        return tQSTATUS;
    }

    public void setTQSTATUS(String tQSTATUS) {
        this.tQSTATUS = tQSTATUS;
    }

    public String getTNCREATEDBY() {
        return tNCREATEDBY;
    }

    public void setTNCREATEDBY(String tNCREATEDBY) {
        this.tNCREATEDBY = tNCREATEDBY;
    }

    public String getTQCREATEDAT() {
        return tQCREATEDAT;
    }

    public void setTQCREATEDAT(String tQCREATEDAT) {
        this.tQCREATEDAT = tQCREATEDAT;
    }
}
