package com.example.cia.deteksikesehatan.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.adapter.AdapterListUser;
import com.example.cia.deteksikesehatan.adapter.SoalAdapter;
import com.example.cia.deteksikesehatan.model.ListUserModel;
import com.example.cia.deteksikesehatan.model.PayLoadListUser;
import com.example.cia.deteksikesehatan.model.PayLoadSoalModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListUserActivity extends AppCompatActivity {

    private RecyclerView rvListuser;
    ArrayList<ListUserModel> listUserModel;
    RecyclerView.LayoutManager layoutManager;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_user);
        initView();

        data = getIntent().getStringExtra("data");

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvListuser.setLayoutManager(layoutManager);

        getUser();

    }

    private void getUser() {

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getUser().enqueue(new Callback<PayLoadListUser>() {
            @Override
            public void onResponse(Call<PayLoadListUser> call, Response<PayLoadListUser> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    if (response.body().getMsg().equals("Data list user")) {
                        listUserModel = response.body().getPayload();
                        AdapterListUser adapter = new AdapterListUser(listUserModel, getApplicationContext(),""+data);
                        rvListuser.setAdapter(adapter);
                    } else {
                        Toast.makeText(ListUserActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PayLoadListUser> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(ListUserActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        rvListuser = findViewById(R.id.rv_listuser);
    }
}
