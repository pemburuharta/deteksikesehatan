package com.example.cia.deteksikesehatan.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.Helper;
import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.BeritaActivity;
import com.example.cia.deteksikesehatan.activity.EditBeritaActivity;
import com.example.cia.deteksikesehatan.activity.MateriActivity;
import com.example.cia.deteksikesehatan.activity.TambahKategoriActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * A simple {@link Fragment} subclass.
 */
public class KategoriFragment extends Fragment {


    private LinearLayout div;
    private SwipeRefreshLayout swipe;
    private TextView tvKet;

    SharedPref pref;
    ProgressDialog pd;

    public KategoriFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_kategori, container, false);
        initView(view);

        pref = new SharedPref(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        getData(true);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
        return view;
    }

    private void getData(Boolean rm) {
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        swipe.setRefreshing(true);
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getKategori().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    swipe.setRefreshing(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("list data news kosong")){
                            div.setVisibility(View.GONE);
                            tvKet.setVisibility(View.VISIBLE);
                        }else {
                            div.setVisibility(View.VISIBLE);
                            tvKet.setVisibility(View.GONE);
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String judul = jsonObject1.optString("TK_NAMA");
                                final String gambar = jsonObject1.optString("TK_IMAGE");
                                final String isi = jsonObject1.optString("TK_SUBJECT");
                                final String created_at = jsonObject1.optString("TK_CREATED_AT");
                                final String id_news = jsonObject1.optString("TK_BIGID");

                                LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_kategori, null);

                                final ImageView img = view.findViewById(R.id.img_berita);
                                final LinearLayout cvMain = view.findViewById(R.id.card_berita);
                                TextView tvJudul = view.findViewById(R.id.txt_judul);
                                TextView tvCreatedAt = view.findViewById(R.id.row_kategori_tv_createdat);

                                tvJudul.setText(judul);
                                tvCreatedAt.setText(""+new Helper().convertDateFormat(created_at,"yyyy-MM-dd hh:mm:s"));
                                Picasso.with(getActivity())
                                        .load("http://dk.mitraredex.com/assets/gambar/"+gambar)
                                        .into(img);

                                    cvMain.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(final View view) {
                                            if (pref.getRule().equals("ADMIN")){
//                                                final CharSequence[] dialogItem = {"Detail","Edit", "Hapus"};
//                                                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
//                                                builder.setTitle("Tentukan Pilihan Anda");
//                                                builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
//                                                    @Override
//                                                    public void onClick(DialogInterface dialogInterface, final int wich) {
//                                                        switch (wich) {
//                                                            case 0 :
//                                                                Intent intent2 = new Intent(getActivity(), MateriActivity.class);
//                                                                intent2.putExtra("id_kategori",id_news);
//                                                                intent2.putExtra("judul", judul);
//                                                                intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                                                                startActivity(intent2);
//                                                                break;
//                                                            case 1 :
//
//                                                                Intent intent = new Intent(getActivity(), TambahKategoriActivity.class);
//                                                                intent.putExtra("status", "EDIT_BERITA");
//                                                                intent.putExtra("judul", judul);
//                                                                intent.putExtra("isi", isi);
//                                                                intent.putExtra("id_kategori", id_news);
//                                                                intent.putExtra("created_at", created_at);
//                                                                intent.putExtra("gambar", gambar);
//                                                                intent.putExtra("value", "EDIT");
//                                                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                                                                startActivity(intent);
//
//                                                                break;
//                                                            case 2 :
//
//                                                                new AlertDialog.Builder(view.getContext())
//                                                                        .setMessage("Apakah anda akan menghapus berita ini ?")
//                                                                        .setCancelable(false)
//                                                                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                                                            public void onClick(DialogInterface dialog, int id) {
//                                                                                HapusBerita(id_news);
//                                                                            }
//                                                                        })
//                                                                        .setNegativeButton("Tidak", null)
//                                                                        .show();
//                                                                break;
//                                                        }
//                                                    }
//                                                });
//                                                builder.create().show();
                                                final CharSequence[] dialogItem = {"Detail"};
                                                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                                builder.setTitle("Tentukan Pilihan Anda");
                                                builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, final int wich) {
                                                        switch (wich) {
                                                            case 0 :
                                                                Intent intent2 = new Intent(getActivity(), MateriActivity.class);
                                                                intent2.putExtra("id_kategori",id_news);
                                                                intent2.putExtra("judul", judul);
                                                                intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                                                startActivity(intent2);
                                                                break;
                                                        }
                                                    }
                                                });
                                                builder.create().show();
                                            }else {
                                                Intent intent = new Intent(getActivity(), MateriActivity.class);
                                                intent.putExtra("id_kategori",id_news);
                                                intent.putExtra("judul", judul);
                                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);
                                            }
                                        }
                                    });

                                div.addView(view);

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                swipe.setRefreshing(false);
                Toast.makeText(getActivity(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void HapusBerita(String id_news) {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.deleteKategoriMateri(
                "deletekategori",
                id_news).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Kategori Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Kategori Gagal Dihapus", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView(View view) {
        div = (LinearLayout) view.findViewById(R.id.div);
        swipe = (SwipeRefreshLayout) view.findViewById(R.id.swipe);
        tvKet = (TextView) view.findViewById(R.id.tv_ket);
    }
}
