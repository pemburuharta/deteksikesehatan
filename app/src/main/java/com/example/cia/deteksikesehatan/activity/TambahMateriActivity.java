package com.example.cia.deteksikesehatan.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahMateriActivity extends AppCompatActivity {

    private ImageView addmateriIvClose;
    private TextView addmateriTvActionbar;
    private EditText addmateriEdtJudul;
    private Spinner addmateriSpinner;
    private EditText addmateriEdtMateri;
    private ImageView addmateriIvThumbnail;
    private TextView addmateriTvThumbnail;
    private LinearLayout addmateriDivSimpan;
    private LinearLayout addmateriDivSpinner;

    ArrayList<String> listKategori = new ArrayList<>();

    File imageGambar;
    SharedPref pref;
    ProgressDialog pd;

    String ID_KATEGORI;
    String STATUS;
    String NamaFoto;
    String idMateri;
    String idKategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_materi);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        addmateriIvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(TambahMateriActivity.this, 3);
            }
        });

        addmateriTvThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openGallery(TambahMateriActivity.this, 3);
            }
        });

        STATUS = getIntent().getStringExtra("status");

        if (STATUS.equals("ADD")){

            addmateriIvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            getKategori();

            addmateriDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addMateri();
                }
            });

        } else {


            addmateriIvClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

            addmateriDivSpinner.setVisibility(View.GONE);
            addmateriEdtJudul.setText(getIntent().getStringExtra("judul"));
            addmateriEdtMateri.setText(getIntent().getStringExtra("isi"));
            NamaFoto = getIntent().getStringExtra("image");
            idMateri = getIntent().getStringExtra("id_materi");
            idKategori = getIntent().getStringExtra("id_kategori");

            Picasso.with(this)
                    .load("http://dk.mitraredex.com/assets/gambar/"+getIntent().getStringExtra("image"))
                    .into(addmateriIvThumbnail);

            addmateriDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editMateri();
                }
            });

        }
        
    }

    private void editMateri() {

        pd.show();

        APIService apiService =APIClient.getRetrofit().create(APIService.class);
        apiService.editMateri(idMateri,
                addmateriEdtJudul.getText().toString(),
                addmateriEdtMateri.getText().toString(),
                NamaFoto).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            onBackPressed();
//                            Intent intent= new Intent(getApplicationContext(), MateriActivity.class);
//                            intent.putExtra("id_kategori", idKategori);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
                            Toast.makeText(getApplicationContext(), "Materi Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Materi Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getKategori() {

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getKategori().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        final JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("list data news kosong")){

                        } else {
                            final JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String nama_kategori = jsonObject1.optString("TK_NAMA");
                                listKategori.add(nama_kategori);
                            }

                            ArrayAdapter<String> adapterKategori = new ArrayAdapter<String>(TambahMateriActivity.this, android.R.layout.simple_list_item_1, listKategori);
                            adapterKategori.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            addmateriSpinner.setAdapter(adapterKategori);
                            addmateriSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    ID_KATEGORI = jsonArray.optJSONObject(i).optString("TK_BIGID");
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahMateriActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void uploadGambar() {

        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageGambar);
        MultipartBody.Part bodyGambar = MultipartBody.Part.createFormData("image", imageGambar.getName(), requestImage);

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.materiUploadGambar(bodyGambar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            NamaFoto = jsonObject.optString("image_name");
                            Toast.makeText(getApplicationContext(), "Gambar Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "Gambar Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addMateri() {

        pd.show();

        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageGambar);
        MultipartBody.Part bodyGambar = MultipartBody.Part.createFormData("image", imageGambar.getName(), requestImage);

        RequestBody nama_materi = RequestBody.create(MediaType.parse("text/plain"), addmateriEdtJudul.getText().toString());
        RequestBody deskripsi_materi = RequestBody.create(MediaType.parse("text/plain"), addmateriEdtMateri.getText().toString());
        RequestBody id_user = RequestBody.create(MediaType.parse("text/plain"), pref.getIdUser());
        RequestBody id_kat = RequestBody.create(MediaType.parse("text/plain"), ID_KATEGORI);

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.addMateri(
                nama_materi,
                deskripsi_materi,
                id_user,
                id_kat,
                bodyGambar).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");

                        if (error.equals("false")){
                            Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            Toast.makeText(TambahMateriActivity.this, "Berhasil Menambahkan Kategori", Toast.LENGTH_SHORT).show();
                        } else {

                            String msg = jsonObject.optString("msg");
                            Toast.makeText(TambahMateriActivity.this, ""+msg, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(TambahMateriActivity.this, "Periksa Koneksi Anda ...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

                if (STATUS.equals("ADD")){
                    Glide.with(TambahMateriActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(addmateriIvThumbnail);
                    imageGambar = new File(imageFile.getPath());
                    NamaFoto = imageGambar.getName();
                } else {
                    Glide.with(TambahMateriActivity.this)
                            .load(new File(imageFile.getPath()))
                            .into(addmateriIvThumbnail);
                    imageGambar = new File(imageFile.getPath());
                    NamaFoto = imageGambar.getName();
                    uploadGambar();
                }

            }

        });

    }

    private void initView() {
        addmateriIvClose = findViewById(R.id.addmateri_iv_close);
        addmateriTvActionbar = findViewById(R.id.addmateri_tv_actionbar);
        addmateriEdtJudul = findViewById(R.id.addmateri_edt_judul);
        addmateriSpinner = findViewById(R.id.addmateri_spinner);
        addmateriEdtMateri = findViewById(R.id.addmateri_edt_materi);
        addmateriIvThumbnail = findViewById(R.id.addmateri_iv_thumbnail);
        addmateriTvThumbnail = findViewById(R.id.addmateri_tv_thumbnail);
        addmateriDivSimpan = findViewById(R.id.addmateri_div_simpan);
        addmateriDivSpinner = findViewById(R.id.addmateri_div_spinner);
    }
}
