package com.example.cia.deteksikesehatan.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

@Dao
public interface QuesionerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertQuesioner(Quesioner quesioner);

    @Query("SELECT * FROM tquesioner WHERE id_user =:idUser")
    Quesioner[] lihatQuesionerByUser(String idUser);

    @Query("SELECT * FROM tquesioner")
    Quesioner[] selectAllPesanan();

    @Delete
    int deletePesanan(Quesioner pesanan);
}
