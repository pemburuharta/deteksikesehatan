package com.example.cia.deteksikesehatan.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.Helper;
import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MateriActivity extends AppCompatActivity {

    private LinearLayout div;

    String data;

    SharedPref pref;
    ProgressDialog pd;
    private ImageView materiIvClose;
    private TextView materiTvActionbar;
    int level;
    private Button btnSoal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materi);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);

        materiIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        materiTvActionbar.setText(getIntent().getStringExtra("judul"));
        data = getIntent().getStringExtra("id_kategori");

        btnSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SoalActivity.class);
                intent.putExtra("id_kategori", data);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        getMateri();
//        cekLevel();

    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        getMateri();
//    }

    private void cekLevel() {
        pd.show();
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.cekLevel(pref.getIdUser(),
                "" + data).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonObject.optJSONArray("list");
                        level = jsonArray.length();

//                        Toast.makeText(MateriActivity.this, "size "+jsonArray.length(), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
            }
        });
    }

    private void getMateri() {
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getMateri(data).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("msg");
                        if (error.equals("false")) {
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String nama = jsonObject1.optString("TS_NAMA");
                                final String id_kat = jsonObject1.optString("TS_KAT_ID");
                                final String id_materi = jsonObject1.optString("TS_BIGID");
                                final String isi = jsonObject1.optString("TS_SUBJECT");
                                final String image = jsonObject1.optString("TS_IMAGE");
                                final String created_at = jsonObject1.optString("TS_CREATED_AT");

                                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_materi, null);

                                final ImageView img = view.findViewById(R.id.img_materi);
                                final LinearLayout cvMain = view.findViewById(R.id.card_materi);
                                TextView tvJudul = view.findViewById(R.id.judul_materi);
                                TextView tvCreatedAt = view.findViewById(R.id.row_materi_tv_createdat);

                                tvJudul.setText("" + nama);
                                tvCreatedAt.setText("" + new Helper().convertDateFormat(created_at, "yyyy-MM-dd hh:mm:s"));
                                Picasso.with(getApplicationContext())
                                        .load("http://dk.mitraredex.com/assets/gambar/" + image)
                                        .into(img);

                                final int posisi = i;

                                cvMain.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (pref.getRule().equals("ADMIN")) {
//                                            final CharSequence[] dialogItem = {"Detail", "Edit", "Hapus"};
//                                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
//                                            builder.setTitle("Tentukan Pilihan Anda");
//                                            builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
//                                                @Override
//                                                public void onClick(DialogInterface dialogInterface, final int wich) {
//                                                    switch (wich) {
//                                                        case 0:
//                                                            Intent intent2 = new Intent(getApplicationContext(), IsiMateriActivity.class);
//                                                            intent2.putExtra("judul", nama);
//                                                            intent2.putExtra("isi", isi);
//                                                            intent2.putExtra("image", image);
//                                                            intent2.putExtra("created_at", created_at);
//                                                            intent2.putExtra("id_materi", id_materi);
//                                                            intent2.putExtra("id_kategori", id_kat);
//                                                            intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                                                            startActivity(intent2);
//                                                            break;
//                                                        case 1:
//
//                                                            Intent intent = new Intent(getApplicationContext(), TambahMateriActivity.class);
//                                                            intent.putExtra("judul", nama);
//                                                            intent.putExtra("isi", isi);
//                                                            intent.putExtra("image", image);
//                                                            intent.putExtra("created_at", created_at);
//                                                            intent.putExtra("id_materi", id_materi);
//                                                            intent.putExtra("id_kategori", id_materi);
//                                                            intent.putExtra("status", "EDIT");
//                                                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                                                            startActivity(intent);
//
//                                                            break;
//                                                        case 2:
//
//                                                            new AlertDialog.Builder(MateriActivity.this)
//                                                                    .setMessage("Apakah anda akan menghapus materi ini ?")
//                                                                    .setCancelable(false)
//                                                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//                                                                        public void onClick(DialogInterface dialog, int id) {
//                                                                            HapusMateri(id_materi);
//                                                                        }
//                                                                    })
//                                                                    .setNegativeButton("Tidak", null)
//                                                                    .show();
//                                                            break;
//                                                    }
//                                                }
//                                            });
//                                            builder.create().show();
                                            final CharSequence[] dialogItem = {"Detail"};
                                            AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                                            builder.setTitle("Tentukan Pilihan Anda");
                                            builder.setItems(dialogItem, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, final int wich) {
                                                    switch (wich) {
                                                        case 0:
                                                            Intent intent2 = new Intent(getApplicationContext(), IsiMateriActivity.class);
                                                            intent2.putExtra("judul", nama);
                                                            intent2.putExtra("isi", isi);
                                                            intent2.putExtra("image", image);
                                                            intent2.putExtra("created_at", created_at);
                                                            intent2.putExtra("id_materi", id_materi);
                                                            intent2.putExtra("id_kategori", id_kat);
                                                            intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                                            startActivity(intent2);
                                                            break;
                                                    }
                                                }
                                            });
                                            builder.create().show();
                                        } else {
//                                            if (level >= posisi) {
//                                                Intent intent2 = new Intent(getApplicationContext(), IsiMateriActivity.class);
//                                                intent2.putExtra("judul", nama);
//                                                intent2.putExtra("isi", isi);
//                                                intent2.putExtra("image", image);
//                                                intent2.putExtra("created_at", created_at);
//                                                intent2.putExtra("id_materi", id_materi);
//                                                intent2.putExtra("id_kategori", id_kat);
//                                                intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
//                                                startActivity(intent2);
//                                            } else {
//                                                Toast.makeText(MateriActivity.this, "Anda Belum Menyelesaikan Materi Sebelumnya", Toast.LENGTH_SHORT).show();
//                                            }
                                            Intent intent2 = new Intent(getApplicationContext(), IsiMateriActivity.class);
                                            intent2.putExtra("judul", nama);
                                            intent2.putExtra("isi", isi);
                                            intent2.putExtra("image", image);
                                            intent2.putExtra("created_at", created_at);
                                            intent2.putExtra("id_materi", id_materi);
                                            intent2.putExtra("id_kategori", id_kat);
                                            intent2.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent2);
                                        }
                                    }
                                });
                                div.addView(view);
                            }

                        } else {
                            Toast.makeText(MateriActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MateriActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void HapusMateri(String id_materi) {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.deleteKategoriMateri(
                "deletemateri",
                id_materi).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")) {
                        Intent intent = new Intent(getApplicationContext(), MateriActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Materi Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Materi Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        div = (LinearLayout) findViewById(R.id.div);
        materiIvClose = findViewById(R.id.materi_iv_close);
        materiTvActionbar = findViewById(R.id.materi_tv_actionbar);
        btnSoal = (Button) findViewById(R.id.btn_soal);
    }

}
