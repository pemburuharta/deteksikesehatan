package com.example.cia.deteksikesehatan.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.MateriActivity;
import com.example.cia.deteksikesehatan.activity.SoalActivity;
import com.example.cia.deteksikesehatan.model.PayLoadSoalModel;
import com.example.cia.deteksikesehatan.model.SoalModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoalAdapter extends RecyclerView.Adapter<SoalAdapter.Holder> {

    ArrayList<SoalModel> list;
    Context context;
    public String[] jawaban;
    public String[] idSoal;

    SharedPref pref;
    int id;

    public SoalAdapter(ArrayList<SoalModel> list, Context context) {
        this.list = list;
        this.context = context;
        this.jawaban = new String[list.size()];
        this.idSoal = new String[list.size()];

        for (int x = 0; x < list.size(); x++) {
            this.jawaban[x] = "null";
            this.idSoal[x] = "null";
        }
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_soal, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int position) {

        pref = new SharedPref(context);
        id = Integer.valueOf(list.get(position).getTQBIGID());

        if (pref.getRule().equals("ADMIN")){
            holder.ivDelete.setVisibility(View.VISIBLE);

            holder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(view.getContext())
                        .setMessage("Apakah anda akan menghapus soal ini ?")
                        .setCancelable(false)
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                hapusSoal(position);
                            }
                        })
                        .setNegativeButton("Tidak", null)
                        .show();
                }
            });

        }

        holder.tvSoal.setText(""+list.get(position).getTQSOAL());
        holder.rbA.setText(""+list.get(position).getTQA());
        holder.rbB.setText(""+list.get(position).getTQB());
        holder.rbC.setText(""+list.get(position).getTQC());
        holder.rbD.setText(""+list.get(position).getTQD());

        holder.rbJawaban.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                SoalAdapter.this.jawaban[holder.getAdapterPosition()] = ((RadioButton) radioGroup.findViewById(i)).getText().toString();
                SoalAdapter.this.idSoal[holder.getAdapterPosition()] = String.valueOf(id);
            }
        });
    }

    private void hapusSoal(Integer id_soal) {

//        final ProgressDialog pd = new ProgressDialog(context);
//        pd.setMessage("Loading...");
//        pd.setCancelable(false);
//        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.deleteSoal(
                "delete",
                list.get(id_soal).getTQBIGID()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                pd.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    String error = jsonObject.optString("error");
                    if (error.equals("false")){
                        Intent intent = new Intent(context, MateriActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                        Toast.makeText(context, "Soal Berhasil Dihapus", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Soal Gagal Dihapus", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                pd.dismiss();
                Toast.makeText(context, "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        private TextView tvSoal;
        private RadioGroup rbJawaban;
        private RadioButton rbA;
        private RadioButton rbB;
        private RadioButton rbC;
        private RadioButton rbD;
        private ImageView ivDelete;

        public Holder(View itemView) {
            super(itemView);

            tvSoal = (TextView) itemView.findViewById(R.id.tv_soal);
            ivDelete = (ImageView) itemView.findViewById(R.id.soal_iv_delete);
            rbJawaban = (RadioGroup) itemView.findViewById(R.id.rb_jawaban);
            rbA = (RadioButton) itemView.findViewById(R.id.rb_a);
            rbB = (RadioButton) itemView.findViewById(R.id.rb_b);
            rbC = (RadioButton) itemView.findViewById(R.id.rb_c);
            rbD = (RadioButton) itemView.findViewById(R.id.rb_d);
        }
    }
}
