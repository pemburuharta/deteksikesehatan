package com.example.cia.deteksikesehatan.Helper;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    public static final String STATUS_LOGIN = "login";
    public static final String MYPREF = "MAIN_PREF";

    public static final String ID_USER = "id_user";
    public static final String NAME = "name";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String NOTELP = "no_telp";
    public static final String TEMPAT_LAHIR = "tempat_lahir";
    public static final String TANGGAL_LAHIR = "tanggal_lahir";
    public static final String STATUS_USER = "status_user";
    public static final String AVATAR = "avatar";
    public static final String RULE = "rule";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        sharedPreferences = context.getSharedPreferences(MYPREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void savePrefBoolean(String keySP, boolean value) {
        editor.putBoolean(keySP, value);
        editor.commit();
    }

    public void savePrefString(String keySP, String value) {
        editor.putString(keySP, value);
        editor.commit();
    }

    public void clearAll(){
        editor.clear();
        editor.commit();
    }

    public Boolean getStatusLogin() {
        return sharedPreferences.getBoolean(STATUS_LOGIN, false);
    }

    public String getIdUser(){
        return sharedPreferences.getString(ID_USER,"");
    }

    public String getName(){
        return sharedPreferences.getString(NAME,"");
    }

    public String getFullname(){
        return sharedPreferences.getString(FULLNAME,"");
    }

    public String getEmail(){
        return sharedPreferences.getString(EMAIL,"");
    }

    public String getNotelp(){
        return sharedPreferences.getString(NOTELP,"");
    }

    public String getStatusUser(){
        return sharedPreferences.getString(STATUS_USER,"");
    }

    public String getAvatar(){
        return sharedPreferences.getString(AVATAR,"");
    }

    public String getTempatLahir(){
        return sharedPreferences.getString(TEMPAT_LAHIR,"");
    }

    public String getTanggalLahir(){
        return sharedPreferences.getString(TANGGAL_LAHIR,"");
    }

    public String getRule(){
        return sharedPreferences.getString(RULE,"");
    }

}
