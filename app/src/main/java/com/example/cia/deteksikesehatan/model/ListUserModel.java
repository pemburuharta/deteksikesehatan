package com.example.cia.deteksikesehatan.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListUserModel implements Parcelable {
    @SerializedName("TU_BIGID")
    @Expose
    private String tUBIGID;
    @SerializedName("TU_NAME")
    @Expose
    private String tUNAME;
    @SerializedName("TU_FULLNAME")
    @Expose
    private String tUFULLNAME;
    @SerializedName("TU_EMAIL")
    @Expose
    private String tUEMAIL;
    @SerializedName("TU_TELP")
    @Expose
    private String tUTELP;
    @SerializedName("TU_STATUS")
    @Expose
    private String tUSTATUS;
    @SerializedName("TU_GROUP_RULE")
    @Expose
    private String tUGROUPRULE;
    @SerializedName("TU_TANGGAL_LAHIR")
    @Expose
    private String tUTANGGALLAHIR;
    @SerializedName("TU_TEMPAT_LAHIR")
    @Expose
    private String tUTEMPATLAHIR;
    @SerializedName("TU_UMUR")
    @Expose
    private String tUUMUR;
    @SerializedName("TU_LOGIN_WAKTU")
    @Expose
    private String tULOGINWAKTU;
    @SerializedName("TU_LOGOUT_WAKTU")
    @Expose
    private String tULOGOUTWAKTU;
    @SerializedName("TU_LOGIN_TOKEN")
    @Expose
    private String tULOGINTOKEN;
    @SerializedName("TU_DEFAULT_BROWSER")
    @Expose
    private String tUDEFAULTBROWSER;
    @SerializedName("TU_IP_POSITION")
    @Expose
    private String tUIPPOSITION;
    @SerializedName("TU_CREATED_AT")
    @Expose
    private String tUCREATEDAT;
    @SerializedName("TU_AVATAR")
    @Expose
    private String tUAVATAR;
    @SerializedName("TUD_BIGID")
    @Expose
    private String tUDBIGID;
    @SerializedName("TUD_KODE")
    @Expose
    private String tUDKODE;
    @SerializedName("TUD_USERID")
    @Expose
    private String tUDUSERID;
    @SerializedName("TD_TINGGI_BDN")
    @Expose
    private String tDTINGGIBDN;
    @SerializedName("TD_BERAT_BDN")
    @Expose
    private String tDBERATBDN;
    @SerializedName("TD_LINGKAR_PRT")
    @Expose
    private String tDLINGKARPRT;
    @SerializedName("TD_SISTOLIK")
    @Expose
    private String tDSISTOLIK;
    @SerializedName("TD_DISTOLIK")
    @Expose
    private String tDDISTOLIK;
    @SerializedName("TD_GULA_DARAH")
    @Expose
    private String tDGULADARAH;
    @SerializedName("TD_KEGIATAN_LUAR")
    @Expose
    private String tDKEGIATANLUAR;
    @SerializedName("TD_GUNA_APPS")
    @Expose
    private String tDGUNAAPPS;
    @SerializedName("TD_JOIN_EVENT")
    @Expose
    private String tDJOINEVENT;
    @SerializedName("TD_INFORM")
    @Expose
    private String tDINFORM;
    @SerializedName("TD_TURUNAN")
    @Expose
    private String tDTURUNAN;
    @SerializedName("TD_PERIKSA")
    @Expose
    private String tDPERIKSA;
    @SerializedName("TD_PEROKOK")
    @Expose
    private String tDPEROKOK;
    @SerializedName("TD_LINGKUNGAN")
    @Expose
    private String tDLINGKUNGAN;
    @SerializedName("TD_MIRAS")
    @Expose
    private String tDMIRAS;
    @SerializedName("TD_OLAHRAGA")
    @Expose
    private String tDOLAHRAGA;
    @SerializedName("TD_SNACK")
    @Expose
    private String tDSNACK;
    @SerializedName("TD_SOFTDRINK")
    @Expose
    private String tDSOFTDRINK;
    @SerializedName("TD_TIDUR")
    @Expose
    private String tDTIDUR;
    @SerializedName("TD_DEKATKAN")
    @Expose
    private String tDDEKATKAN;
    @SerializedName("TUD_ASAL_SEKOLAH")
    @Expose
    private String tUDASALSEKOLAH;
    @SerializedName("TUD_KELAS")
    @Expose
    private String tUDKELAS;
    @SerializedName("TUD_JK")
    @Expose
    private String tUDJK;
    @SerializedName("TUD_KOTA")
    @Expose
    private String tUDKOTA;
    @SerializedName("TUD_KECAMATAN")
    @Expose
    private String tUDKECAMATAN;
    @SerializedName("TUD_KELURAHAN")
    @Expose
    private String tUDKELURAHAN;
    @SerializedName("TUD_RT")
    @Expose
    private String tUDRT;
    @SerializedName("TUD_RW")
    @Expose
    private String tUDRW;
    @SerializedName("TUD_STUDI_AYAH")
    @Expose
    private String tUDSTUDIAYAH;
    @SerializedName("TUD_WORK_AYAH")
    @Expose
    private String tUDWORKAYAH;
    @SerializedName("TUD_STUDI_IBU")
    @Expose
    private String tUDSTUDIIBU;
    @SerializedName("TUD_WORK_IBU")
    @Expose
    private String tUDWORKIBU;
    @SerializedName("TUD_INCOME")
    @Expose
    private String tUDINCOME;
    @SerializedName("TUD_CREATED_AT")
    @Expose
    private String tUDCREATEDAT;

    protected ListUserModel(Parcel in) {
        tUBIGID = in.readString();
        tUNAME = in.readString();
        tUFULLNAME = in.readString();
        tUEMAIL = in.readString();
        tUTELP = in.readString();
        tUSTATUS = in.readString();
        tUGROUPRULE = in.readString();
        tUTANGGALLAHIR = in.readString();
        tUTEMPATLAHIR = in.readString();
        tUUMUR = in.readString();
        tULOGINWAKTU = in.readString();
        tULOGOUTWAKTU = in.readString();
        tULOGINTOKEN = in.readString();
        tUDEFAULTBROWSER = in.readString();
        tUIPPOSITION = in.readString();
        tUCREATEDAT = in.readString();
        tUAVATAR = in.readString();
        tUDBIGID = in.readString();
        tUDKODE = in.readString();
        tUDUSERID = in.readString();
        tDTINGGIBDN = in.readString();
        tDBERATBDN = in.readString();
        tDLINGKARPRT = in.readString();
        tDSISTOLIK = in.readString();
        tDDISTOLIK = in.readString();
        tDGULADARAH = in.readString();
        tDKEGIATANLUAR = in.readString();
        tDGUNAAPPS = in.readString();
        tDJOINEVENT = in.readString();
        tDINFORM = in.readString();
        tDTURUNAN = in.readString();
        tDPERIKSA = in.readString();
        tDPEROKOK = in.readString();
        tDLINGKUNGAN = in.readString();
        tDMIRAS = in.readString();
        tDOLAHRAGA = in.readString();
        tDSNACK = in.readString();
        tDSOFTDRINK = in.readString();
        tDTIDUR = in.readString();
        tDDEKATKAN = in.readString();
        tUDASALSEKOLAH = in.readString();
        tUDKELAS = in.readString();
        tUDJK = in.readString();
        tUDKOTA = in.readString();
        tUDKECAMATAN = in.readString();
        tUDKELURAHAN = in.readString();
        tUDRT = in.readString();
        tUDRW = in.readString();
        tUDSTUDIAYAH = in.readString();
        tUDWORKAYAH = in.readString();
        tUDSTUDIIBU = in.readString();
        tUDWORKIBU = in.readString();
        tUDINCOME = in.readString();
        tUDCREATEDAT = in.readString();
    }

    public static final Creator<ListUserModel> CREATOR = new Creator<ListUserModel>() {
        @Override
        public ListUserModel createFromParcel(Parcel in) {
            return new ListUserModel(in);
        }

        @Override
        public ListUserModel[] newArray(int size) {
            return new ListUserModel[size];
        }
    };

    public String getTUBIGID() {
        return tUBIGID;
    }

    public void setTUBIGID(String tUBIGID) {
        this.tUBIGID = tUBIGID;
    }

    public String getTUNAME() {
        return tUNAME;
    }

    public void setTUNAME(String tUNAME) {
        this.tUNAME = tUNAME;
    }

    public String getTUFULLNAME() {
        return tUFULLNAME;
    }

    public void setTUFULLNAME(String tUFULLNAME) {
        this.tUFULLNAME = tUFULLNAME;
    }

    public String getTUEMAIL() {
        return tUEMAIL;
    }

    public void setTUEMAIL(String tUEMAIL) {
        this.tUEMAIL = tUEMAIL;
    }

    public String getTUTELP() {
        return tUTELP;
    }

    public void setTUTELP(String tUTELP) {
        this.tUTELP = tUTELP;
    }

    public String getTUSTATUS() {
        return tUSTATUS;
    }

    public void setTUSTATUS(String tUSTATUS) {
        this.tUSTATUS = tUSTATUS;
    }

    public String getTUGROUPRULE() {
        return tUGROUPRULE;
    }

    public void setTUGROUPRULE(String tUGROUPRULE) {
        this.tUGROUPRULE = tUGROUPRULE;
    }

    public String getTUTANGGALLAHIR() {
        return tUTANGGALLAHIR;
    }

    public void setTUTANGGALLAHIR(String tUTANGGALLAHIR) {
        this.tUTANGGALLAHIR = tUTANGGALLAHIR;
    }

    public String getTUTEMPATLAHIR() {
        return tUTEMPATLAHIR;
    }

    public void setTUTEMPATLAHIR(String tUTEMPATLAHIR) {
        this.tUTEMPATLAHIR = tUTEMPATLAHIR;
    }

    public String getTUUMUR() {
        return tUUMUR;
    }

    public void setTUUMUR(String tUUMUR) {
        this.tUUMUR = tUUMUR;
    }

    public String getTULOGINWAKTU() {
        return tULOGINWAKTU;
    }

    public void setTULOGINWAKTU(String tULOGINWAKTU) {
        this.tULOGINWAKTU = tULOGINWAKTU;
    }

    public String getTULOGOUTWAKTU() {
        return tULOGOUTWAKTU;
    }

    public void setTULOGOUTWAKTU(String tULOGOUTWAKTU) {
        this.tULOGOUTWAKTU = tULOGOUTWAKTU;
    }

    public String getTULOGINTOKEN() {
        return tULOGINTOKEN;
    }

    public void setTULOGINTOKEN(String tULOGINTOKEN) {
        this.tULOGINTOKEN = tULOGINTOKEN;
    }

    public String getTUDEFAULTBROWSER() {
        return tUDEFAULTBROWSER;
    }

    public void setTUDEFAULTBROWSER(String tUDEFAULTBROWSER) {
        this.tUDEFAULTBROWSER = tUDEFAULTBROWSER;
    }

    public String getTUIPPOSITION() {
        return tUIPPOSITION;
    }

    public void setTUIPPOSITION(String tUIPPOSITION) {
        this.tUIPPOSITION = tUIPPOSITION;
    }

    public String getTUCREATEDAT() {
        return tUCREATEDAT;
    }

    public void setTUCREATEDAT(String tUCREATEDAT) {
        this.tUCREATEDAT = tUCREATEDAT;
    }

    public String getTUAVATAR() {
        return tUAVATAR;
    }

    public void setTUAVATAR(String tUAVATAR) {
        this.tUAVATAR = tUAVATAR;
    }

    public String getTUDBIGID() {
        return tUDBIGID;
    }

    public void setTUDBIGID(String tUDBIGID) {
        this.tUDBIGID = tUDBIGID;
    }

    public String getTUDKODE() {
        return tUDKODE;
    }

    public void setTUDKODE(String tUDKODE) {
        this.tUDKODE = tUDKODE;
    }

    public String getTUDUSERID() {
        return tUDUSERID;
    }

    public void setTUDUSERID(String tUDUSERID) {
        this.tUDUSERID = tUDUSERID;
    }

    public String getTDTINGGIBDN() {
        return tDTINGGIBDN;
    }

    public void setTDTINGGIBDN(String tDTINGGIBDN) {
        this.tDTINGGIBDN = tDTINGGIBDN;
    }

    public String getTDBERATBDN() {
        return tDBERATBDN;
    }

    public void setTDBERATBDN(String tDBERATBDN) {
        this.tDBERATBDN = tDBERATBDN;
    }

    public String getTDLINGKARPRT() {
        return tDLINGKARPRT;
    }

    public void setTDLINGKARPRT(String tDLINGKARPRT) {
        this.tDLINGKARPRT = tDLINGKARPRT;
    }

    public String getTDSISTOLIK() {
        return tDSISTOLIK;
    }

    public void setTDSISTOLIK(String tDSISTOLIK) {
        this.tDSISTOLIK = tDSISTOLIK;
    }

    public String getTDDISTOLIK() {
        return tDDISTOLIK;
    }

    public void setTDDISTOLIK(String tDDISTOLIK) {
        this.tDDISTOLIK = tDDISTOLIK;
    }

    public String getTDGULADARAH() {
        return tDGULADARAH;
    }

    public void setTDGULADARAH(String tDGULADARAH) {
        this.tDGULADARAH = tDGULADARAH;
    }

    public String getTDKEGIATANLUAR() {
        return tDKEGIATANLUAR;
    }

    public void setTDKEGIATANLUAR(String tDKEGIATANLUAR) {
        this.tDKEGIATANLUAR = tDKEGIATANLUAR;
    }

    public String getTDGUNAAPPS() {
        return tDGUNAAPPS;
    }

    public void setTDGUNAAPPS(String tDGUNAAPPS) {
        this.tDGUNAAPPS = tDGUNAAPPS;
    }

    public String getTDJOINEVENT() {
        return tDJOINEVENT;
    }

    public void setTDJOINEVENT(String tDJOINEVENT) {
        this.tDJOINEVENT = tDJOINEVENT;
    }

    public String getTDINFORM() {
        return tDINFORM;
    }

    public void setTDINFORM(String tDINFORM) {
        this.tDINFORM = tDINFORM;
    }

    public String getTDTURUNAN() {
        return tDTURUNAN;
    }

    public void setTDTURUNAN(String tDTURUNAN) {
        this.tDTURUNAN = tDTURUNAN;
    }

    public String getTDPERIKSA() {
        return tDPERIKSA;
    }

    public void setTDPERIKSA(String tDPERIKSA) {
        this.tDPERIKSA = tDPERIKSA;
    }

    public String getTDPEROKOK() {
        return tDPEROKOK;
    }

    public void setTDPEROKOK(String tDPEROKOK) {
        this.tDPEROKOK = tDPEROKOK;
    }

    public String getTDLINGKUNGAN() {
        return tDLINGKUNGAN;
    }

    public void setTDLINGKUNGAN(String tDLINGKUNGAN) {
        this.tDLINGKUNGAN = tDLINGKUNGAN;
    }

    public String getTDMIRAS() {
        return tDMIRAS;
    }

    public void setTDMIRAS(String tDMIRAS) {
        this.tDMIRAS = tDMIRAS;
    }

    public String getTDOLAHRAGA() {
        return tDOLAHRAGA;
    }

    public void setTDOLAHRAGA(String tDOLAHRAGA) {
        this.tDOLAHRAGA = tDOLAHRAGA;
    }

    public String getTDSNACK() {
        return tDSNACK;
    }

    public void setTDSNACK(String tDSNACK) {
        this.tDSNACK = tDSNACK;
    }

    public String getTDSOFTDRINK() {
        return tDSOFTDRINK;
    }

    public void setTDSOFTDRINK(String tDSOFTDRINK) {
        this.tDSOFTDRINK = tDSOFTDRINK;
    }

    public String getTDTIDUR() {
        return tDTIDUR;
    }

    public void setTDTIDUR(String tDTIDUR) {
        this.tDTIDUR = tDTIDUR;
    }

    public String getTDDEKATKAN() {
        return tDDEKATKAN;
    }

    public void setTDDEKATKAN(String tDDEKATKAN) {
        this.tDDEKATKAN = tDDEKATKAN;
    }

    public String getTUDASALSEKOLAH() {
        return tUDASALSEKOLAH;
    }

    public void setTUDASALSEKOLAH(String tUDASALSEKOLAH) {
        this.tUDASALSEKOLAH = tUDASALSEKOLAH;
    }

    public String getTUDKELAS() {
        return tUDKELAS;
    }

    public void setTUDKELAS(String tUDKELAS) {
        this.tUDKELAS = tUDKELAS;
    }

    public String getTUDJK() {
        return tUDJK;
    }

    public void setTUDJK(String tUDJK) {
        this.tUDJK = tUDJK;
    }

    public String getTUDKOTA() {
        return tUDKOTA;
    }

    public void setTUDKOTA(String tUDKOTA) {
        this.tUDKOTA = tUDKOTA;
    }

    public String getTUDKECAMATAN() {
        return tUDKECAMATAN;
    }

    public void setTUDKECAMATAN(String tUDKECAMATAN) {
        this.tUDKECAMATAN = tUDKECAMATAN;
    }

    public String getTUDKELURAHAN() {
        return tUDKELURAHAN;
    }

    public void setTUDKELURAHAN(String tUDKELURAHAN) {
        this.tUDKELURAHAN = tUDKELURAHAN;
    }

    public String getTUDRT() {
        return tUDRT;
    }

    public void setTUDRT(String tUDRT) {
        this.tUDRT = tUDRT;
    }

    public String getTUDRW() {
        return tUDRW;
    }

    public void setTUDRW(String tUDRW) {
        this.tUDRW = tUDRW;
    }

    public String getTUDSTUDIAYAH() {
        return tUDSTUDIAYAH;
    }

    public void setTUDSTUDIAYAH(String tUDSTUDIAYAH) {
        this.tUDSTUDIAYAH = tUDSTUDIAYAH;
    }

    public String getTUDWORKAYAH() {
        return tUDWORKAYAH;
    }

    public void setTUDWORKAYAH(String tUDWORKAYAH) {
        this.tUDWORKAYAH = tUDWORKAYAH;
    }

    public String getTUDSTUDIIBU() {
        return tUDSTUDIIBU;
    }

    public void setTUDSTUDIIBU(String tUDSTUDIIBU) {
        this.tUDSTUDIIBU = tUDSTUDIIBU;
    }

    public String getTUDWORKIBU() {
        return tUDWORKIBU;
    }

    public void setTUDWORKIBU(String tUDWORKIBU) {
        this.tUDWORKIBU = tUDWORKIBU;
    }

    public String getTUDINCOME() {
        return tUDINCOME;
    }

    public void setTUDINCOME(String tUDINCOME) {
        this.tUDINCOME = tUDINCOME;
    }

    public String getTUDCREATEDAT() {
        return tUDCREATEDAT;
    }

    public void setTUDCREATEDAT(String tUDCREATEDAT) {
        this.tUDCREATEDAT = tUDCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tUBIGID);
        parcel.writeString(tUNAME);
        parcel.writeString(tUFULLNAME);
        parcel.writeString(tUEMAIL);
        parcel.writeString(tUTELP);
        parcel.writeString(tUSTATUS);
        parcel.writeString(tUGROUPRULE);
        parcel.writeString(tUTANGGALLAHIR);
        parcel.writeString(tUTEMPATLAHIR);
        parcel.writeString(tUUMUR);
        parcel.writeString(tULOGINWAKTU);
        parcel.writeString(tULOGOUTWAKTU);
        parcel.writeString(tULOGINTOKEN);
        parcel.writeString(tUDEFAULTBROWSER);
        parcel.writeString(tUIPPOSITION);
        parcel.writeString(tUCREATEDAT);
        parcel.writeString(tUAVATAR);
        parcel.writeString(tUDBIGID);
        parcel.writeString(tUDKODE);
        parcel.writeString(tUDUSERID);
        parcel.writeString(tDTINGGIBDN);
        parcel.writeString(tDBERATBDN);
        parcel.writeString(tDLINGKARPRT);
        parcel.writeString(tDSISTOLIK);
        parcel.writeString(tDDISTOLIK);
        parcel.writeString(tDGULADARAH);
        parcel.writeString(tDKEGIATANLUAR);
        parcel.writeString(tDGUNAAPPS);
        parcel.writeString(tDJOINEVENT);
        parcel.writeString(tDINFORM);
        parcel.writeString(tDTURUNAN);
        parcel.writeString(tDPERIKSA);
        parcel.writeString(tDPEROKOK);
        parcel.writeString(tDLINGKUNGAN);
        parcel.writeString(tDMIRAS);
        parcel.writeString(tDOLAHRAGA);
        parcel.writeString(tDSNACK);
        parcel.writeString(tDSOFTDRINK);
        parcel.writeString(tDTIDUR);
        parcel.writeString(tDDEKATKAN);
        parcel.writeString(tUDASALSEKOLAH);
        parcel.writeString(tUDKELAS);
        parcel.writeString(tUDJK);
        parcel.writeString(tUDKOTA);
        parcel.writeString(tUDKECAMATAN);
        parcel.writeString(tUDKELURAHAN);
        parcel.writeString(tUDRT);
        parcel.writeString(tUDRW);
        parcel.writeString(tUDSTUDIAYAH);
        parcel.writeString(tUDWORKAYAH);
        parcel.writeString(tUDSTUDIIBU);
        parcel.writeString(tUDWORKIBU);
        parcel.writeString(tUDINCOME);
        parcel.writeString(tUDCREATEDAT);
    }
}
