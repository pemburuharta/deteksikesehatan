package com.example.cia.deteksikesehatan.room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "tquesioner")
public class Quesioner implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int idQuesioner;

    @ColumnInfo(name = "id_user")
    public String idUser;

    @ColumnInfo(name = "nama_user")
    public String namaUser;


    @ColumnInfo(name = "berat_badan")
    public String beratBadan;


    @ColumnInfo(name = "tinggi_badan")
    public String tinggiBadan;


    @ColumnInfo(name = "merokok")
    public String merokok;


    @ColumnInfo(name = "asap_roko")
    public String asapRokok;


    @ColumnInfo(name = "alkohol")
    public String alkohol;

    @ColumnInfo(name = "aktifitas")
    public String aktifitasHarian;

    @ColumnInfo(name = "olahraga")
    public String olahraga;

    @ColumnInfo(name = "minum")
    public String minum;

    @ColumnInfo(name = "makanan")
    public String makanan;

    @ColumnInfo(name = "lauk")
    public String lauk;

    @ColumnInfo(name = "buah")
    public String buah;

    @ColumnInfo(name = "sayur")
    public String sayur;

    @ColumnInfo(name = "tidur")
    public String tidur;

    @ColumnInfo(name = "stres")
    public String stres;

    public int getIdQuesioner() {
        return idQuesioner;
    }

    public void setIdQuesioner(int idQuesioner) {
        this.idQuesioner = idQuesioner;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNamaUser() {
        return namaUser;
    }

    public void setNamaUser(String namaUser) {
        this.namaUser = namaUser;
    }

    public String getBeratBadan() {
        return beratBadan;
    }

    public void setBeratBadan(String beratBadan) {
        this.beratBadan = beratBadan;
    }

    public String getTinggiBadan() {
        return tinggiBadan;
    }

    public void setTinggiBadan(String tinggiBadan) {
        this.tinggiBadan = tinggiBadan;
    }

    public String getMerokok() {
        return merokok;
    }

    public void setMerokok(String merokok) {
        this.merokok = merokok;
    }

    public String getAsapRokok() {
        return asapRokok;
    }

    public void setAsapRokok(String asapRokok) {
        this.asapRokok = asapRokok;
    }

    public String getAlkohol() {
        return alkohol;
    }

    public void setAlkohol(String alkohol) {
        this.alkohol = alkohol;
    }

    public String getAktifitasHarian() {
        return aktifitasHarian;
    }

    public void setAktifitasHarian(String aktifitasHarian) {
        this.aktifitasHarian = aktifitasHarian;
    }

    public String getOlahraga() {
        return olahraga;
    }

    public void setOlahraga(String olahraga) {
        this.olahraga = olahraga;
    }

    public String getMinum() {
        return minum;
    }

    public void setMinum(String minum) {
        this.minum = minum;
    }

    public String getMakanan() {
        return makanan;
    }

    public void setMakanan(String makanan) {
        this.makanan = makanan;
    }

    public String getLauk() {
        return lauk;
    }

    public void setLauk(String lauk) {
        this.lauk = lauk;
    }

    public String getBuah() {
        return buah;
    }

    public void setBuah(String buah) {
        this.buah = buah;
    }

    public String getSayur() {
        return sayur;
    }

    public void setSayur(String sayur) {
        this.sayur = sayur;
    }

    public String getTidur() {
        return tidur;
    }

    public void setTidur(String tidur) {
        this.tidur = tidur;
    }

    public String getStres() {
        return stres;
    }

    public void setStres(String stres) {
        this.stres = stres;
    }
}
