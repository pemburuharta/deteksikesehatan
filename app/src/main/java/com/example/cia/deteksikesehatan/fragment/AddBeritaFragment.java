package com.example.cia.deteksikesehatan.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.BeritaActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddBeritaFragment extends Fragment {


    private EditText addberitaEdtJudul;
    private EditText addberitaEdtDeskripsi;
    private ImageView addberitaIvThumbnail;
    private TextView addberitaTvThumbnail;
    private LinearLayout addberitaDivSimpan;

    SharedPref pref;
    ProgressDialog pd;

    File FileSatu;
    String NamaFoto;
    String STATUS;
    String id_news;

    public AddBeritaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_berita, container, false);
        initView(view);

        pref = new SharedPref(getActivity());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Proses...");
        pd.setCancelable(false);

        STATUS = getActivity().getIntent().getStringExtra("status");

        if (STATUS.equals("ADD_BERITA")) {

            addberitaIvThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 3);
                }
            });

            addberitaTvThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 3);
                }
            });

            addberitaDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!validasi()) {
                        return;
                    }

                    addBerita();

                }
            });

        } else {

            addberitaEdtJudul.setText(getActivity().getIntent().getStringExtra("judul"));
            addberitaEdtDeskripsi.setText(getActivity().getIntent().getStringExtra("isi"));
            NamaFoto = getActivity().getIntent().getStringExtra("gambar");
            id_news = getActivity().getIntent().getStringExtra("id_berita");

            Picasso.with(getActivity())
                    .load("http://dk.mitraredex.com/assets/gambar/"+getActivity().getIntent().getStringExtra("gambar"))
                    .into(addberitaIvThumbnail);

            addberitaIvThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 3);
                }
            });

            addberitaTvThumbnail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(intent, "Select File"), 3);
                }
            });

            addberitaDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (!validasi()) {
                        return;
                    }

                    editBerita();

                }
            });

        }

        return view;

    }

    private void editBerita() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.editNews(id_news,
                addberitaEdtJudul.getText().toString(),
                addberitaEdtDeskripsi.getText().toString(),
                NamaFoto).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                            Toast.makeText(getActivity(), "Berita Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Berita Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private boolean validasi() {

        if (addberitaEdtJudul.getText().toString().isEmpty()) {
            addberitaEdtJudul.requestFocus();
            addberitaEdtJudul.setError("Harus di isi");
            return false;
        }
        if (addberitaEdtDeskripsi.getText().toString().isEmpty()) {
            addberitaEdtDeskripsi.requestFocus();
            addberitaEdtDeskripsi.setError("Harus di isi");
            return false;
        }
        if (NamaFoto == null) {
            Toast.makeText(getActivity(), "Gambar Belum di isi", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    private void uploadGambar(String namaFoto) {

        pd.show();

        File f = new File(namaFoto);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);
        final MultipartBody.Part part = MultipartBody.Part.createFormData("image", f.getName(), requestFile);

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.newsUploadGambar(part).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){
                            NamaFoto = jsonObject.optString("image_name");
                            Toast.makeText(getActivity(), "Gambar Berhasil Diubah", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Gambar Gagal Diubah", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addBerita() {

        pd.show();

        File f = new File(NamaFoto);

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), f);
        final MultipartBody.Part part = MultipartBody.Part.createFormData("gambar", f.getName(), requestFile);

        RequestBody judul = RequestBody.create(MediaType.parse("text/plain"), addberitaEdtJudul.getText().toString());
        RequestBody isi = RequestBody.create(MediaType.parse("text/plain"), addberitaEdtDeskripsi.getText().toString());
        RequestBody id_user = RequestBody.create(MediaType.parse("text/plain"), pref.getIdUser());

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.newsApi(judul,
                isi,
                id_user,
                part).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")) {

                            Intent intent = new Intent(getActivity(), BeritaActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            getActivity().finish();
                            Toast.makeText(getActivity(), "Berita Berhasil Ditambahkan", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Berita Gagal Ditambahkan", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getActivity(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            System.out.println("Get the Image from data");

            // Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);

            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            NamaFoto = cursor.getString(columnIndex);


            cursor.close();

            // Set the Image in ImageView after decoding the String
            if (STATUS.equals("ADD_BERITA")){

                addberitaIvThumbnail.setImageBitmap(BitmapFactory.decodeFile(NamaFoto));

            } else {

                addberitaIvThumbnail.setImageBitmap(BitmapFactory.decodeFile(NamaFoto));
                uploadGambar(NamaFoto);

            }

        }
    }

    private void initView(View view) {
        addberitaEdtJudul = view.findViewById(R.id.addberita_edt_judul);
        addberitaEdtDeskripsi = view.findViewById(R.id.addberita_edt_deskripsi);
        addberitaIvThumbnail = view.findViewById(R.id.addberita_iv_thumbnail);
        addberitaTvThumbnail = view.findViewById(R.id.addberita_tv_thumbnail);
        addberitaDivSimpan = view.findViewById(R.id.addberita_div_simpan);
    }

}
