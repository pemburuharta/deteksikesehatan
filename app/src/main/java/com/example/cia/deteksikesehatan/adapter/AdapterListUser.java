package com.example.cia.deteksikesehatan.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.activity.DetailUserActivity;
import com.example.cia.deteksikesehatan.model.ListUserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AdapterListUser extends RecyclerView.Adapter<AdapterListUser.ViewHolder> {

    ArrayList<ListUserModel> dataList;
    Context context;
    String data;
    int size;
    SharedPref pref;

    public AdapterListUser(ArrayList<ListUserModel> dataList, Context context, String data) {
        this.dataList = dataList;
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        pref = new SharedPref(context);
//        Toast.makeText(context, ""+data, Toast.LENGTH_SHORT).show();
        if (pref.getRule().equals("ADMIN")){
            holder.listuserTvNama.setText(dataList.get(position).getTUFULLNAME());
            holder.listuserTvEmail.setText(dataList.get(position).getTUEMAIL());

            if (dataList.get(position).getTUAVATAR().equals("")){

                holder.listuserIvAvatar.setImageResource(R.drawable.ic_profil);

            } else {
                Picasso.with(context)
                        .load("http://dk.mitraredex.com/assets/gambar/"+dataList.get(position).getTUAVATAR())
                        .into(holder.listuserIvAvatar);
            }


            holder.listuserDivMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailUserActivity.class);
                    intent.putParcelableArrayListExtra("model", dataList);
                    intent.putExtra("posisi",""+position);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }else {
            for (int i = 0; i <dataList.size() ; i++) {
                if (dataList.get(i).getTUEMAIL().equals(pref.getEmail())){
                    holder.listuserTvNama.setText(dataList.get(i).getTUFULLNAME());
                    holder.listuserTvEmail.setText(dataList.get(i).getTUEMAIL());

                    final int po = i;

                    if (dataList.get(i).getTUAVATAR().equals("")){

                        holder.listuserIvAvatar.setImageResource(R.drawable.ic_profil);

                    } else {
                        Picasso.with(context)
                                .load("http://dk.mitraredex.com/assets/gambar/"+dataList.get(i).getTUAVATAR())
                                .into(holder.listuserIvAvatar);
                    }


                    holder.listuserDivMain.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, DetailUserActivity.class);
                            intent.putParcelableArrayListExtra("model", dataList);
                            intent.putExtra("posisi",""+po);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }
                    });
                }else {

                }
            }
        }


    }

    @Override
    public int getItemCount() {
        if (data.equals("admin")){

            size = dataList.size();
        }else {
            size = 1;
        }
        return size;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout listuserDivMain;
        private TextView listuserTvNama;
        private TextView listuserTvEmail;
        private CircleImageView listuserIvAvatar;

        public ViewHolder(View itemView) {
            super(itemView);
            listuserDivMain = itemView.findViewById(R.id.listuser_div_main);
            listuserTvNama = itemView.findViewById(R.id.listuser_tv_nama);
            listuserTvEmail = itemView.findViewById(R.id.listuser_tv_email);
            listuserIvAvatar = itemView.findViewById(R.id.listuser_iv_avatar);
        }
    }
}
