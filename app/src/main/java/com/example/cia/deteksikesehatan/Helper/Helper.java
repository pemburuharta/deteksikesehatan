package com.example.cia.deteksikesehatan.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helper {

    public Helper(){

    }


    public String convertDateFormat(String date, String frmtlama) {

        String hasil= "";

        final String formatBaru = "dd MMMM yyyy";

        SimpleDateFormat dateFormat = new SimpleDateFormat(frmtlama);
        try {
            Date dd = dateFormat.parse(date);
            dateFormat.applyPattern(formatBaru);
            hasil = dateFormat.format(dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return hasil;

    }

}
