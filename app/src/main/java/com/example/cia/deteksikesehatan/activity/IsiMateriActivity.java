package com.example.cia.deteksikesehatan.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.Helper.Helper;
import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.squareup.picasso.Picasso;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class IsiMateriActivity extends AppCompatActivity {

    private ImageView img;
    private TextView tvJudul;
    private TextView tvIsi;
    private TextView tvCreatedAt;
    SharedPref pref;
    String judul, isi, gambar, created;
    String id_kat, id_materi;
    private LinearLayout tvLihatSoal;
    private ImageView isimateriIvClose;
    private TextView isimateriTvActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_isi_materi);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        judul = getIntent().getStringExtra("judul");
        isi = getIntent().getStringExtra("isi");
        gambar = getIntent().getStringExtra("image");
        created = getIntent().getStringExtra("created_at");
        id_kat = getIntent().getStringExtra("id_kategori");
        id_materi = getIntent().getStringExtra("id_materi");

        isimateriIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        isimateriTvActionbar.setText(judul);

        Picasso.with(getApplicationContext())
                .load("http://dk.mitraredex.com/assets/gambar/" + gambar)
                .into(img);

        tvJudul.setText("" + judul);
        tvIsi.setText("" + isi);
        tvCreatedAt.setText("" + new Helper().convertDateFormat(created, "yyyy-MM-dd hh:mm:s"));

        tvLihatSoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SoalActivity.class);
                intent.putExtra("id_materi", id_materi);
                intent.putExtra("id_kategori", id_kat);
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        img = (ImageView) findViewById(R.id.img);
        tvJudul = (TextView) findViewById(R.id.tv_judul);
        tvIsi = (TextView) findViewById(R.id.tv_isi);
        tvCreatedAt = findViewById(R.id.tv_created_at);
        tvLihatSoal = (LinearLayout) findViewById(R.id.div_lihat_soal);
        isimateriIvClose = findViewById(R.id.isimateri_iv_close);
        isimateriTvActionbar = findViewById(R.id.isimateri_tv_actionbar);
    }
}
