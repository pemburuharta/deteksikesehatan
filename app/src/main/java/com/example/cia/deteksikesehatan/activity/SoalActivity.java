package com.example.cia.deteksikesehatan.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.MainActivity;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.adapter.SoalAdapter;
import com.example.cia.deteksikesehatan.model.PayLoadSoalModel;
import com.example.cia.deteksikesehatan.model.SoalModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SoalActivity extends AppCompatActivity {

    String id_kat, id_materi;
    private RecyclerView rv;
    private Button btnJawab;
    SoalAdapter soalAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<SoalModel> list;
    int hasil = 0;
    private ImageView soalIvClose;
    private TextView soalTvActionbar;
    SharedPref pref;
    List<String> jawaban = new ArrayList<>();
    List<String> idSoal = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);

        if (pref.getRule().equals("ADMIN")){
            btnJawab.setVisibility(View.GONE);
        }else {
            btnJawab.setVisibility(View.VISIBLE);
        }

        soalIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        id_kat = getIntent().getStringExtra("id_kategori");
//        id_materi = getIntent().getStringExtra("id_materi");

        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        getSoalDua();

        btnJawab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getJawabanDua();
            }
        });
    }

    public void getJawaban() {
        String[] jawabanAkhir = ((SoalAdapter) rv.getAdapter()).jawaban;
        String[] id = ((SoalAdapter) rv.getAdapter()).idSoal;
        for (int x = 0; x < jawabanAkhir.length; x++) {
//            Toast.makeText(this, ""+list.get(x).getTQTRUE(), Toast.LENGTH_SHORT).show();
            jawaban.add(jawabanAkhir[x]);
        }

        for (int i = 0; i <id.length ; i++) {
            idSoal.add(id[i]);
        }

//        Toast.makeText(this, "nilai ku " + hasil, Toast.LENGTH_SHORT).show();
//        hasil = 0;
        jawabSoal();
    }

    public void getJawabanDua() {
        String[] jawabanAkhir = ((SoalAdapter) rv.getAdapter()).jawaban;
        String[] id = ((SoalAdapter) rv.getAdapter()).idSoal;
        for (int x = 0; x < jawabanAkhir.length; x++) {
//            Toast.makeText(this, ""+list.get(x).getTQTRUE(), Toast.LENGTH_SHORT).show();
            if (jawabanAkhir[x].equals(list.get(x).getTQTRUE())){
                hasil = hasil + 10;
            }
        }

        final AlertDialog.Builder dialog = new AlertDialog.Builder(SoalActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Nilai Anda "+hasil);
        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onBackPressed();
            }
        });

        AlertDialog alert = dialog.create();
        alert.show();
    }

    private void jawabSoal(){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.jawabSoal("jawabsoal",
                pref.getIdUser(),
                id_kat,
                id_materi,
                jawaban,
                idSoal).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("Anda sudah menyelesaikan materi ini,silahkan lanjutkan kemateri selanjutnya")){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(SoalActivity.this);
                            dialog.setCancelable(true);
                            dialog.setMessage(""+msg);
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    startActivity(new Intent(getApplicationContext(), MainUserActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            });

                            AlertDialog alert = dialog.create();
                            alert.show();
                        }else if (msg.equals("Jawaban anda belum sepenuhnya cocok")){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(SoalActivity.this);
                            dialog.setCancelable(true);
                            dialog.setMessage(""+msg);
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    onBackPressed();
                                }
                            });

                            AlertDialog alert = dialog.create();
                            alert.show();
                        }else {
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(SoalActivity.this);
                            dialog.setCancelable(true);
                            dialog.setMessage(""+msg);
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    startActivity(new Intent(getApplicationContext(), MainUserActivity.class)
                                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
                                    finish();
                                }
                            });

                            AlertDialog alert = dialog.create();
                            alert.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SoalActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSoal() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getSoal(id_kat, id_materi).enqueue(new Callback<PayLoadSoalModel>() {
            @Override
            public void onResponse(Call<PayLoadSoalModel> call, Response<PayLoadSoalModel> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    if (response.body().getMsg().equals("list data soal permateri")) {
                        list = response.body().getPayload();
                        soalAdapter = new SoalAdapter(list, getApplicationContext());
                        rv.setAdapter(soalAdapter);
                    } else {
                        Toast.makeText(SoalActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PayLoadSoalModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SoalActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getSoalDua() {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getSoalDua(id_kat).enqueue(new Callback<PayLoadSoalModel>() {
            @Override
            public void onResponse(Call<PayLoadSoalModel> call, Response<PayLoadSoalModel> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    if (response.body().getMsg().equals("list data soal permateri")) {
                        list = response.body().getPayload();
                        soalAdapter = new SoalAdapter(list, getApplicationContext());
                        rv.setAdapter(soalAdapter);
                    } else {
                        Toast.makeText(SoalActivity.this, "" + response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PayLoadSoalModel> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(SoalActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        getSoalDua();
    }

    private void initView() {
        rv = (RecyclerView) findViewById(R.id.rv);
        btnJawab = (Button) findViewById(R.id.btn_jawab);
        soalIvClose = findViewById(R.id.soal_iv_close);
        soalTvActionbar = findViewById(R.id.soal_tv_actionbar);
    }
}
