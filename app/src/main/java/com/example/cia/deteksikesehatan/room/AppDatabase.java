package com.example.cia.deteksikesehatan.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {Quesioner.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract QuesionerDAO quesionerDAO();
}
