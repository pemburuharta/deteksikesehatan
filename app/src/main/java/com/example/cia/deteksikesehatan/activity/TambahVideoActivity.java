package com.example.cia.deteksikesehatan.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahVideoActivity extends AppCompatActivity {

    SharedPref pref;
    ProgressDialog pd;

    String idVideo;
    private EditText addvideoEdtNama;
    private EditText addvideoEdtLink;
    private LinearLayout addvideoDivSimpan;
    private ImageView addvideoIvClose;
    private TextView addvideoTvActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_video);
        getSupportActionBar().hide();
        initView();

        addvideoIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);

        if (getIntent().getStringExtra("status").equals("ADD_VIDEO")) {

            addvideoDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addVideo();
                }
            });

        } else {

            addvideoEdtNama.setText(getIntent().getStringExtra("judul"));
            addvideoEdtLink.setText(getIntent().getStringExtra("file"));
            idVideo = getIntent().getStringExtra("id_video");

            addvideoDivSimpan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editVideo();
                }
            });

        }

    }

    private void editVideo() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.editVideoApi("edit",
                idVideo,
                addvideoEdtNama.getText().toString(),
                addvideoEdtLink.getText().toString(),
                pref.getIdUser()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")) {

                            Toast.makeText(getApplicationContext(), "Berhasil Menambahkan Video", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(getApplicationContext(), BeritaActivity.class);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                            finish();
                            onBackPressed();

                        } else {
                            Toast.makeText(getApplicationContext(), "Gagal Menambahkan Video", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addVideo() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.videoApi("add",
                addvideoEdtNama.getText().toString(),
                addvideoEdtLink.getText().toString(),
                pref.getIdUser()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")) {

                            Toast.makeText(getApplicationContext(), "Berhasil Menambahkan Video", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), "Gagal Menambahkan Video", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        addvideoEdtNama = findViewById(R.id.addvideo_edt_nama);
        addvideoEdtLink = findViewById(R.id.addvideo_edt_link);
        addvideoDivSimpan = findViewById(R.id.addvideo_div_simpan);
        addvideoIvClose = findViewById(R.id.addvideo_iv_close);
        addvideoTvActionbar = findViewById(R.id.addvideo_tv_actionbar);
    }
}
