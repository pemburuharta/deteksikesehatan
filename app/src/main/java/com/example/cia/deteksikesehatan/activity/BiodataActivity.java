package com.example.cia.deteksikesehatan.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BiodataActivity extends AppCompatActivity {

    private ImageView biodataIvClose;
    private TextView beritaTvTitle;
    private EditText biodataEdtFullname;
    private EditText biodataEdtTelpon;
    private EditText biodataEdtEmail;
    private EditText biodataEdtUmur;
    private EditText biodataEdtTempatlahir;
    private LinearLayout biodataDivTanggallahir;
    private TextView biodataTvTanggallahir;
    private RadioButton biodataRbLaki;
    private RadioButton biodataRbPerempuan;
    private EditText biodataEdtKota;
    private EditText biodataEdtKecamatan;
    private EditText biodataEdtKelurahan;
    private EditText biodataEdtRt;
    private EditText biodataEdtRw;
    private EditText biodataEdtSekolah;
    private EditText biodataEdtKelas;
    private RadioButton biodataRbPndkayah1;
    private RadioButton biodataRbPndkayah2;
    private RadioButton biodataRbPndkayah3;
    private RadioButton biodataRbPndkayah4;
    private RadioButton biodataRbPndkayah5;
    private RadioButton biodataRbPrkjayah1;
    private RadioButton biodataRbPrkjayah2;
    private RadioButton biodataRbPrkjayah3;
    private RadioButton biodataRbPrkjayah4;
    private RadioButton biodataRbPndkibu1;
    private RadioButton biodataRbPndkibu2;
    private RadioButton biodataRbPndkibu3;
    private RadioButton biodataRbPndkibu4;
    private RadioButton biodataRbPndkibu5;
    private RadioButton biodataRbPrkjibu1;
    private RadioButton biodataRbPrkjibu2;
    private RadioButton biodataRbPrkjibu3;
    private RadioButton biodataRbPrkjibu4;
    private RadioButton biodataRbPendapatan1;
    private RadioButton biodataRbPendapatan2;
    private RadioButton biodataRbPendapatan3;
    private Button biodataBtnSimpan;

    SharedPref pref;
    ProgressDialog pd;
    Calendar myCalendar;

    String NamaLengkap;
    String NomorHp;
    String Email;
    String Umur;
    String TempatLahir;
    String TanggalLahir;
    String Jenis;
    String Kota;
    String Kecamatan;
    String Kelurahan;
    String RT;
    String RW;
    String NamaSekolah;
    String Kelas;
    String PndkAyah;
    String PkrjAyah;
    String PndkIbu;
    String PkrjIbu;
    String Pendapatan;
    private EditText edtBerat;
    private EditText edtTinggi;
    private EditText biodataEdtLingkarPerut;
    private EditText edtSistolik;
    private EditText edtDiastolik;
    private RadioButton rbEkstraKarangTaruna;
    private RadioButton rbEkstraRemajaMasjid;
    private RadioButton rbEkstraOsiss;
    private RadioButton rbEkstraPramuka;
    private RadioButton rbEkstraLsm;
    private RadioButton rbEkstraKursus;
    private RadioButton rbEkstraOlahraga;
    private RadioButton rbEkstraPmr;
    private RadioButton rbEkstraUks;
    private RadioButton rbEkstraKir;
    private RadioButton rbEkstraTidak;
    private RadioGroup rgAplikasi;
    private RadioButton rbAplikasi1;
    private RadioButton rbAplikasi2;
    private RadioButton rbPencegahanSeminar;
    private RadioButton rbPencegahanTalkShow;
    private RadioButton rbPencegahanKampanye;
    private RadioButton rbPencegahanPenyuluhan;
    private RadioButton rbPencegahanTidak;
    private RadioButton rbInformasiPetugas;
    private RadioButton rbInformasiKeluarga;
    private RadioButton rbInformasiBuku;
    private RadioButton rbInformasiSekolah;
    private RadioButton rbInformasiInternet;
    private RadioButton rbInformasiTv;
    private RadioButton rbInformasiRadio;
    private RadioButton rbInformasiTidak;
    private RadioButton rbKeluargaAyah;
    private RadioButton rbKeluargaIbu;
    private RadioButton rbKeluargaAyahIbu;
    private RadioButton rbKeluargTidak;
    private RadioButton rbPriksaTekananDarah;
    private RadioButton rbPriksaGulaDarah;
    private RadioButton rbPriksaKolesterol;
    private RadioButton rbPriksaTidak;
    private RadioGroup rgRokok;
    private RadioButton rbRokokYa;
    private RadioButton rbRokokTidak;
    private EditText edtLamaMerokok;
    private RadioGroup rgRokokPasif;
    private RadioButton rbRokokPasifYa;
    private RadioButton rbRokokPasifTidak;
    private RadioGroup rgAlkohol;
    private RadioButton rbAlkoholYa;
    private RadioButton rbAlkoholTidak;
    private EditText edtLamaAlkohol;
    private RadioButton rbOlahragaJoging;
    private RadioButton rbOlahragaRenang;
    private RadioButton rbOlahragaSepakBola;
    private RadioButton rbOlahragaBasket;
    private RadioButton rbOlahragaBadminton;
    private RadioButton rbOlahragaFitnes;
    private RadioButton rbOlahragaFutsal;
    private RadioButton rbOlahragaKarate;
    private RadioButton rbOlahragaTenis;
    private RadioButton rbOlahragaTidak;
    private RadioButton rbMakanMi;
    private RadioButton rbMakanHamburger;
    private RadioButton rbMakanPizza;
    private RadioButton rbMakanHotDog;
    private RadioButton rbMakanKentang;
    private RadioButton rbMakanNugget;
    private RadioButton rbMakanCilok;
    private RadioButton rbMakanSosis;
    private RadioButton rbMakanTidak;
    private RadioGroup rgMinum;
    private RadioButton rbMinumYa;
    private RadioButton rbMinumTidak;
    private RadioGroup rgIstirahat;
    private RadioButton rbIstirahatYa;
    private RadioButton rbIstirahatTidak;
    private RadioButton rbStresMendekatkanDiri;
    private RadioButton rbStresTerbuka;
    private RadioButton rbStresPikirPositif;
    private RadioButton rbStresMenyalurkanHobi;
    private RadioButton rbStresMenyendiri;
    private RadioButton rbStresMerenung;
    private RadioButton rbStresMenjauh;
    private RadioButton rbStresMerokok;
    private RadioButton rbStresTidak;
    private EditText biodataEdtGlukosaDarah;

    List<String> listEkstra = new ArrayList<>();
    List<String> listPencegahan = new ArrayList<>();
    List<String> listInformasi = new ArrayList<>();
    List<String> listKeluargaDM = new ArrayList<>();
    List<String> listPeriksa = new ArrayList<>();
    List<String> listOlahraga = new ArrayList<>();
    List<String> listMakan = new ArrayList<>();
    List<String> listStres = new ArrayList<>();
    List<String> listRokok = new ArrayList<>();
    List<String> listAlkohol = new ArrayList<>();
    List<String>  rokokPasif = new ArrayList<>();
    List<String>  minum = new ArrayList<>();
    List<String>  istirahat = new ArrayList<>();
    List<String> menggunakanAplikasi = new ArrayList<>();

    String  merokok, lamaMerok,
            alkohol, lamaAlkohol;
    String berat,tinggi,lingkar,glukosa,sistolik,diastolik;
    private RadioButton rbMakanEskrim;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_biodata);
        getSupportActionBar().hide();
        initView();

        biodataIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        myCalendar = Calendar.getInstance();
        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setCancelable(false);
        pd.setMessage("Loading...");

        biodataEdtFullname.setText(pref.getFullname());
        biodataEdtTelpon.setText(pref.getNotelp());
        biodataEdtEmail.setText(pref.getEmail());
        biodataEdtTempatlahir.setText(pref.getTempatLahir());
        biodataTvTanggallahir.setText(pref.getTanggalLahir());

        TanggalLahir = pref.getTanggalLahir();

        biodataDivTanggallahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(BiodataActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        myCalendar.set(Calendar.YEAR, i);
                        myCalendar.set(Calendar.MONTH, i1);
                        myCalendar.set(Calendar.DAY_OF_MONTH, i2);

                        String fromTanggal = "yyyy-MM-dd";
                        SimpleDateFormat dateFormat = new SimpleDateFormat(fromTanggal);
                        biodataTvTanggallahir.setText(dateFormat.format(myCalendar.getTime()));

                        TanggalLahir = dateFormat.format(myCalendar.getTime());
                    }
                },
                        myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        biodataBtnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                simpanData();
            }
        });

        rbEkstraTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbEkstraKarangTaruna.setChecked(false);
                rbEkstraRemajaMasjid.setChecked(false);
                rbEkstraOsiss.setChecked(false);
                rbEkstraPramuka.setChecked(false);
                rbEkstraLsm.setChecked(false);
                rbEkstraKursus.setChecked(false);
                rbEkstraOlahraga.setChecked(false);
                rbEkstraPmr.setChecked(false);
                rbEkstraUks.setChecked(false);
                rbEkstraKir.setChecked(false);
            }
        });

        rbPencegahanTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbPencegahanSeminar.setChecked(false);
                rbPencegahanTalkShow.setChecked(false);
                rbPencegahanKampanye.setChecked(false);
                rbPencegahanPenyuluhan.setChecked(false);
            }
        });

        rbInformasiTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbInformasiPetugas.setChecked(false);
                rbInformasiKeluarga.setChecked(false);
                rbInformasiBuku.setChecked(false);
                rbInformasiSekolah.setChecked(false);
                rbInformasiInternet.setChecked(false);
                rbInformasiTv.setChecked(false);
                rbInformasiRadio.setChecked(false);
            }
        });

        rbKeluargTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbKeluargaAyah.setChecked(false);
                rbKeluargaIbu.setChecked(false);
                rbKeluargaAyahIbu.setChecked(false);
            }
        });

        rbPriksaTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbPriksaTekananDarah.setChecked(false);
                rbPriksaGulaDarah.setChecked(false);
                rbPriksaKolesterol.setChecked(false);
            }
        });

        rbOlahragaTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbOlahragaJoging.setChecked(false);
                rbOlahragaRenang.setChecked(false);
                rbOlahragaSepakBola.setChecked(false);
                rbOlahragaBasket.setChecked(false);
                rbOlahragaBadminton.setChecked(false);
                rbOlahragaFitnes.setChecked(false);
                rbOlahragaFutsal.setChecked(false);
                rbOlahragaKarate.setChecked(false);
                rbOlahragaTenis.setChecked(false);
            }
        });

        rbMakanTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbMakanMi.setChecked(false);
                rbMakanHamburger.setChecked(false);
                rbMakanPizza.setChecked(false);
                rbMakanHotDog.setChecked(false);
                rbMakanKentang.setChecked(false);
                rbMakanEskrim.setChecked(false);
                rbMakanNugget.setChecked(false);
                rbMakanCilok.setChecked(false);
                rbMakanSosis.setChecked(false);
            }
        });

        rbStresTidak.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                rbStresMendekatkanDiri.setChecked(false);
                rbStresTerbuka.setChecked(false);
                rbStresPikirPositif.setChecked(false);
                rbStresMenyalurkanHobi.setChecked(false);
                rbStresMenyendiri.setChecked(false);
                rbStresMerenung.setChecked(false);
                rbStresMenjauh.setChecked(false);
                rbStresMerokok.setChecked(false);
            }
        });
    }

    private void simpanData() {
        NamaLengkap = biodataEdtFullname.getText().toString();
        NomorHp = biodataEdtTelpon.getText().toString();
        Email = biodataEdtEmail.getText().toString();
        Umur = biodataEdtUmur.getText().toString();
        TempatLahir = biodataEdtTempatlahir.getText().toString();
        Kota = biodataEdtKota.getText().toString();
        Kecamatan = biodataEdtKecamatan.getText().toString();
        Kelurahan = biodataEdtKelurahan.getText().toString();
        RT = biodataEdtRt.getText().toString();
        RW = biodataEdtRw.getText().toString();
        NamaSekolah = biodataEdtSekolah.getText().toString();
        Kelas = biodataEdtKelas.getText().toString();

        /** Gender **/
        if (biodataRbLaki.isChecked()) {
            Jenis = "1";
        }
        if (biodataRbPerempuan.isChecked()) {
            Jenis = "0";
        }

        /** Pendidikan Ayah **/
        if (biodataRbPndkayah1.isChecked()) {
            PndkAyah = "1";
        }
        if (biodataRbPndkayah2.isChecked()) {
            PndkAyah = "2";
        }
        if (biodataRbPndkayah3.isChecked()) {
            PndkAyah = "3";
        }
        if (biodataRbPndkayah4.isChecked()) {
            PndkAyah = "4";
        }
        if (biodataRbPndkayah5.isChecked()) {
            PndkAyah = "5";
        }

        /** Pekerjaan Ayah **/
        if (biodataRbPrkjayah1.isChecked()) {
            PkrjAyah = "1";
        }
        if (biodataRbPrkjayah2.isChecked()) {
            PkrjAyah = "2";
        }
        if (biodataRbPrkjayah3.isChecked()) {
            PkrjAyah = "3";
        }
        if (biodataRbPrkjayah4.isChecked()) {
            PkrjAyah = "4";
        }

        /** Pendidikan Ibu **/
        if (biodataRbPndkibu1.isChecked()) {
            PndkIbu = "1";
        }
        if (biodataRbPndkibu2.isChecked()) {
            PndkIbu = "2";
        }
        if (biodataRbPndkibu3.isChecked()) {
            PndkIbu = "3";
        }
        if (biodataRbPndkibu4.isChecked()) {
            PndkIbu = "4";
        }
        if (biodataRbPndkibu5.isChecked()) {
            PndkIbu = "5";
        }

        /** Pekerjaan Ibu **/
        if (biodataRbPrkjibu1.isChecked()) {
            PkrjIbu = "1";
        }
        if (biodataRbPrkjibu2.isChecked()) {
            PkrjIbu = "2";
        }
        if (biodataRbPrkjibu3.isChecked()) {
            PkrjIbu = "3";
        }
        if (biodataRbPrkjibu4.isChecked()) {
            PkrjIbu = "4";
        }

        /** Pendapatan **/
        if (biodataRbPendapatan1.isChecked()) {
            Pendapatan = "1";
        }
        if (biodataRbPendapatan2.isChecked()) {
            Pendapatan = "2";
        }
        if (biodataRbPendapatan3.isChecked()) {
            Pendapatan = "3";
        }
        ambilEkstra();
        ambilAplikasi();
        ambilPencegahan();
        ambilInformasi();
        ambilKeluarga();
        ambilPeriksa();
        ambilRokok();
        ambilAlkohol();
        ambilOlahraga();
        ambilMakan();
        ambilMinumIstirahat();
        ambilStres();
        ambilKesehatan();

        simpanBiodata();
    }

    private void ambilEkstra() {
        if (rbEkstraKarangTaruna.isChecked()) {
            listEkstra.add(rbEkstraKarangTaruna.getText().toString());
        }
        if (rbEkstraRemajaMasjid.isChecked()) {
            listEkstra.add(rbEkstraRemajaMasjid.getText().toString());
        }
        if (rbEkstraOsiss.isChecked()) {
            listEkstra.add(rbEkstraOsiss.getText().toString());
        }
        if (rbEkstraPramuka.isChecked()) {
            listEkstra.add(rbEkstraPramuka.getText().toString());
        }
        if (rbEkstraLsm.isChecked()) {
            listEkstra.add(rbEkstraLsm.getText().toString());
        }
        if (rbEkstraKursus.isChecked()) {
            listEkstra.add(rbEkstraKursus.getText().toString());
        }
        if (rbEkstraOlahraga.isChecked()) {
            listEkstra.add(rbEkstraOlahraga.getText().toString());
        }
        if (rbEkstraPmr.isChecked()) {
            listEkstra.add(rbEkstraPmr.getText().toString());
        }
        if (rbEkstraUks.isChecked()) {
            listEkstra.add(rbEkstraUks.getText().toString());
        }
        if (rbEkstraKir.isChecked()) {
            listEkstra.add(rbEkstraKir.getText().toString());
        }


        if (rbEkstraTidak.isChecked()) {
            listEkstra.removeAll(listStres);

            listEkstra.add(rbEkstraTidak.getText().toString());
        }
    }

    private void ambilAplikasi() {
        RadioButton rbAplik = findViewById(rgAplikasi.getCheckedRadioButtonId());

        menggunakanAplikasi.add(rbAplik.getText().toString());
    }

    private void ambilPencegahan() {
        if (rbPencegahanSeminar.isChecked()) {
            listPencegahan.add(rbPencegahanSeminar.getText().toString());
        }
        if (rbPencegahanTalkShow.isChecked()) {
            listPencegahan.add(rbPencegahanTalkShow.getText().toString());
        }
        if (rbPencegahanKampanye.isChecked()) {
            listPencegahan.add(rbPencegahanKampanye.getText().toString());
        }
        if (rbPencegahanPenyuluhan.isChecked()) {
            listPencegahan.add(rbPencegahanPenyuluhan.getText().toString());
        }


        if (rbPencegahanTidak.isChecked()) {
            listPencegahan.removeAll(listPencegahan);
            listPencegahan.add(rbPencegahanTidak.getText().toString());
        }
    }

    private void ambilInformasi() {
        if (rbInformasiPetugas.isChecked()) {
            listInformasi.add(rbInformasiPetugas.getText().toString());
        }
        if (rbInformasiKeluarga.isChecked()) {
            listInformasi.add(rbInformasiKeluarga.getText().toString());
        }
        if (rbInformasiBuku.isChecked()) {
            listInformasi.add(rbInformasiBuku.getText().toString());
        }
        if (rbInformasiSekolah.isChecked()) {
            listInformasi.add(rbInformasiSekolah.getText().toString());
        }
        if (rbInformasiInternet.isChecked()) {
            listInformasi.add(rbInformasiInternet.getText().toString());
        }
        if (rbInformasiTv.isChecked()) {
            listInformasi.add(rbInformasiTv.getText().toString());
        }
        if (rbInformasiRadio.isChecked()) {
            listInformasi.add(rbInformasiRadio.getText().toString());
        }

        if (rbInformasiTidak.isChecked()) {
            listInformasi.removeAll(listInformasi);

            listInformasi.add(rbInformasiTidak.getText().toString());
        }
    }

    private void ambilKeluarga() {
        if (rbKeluargaAyah.isChecked()) {
            listKeluargaDM.add(rbKeluargaAyah.getText().toString());
        }
        if (rbKeluargaIbu.isChecked()) {
            listKeluargaDM.add(rbKeluargaIbu.getText().toString());
        }
        if (rbKeluargaAyahIbu.isChecked()) {
            listKeluargaDM.add(rbKeluargaAyahIbu.getText().toString());
        }


        if (rbKeluargTidak.isChecked()) {
            listKeluargaDM.removeAll(listKeluargaDM);

            listKeluargaDM.add(rbKeluargTidak.getText().toString());
        }
    }

    private void ambilPeriksa() {
        if (rbPriksaTekananDarah.isChecked()) {
            listPeriksa.add(rbPriksaTekananDarah.getText().toString());
        }
        if (rbPriksaGulaDarah.isChecked()) {
            listPeriksa.add(rbPriksaGulaDarah.getText().toString());
        }
        if (rbPriksaKolesterol.isChecked()) {
            listPeriksa.add(rbPriksaKolesterol.getText().toString());
        }

        if (rbPriksaTidak.isChecked()) {
            listPeriksa.removeAll(listPeriksa);

            listPeriksa.add(rbPriksaTidak.getText().toString());
        }
    }

    private void ambilRokok() {
        RadioButton rbRokok = findViewById(rgRokok.getCheckedRadioButtonId());
        merokok = rbRokok.getText().toString();
        lamaMerok = edtLamaMerokok.getText().toString() + " Tahun";

        RadioButton rbRokokPasif = findViewById(rgRokokPasif.getCheckedRadioButtonId());
        rokokPasif.add(rbRokokPasif.getText().toString());

        listRokok.add(merokok);
        listRokok.add(lamaMerok);
    }

    private void ambilAlkohol() {
        RadioButton rbAlkohol = findViewById(rgAlkohol.getCheckedRadioButtonId());
        alkohol = rbAlkohol.getText().toString();

        lamaAlkohol = edtLamaAlkohol.getText().toString() +" Tahun";

        listAlkohol.add(alkohol);
        listAlkohol.add(lamaAlkohol);
    }

    private void ambilOlahraga() {
        if (rbOlahragaJoging.isChecked()) {
            listOlahraga.add(rbOlahragaJoging.getText().toString());
        }
        if (rbOlahragaRenang.isChecked()) {
            listOlahraga.add(rbOlahragaRenang.getText().toString());
        }
        if (rbOlahragaSepakBola.isChecked()) {
            listOlahraga.add(rbOlahragaSepakBola.getText().toString());
        }
        if (rbOlahragaBasket.isChecked()) {
            listOlahraga.add(rbOlahragaBasket.getText().toString());
        }
        if (rbOlahragaBadminton.isChecked()) {
            listOlahraga.add(rbOlahragaBadminton.getText().toString());
        }
        if (rbOlahragaFitnes.isChecked()) {
            listOlahraga.add(rbOlahragaFitnes.getText().toString());
        }
        if (rbOlahragaFutsal.isChecked()) {
            listOlahraga.add(rbOlahragaFutsal.getText().toString());
        }
        if (rbOlahragaKarate.isChecked()) {
            listOlahraga.add(rbOlahragaKarate.getText().toString());
        }
        if (rbOlahragaTenis.isChecked()) {
            listOlahraga.add(rbOlahragaTenis.getText().toString());
        }

        if (rbOlahragaTidak.isChecked()) {
            listOlahraga.removeAll(listOlahraga);

            listOlahraga.add(rbOlahragaTidak.getText().toString());
        }
    }

    private void ambilMakan() {
        if (rbMakanMi.isChecked()) {
            listMakan.add(rbMakanMi.getText().toString());
        }
        if (rbMakanHamburger.isChecked()) {
            listMakan.add(rbMakanHamburger.getText().toString());
        }
        if (rbMakanPizza.isChecked()) {
            listMakan.add(rbMakanPizza.getText().toString());
        }
        if (rbMakanHotDog.isChecked()) {
            listMakan.add(rbMakanHotDog.getText().toString());
        }
        if (rbMakanKentang.isChecked()) {
            listMakan.add(rbMakanKentang.getText().toString());
        }
        if (rbMakanEskrim.isChecked()) {
            listMakan.add(rbMakanEskrim.getText().toString());
        }
        if (rbMakanNugget.isChecked()) {
            listMakan.add(rbMakanNugget.getText().toString());
        }
        if (rbMakanCilok.isChecked()) {
            listMakan.add(rbMakanCilok.getText().toString());
        }
        if (rbMakanSosis.isChecked()) {
            listMakan.add(rbMakanSosis.getText().toString());
        }

        if (rbMakanTidak.isChecked()) {
            listMakan.removeAll(listMakan);

            listMakan.add(rbMakanTidak.getText().toString());
        }
    }

    private void ambilMinumIstirahat(){
        RadioButton rbMinum = findViewById(rgMinum.getCheckedRadioButtonId());
        minum.add(rbMinum.getText().toString());

        RadioButton rbIstirahan = findViewById(rgIstirahat.getCheckedRadioButtonId());
        istirahat.add(rbIstirahan.getText().toString());
    }

    private void ambilStres(){
        if (rbStresMendekatkanDiri.isChecked()) {
            listStres.add(rbStresMendekatkanDiri.getText().toString());
        }
        if (rbStresTerbuka.isChecked()) {
            listStres.add(rbStresTerbuka.getText().toString());
        }
        if (rbStresPikirPositif.isChecked()) {
            listStres.add(rbStresPikirPositif.getText().toString());
        }
        if (rbStresMenyalurkanHobi.isChecked()) {
            listStres.add(rbStresMenyalurkanHobi.getText().toString());
        }
        if (rbStresMenyendiri.isChecked()) {
            listStres.add(rbStresMenyendiri.getText().toString());
        }
        if (rbStresMerenung.isChecked()) {
            listStres.add(rbStresMerenung.getText().toString());
        }
        if (rbStresMenjauh.isChecked()) {
            listStres.add(rbStresMenjauh.getText().toString());
        }
        if (rbStresMerokok.isChecked()) {
            listStres.add(rbStresMerokok.getText().toString());
        }
        if (rbStresTidak.isChecked()) {
            listStres.removeAll(listStres);

            listStres.add(rbStresTidak.getText().toString());
        }
    }

    private void ambilKesehatan(){
        berat = edtBerat.getText().toString();
        tinggi = edtTinggi.getText().toString();
        sistolik = edtSistolik.getText().toString();
        diastolik = edtDiastolik.getText().toString();
        lingkar = biodataEdtLingkarPerut.getText().toString();
        glukosa = biodataEdtGlukosaDarah.getText().toString();
    }

    private void simpanBiodata() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.biodata(pref.getIdUser(),
                NamaLengkap,
                NomorHp,
                Email,
                Umur,
                TempatLahir,
                TanggalLahir,
                Kota,
                Kecamatan,
                Kelurahan,
                RT, RW,
                NamaSekolah,
                Kelas,
                PndkAyah,
                PkrjAyah,
                PndkIbu,
                PkrjIbu,
                Pendapatan,
                listEkstra,
                menggunakanAplikasi,
                listPencegahan,
                listInformasi,
                listKeluargaDM,
                listPeriksa,
                listRokok,
                rokokPasif,
                listAlkohol,
                listOlahraga,
                listMakan,
                minum,
                istirahat,
                listStres,
                berat,
                tinggi,
                lingkar,
                sistolik,
                diastolik,
                glukosa).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")) {

                            startActivity(new Intent(getApplicationContext(), MainUserActivity.class));
                            Toast.makeText(BiodataActivity.this, "Berhasil Menambahkan Biodata", Toast.LENGTH_SHORT).show();

                        } else {

                            String msg = jsonObject.optString("msg");

                            Toast.makeText(BiodataActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(BiodataActivity.this, "Periksa Koneksi Anda ...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(BiodataActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Anda Harus Melengkapi Data Diri");
        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                Intent startMain = new Intent(Intent.ACTION_MAIN);
//                startMain.addCategory(Intent.CATEGORY_HOME);
//                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(startMain);
            }
        });
//
//        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//            }
//        });
        AlertDialog alert = dialog.create();
        alert.show();
    }

    private void initView() {
        biodataIvClose = findViewById(R.id.biodata_iv_close);
        beritaTvTitle = findViewById(R.id.berita_tv_title);
        biodataEdtFullname = findViewById(R.id.biodata_edt_fullname);
        biodataEdtTelpon = findViewById(R.id.biodata_edt_telpon);
        biodataEdtEmail = findViewById(R.id.biodata_edt_email);
        biodataEdtUmur = findViewById(R.id.biodata_edt_umur);
        biodataEdtTempatlahir = findViewById(R.id.biodata_edt_tempatlahir);
        biodataDivTanggallahir = findViewById(R.id.biodata_div_tanggallahir);
        biodataTvTanggallahir = findViewById(R.id.biodata_tv_tanggallahir);
        biodataRbLaki = findViewById(R.id.biodata_rb_laki);
        biodataRbPerempuan = findViewById(R.id.biodata_rb_perempuan);
        biodataEdtKota = findViewById(R.id.biodata_edt_kota);
        biodataEdtKecamatan = findViewById(R.id.biodata_edt_kecamatan);
        biodataEdtKelurahan = findViewById(R.id.biodata_edt_kelurahan);
        biodataEdtRt = findViewById(R.id.biodata_edt_rt);
        biodataEdtRw = findViewById(R.id.biodata_edt_rw);
        biodataEdtSekolah = findViewById(R.id.biodata_edt_sekolah);
        biodataEdtKelas = findViewById(R.id.biodata_edt_kelas);
        biodataRbPndkayah1 = findViewById(R.id.biodata_rb_pndkayah1);
        biodataRbPndkayah2 = findViewById(R.id.biodata_rb_pndkayah2);
        biodataRbPndkayah3 = findViewById(R.id.biodata_rb_pndkayah3);
        biodataRbPndkayah4 = findViewById(R.id.biodata_rb_pndkayah4);
        biodataRbPndkayah5 = findViewById(R.id.biodata_rb_pndkayah5);
        biodataRbPrkjayah1 = findViewById(R.id.biodata_rb_prkjayah1);
        biodataRbPrkjayah2 = findViewById(R.id.biodata_rb_prkjayah2);
        biodataRbPrkjayah3 = findViewById(R.id.biodata_rb_prkjayah3);
        biodataRbPrkjayah4 = findViewById(R.id.biodata_rb_prkjayah4);
        biodataRbPndkibu1 = findViewById(R.id.biodata_rb_pndkibu1);
        biodataRbPndkibu2 = findViewById(R.id.biodata_rb_pndkibu2);
        biodataRbPndkibu3 = findViewById(R.id.biodata_rb_pndkibu3);
        biodataRbPndkibu4 = findViewById(R.id.biodata_rb_pndkibu4);
        biodataRbPndkibu5 = findViewById(R.id.biodata_rb_pndkibu5);
        biodataRbPrkjibu1 = findViewById(R.id.biodata_rb_prkjibu1);
        biodataRbPrkjibu2 = findViewById(R.id.biodata_rb_prkjibu2);
        biodataRbPrkjibu3 = findViewById(R.id.biodata_rb_prkjibu3);
        biodataRbPrkjibu4 = findViewById(R.id.biodata_rb_prkjibu4);
        biodataRbPendapatan1 = findViewById(R.id.biodata_rb_pendapatan1);
        biodataRbPendapatan2 = findViewById(R.id.biodata_rb_pendapatan2);
        biodataRbPendapatan3 = findViewById(R.id.biodata_rb_pendapatan3);
        biodataBtnSimpan = findViewById(R.id.biodata_btn_simpan);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        edtTinggi = (EditText) findViewById(R.id.edt_tinggi);
        biodataEdtLingkarPerut = (EditText) findViewById(R.id.biodata_edt_lingkar_perut);
        edtSistolik = (EditText) findViewById(R.id.edt_sistolik);
        edtDiastolik = (EditText) findViewById(R.id.edt_diastolik);
        rbEkstraKarangTaruna = (RadioButton) findViewById(R.id.rb_ekstra_karang_taruna);
        rbEkstraRemajaMasjid = (RadioButton) findViewById(R.id.rb_ekstra_remaja_masjid);
        rbEkstraOsiss = (RadioButton) findViewById(R.id.rb_ekstra_osiss);
        rbEkstraPramuka = (RadioButton) findViewById(R.id.rb_ekstra_pramuka);
        rbEkstraLsm = (RadioButton) findViewById(R.id.rb_ekstra_lsm);
        rbEkstraKursus = (RadioButton) findViewById(R.id.rb_ekstra_kursus);
        rbEkstraOlahraga = (RadioButton) findViewById(R.id.rb_ekstra_olahraga);
        rbEkstraPmr = (RadioButton) findViewById(R.id.rb_ekstra_pmr);
        rbEkstraUks = (RadioButton) findViewById(R.id.rb_ekstra_uks);
        rbEkstraKir = (RadioButton) findViewById(R.id.rb_ekstra_kir);
        rbEkstraTidak = (RadioButton) findViewById(R.id.rb_ekstra_tidak);
        rgAplikasi = (RadioGroup) findViewById(R.id.rg_aplikasi);
        rbAplikasi1 = (RadioButton) findViewById(R.id.rb_aplikasi1);
        rbAplikasi2 = (RadioButton) findViewById(R.id.rb_aplikasi2);
        rbPencegahanSeminar = (RadioButton) findViewById(R.id.rb_pencegahan_seminar);
        rbPencegahanTalkShow = (RadioButton) findViewById(R.id.rb_pencegahan_talk_show);
        rbPencegahanKampanye = (RadioButton) findViewById(R.id.rb_pencegahan_kampanye);
        rbPencegahanPenyuluhan = (RadioButton) findViewById(R.id.rb_pencegahan_penyuluhan);
        rbPencegahanTidak = (RadioButton) findViewById(R.id.rb_pencegahan_tidak);
        rbInformasiPetugas = (RadioButton) findViewById(R.id.rb_informasi_petugas);
        rbInformasiKeluarga = (RadioButton) findViewById(R.id.rb_informasi_keluarga);
        rbInformasiBuku = (RadioButton) findViewById(R.id.rb_informasi_buku);
        rbInformasiSekolah = (RadioButton) findViewById(R.id.rb_informasi_sekolah);
        rbInformasiInternet = (RadioButton) findViewById(R.id.rb_informasi_internet);
        rbInformasiTv = (RadioButton) findViewById(R.id.rb_informasi_tv);
        rbInformasiRadio = (RadioButton) findViewById(R.id.rb_informasi_radio);
        rbInformasiTidak = (RadioButton) findViewById(R.id.rb_informasi_tidak);
        rbKeluargaAyah = (RadioButton) findViewById(R.id.rb_keluarga_ayah);
        rbKeluargaIbu = (RadioButton) findViewById(R.id.rb_keluarga_ibu);
        rbKeluargaAyahIbu = (RadioButton) findViewById(R.id.rb_keluarga_ayah_ibu);
        rbKeluargTidak = (RadioButton) findViewById(R.id.rb_keluarg_tidak);
        rbPriksaTekananDarah = (RadioButton) findViewById(R.id.rb_priksa_tekanan_darah);
        rbPriksaGulaDarah = (RadioButton) findViewById(R.id.rb_priksa_gula_darah);
        rbPriksaKolesterol = (RadioButton) findViewById(R.id.rb_priksa_kolesterol);
        rbPriksaTidak = (RadioButton) findViewById(R.id.rb_priksa_tidak);
        rgRokok = (RadioGroup) findViewById(R.id.rg_rokok);
        rbRokokYa = (RadioButton) findViewById(R.id.rb_rokok_ya);
        rbRokokTidak = (RadioButton) findViewById(R.id.rb_rokok_tidak);
        edtLamaMerokok = (EditText) findViewById(R.id.edt_lama_merokok);
        rgRokokPasif = (RadioGroup) findViewById(R.id.rg_rokok_pasif);
        rbRokokPasifYa = (RadioButton) findViewById(R.id.rb_rokok_pasif_ya);
        rbRokokPasifTidak = (RadioButton) findViewById(R.id.rb_rokok_pasif_tidak);
        rgAlkohol = (RadioGroup) findViewById(R.id.rg_alkohol);
        rbAlkoholYa = (RadioButton) findViewById(R.id.rb_alkohol_ya);
        rbAlkoholTidak = (RadioButton) findViewById(R.id.rb_alkohol_tidak);
        edtLamaAlkohol = (EditText) findViewById(R.id.edt_lama_alkohol);
        rbOlahragaJoging = (RadioButton) findViewById(R.id.rb_olahraga_joging);
        rbOlahragaRenang = (RadioButton) findViewById(R.id.rb_olahraga_renang);
        rbOlahragaSepakBola = (RadioButton) findViewById(R.id.rb_olahraga_sepak_bola);
        rbOlahragaBasket = (RadioButton) findViewById(R.id.rb_olahraga_basket);
        rbOlahragaBadminton = (RadioButton) findViewById(R.id.rb_olahraga_badminton);
        rbOlahragaFitnes = (RadioButton) findViewById(R.id.rb_olahraga_fitnes);
        rbOlahragaFutsal = (RadioButton) findViewById(R.id.rb_olahraga_futsal);
        rbOlahragaKarate = (RadioButton) findViewById(R.id.rb_olahraga_karate);
        rbOlahragaTenis = (RadioButton) findViewById(R.id.rb_olahraga_tenis);
        rbOlahragaTidak = (RadioButton) findViewById(R.id.rb_olahraga_tidak);
        rbMakanMi = (RadioButton) findViewById(R.id.rb_makan_mi);
        rbMakanHamburger = (RadioButton) findViewById(R.id.rb_makan_hamburger);
        rbMakanPizza = (RadioButton) findViewById(R.id.rb_makan_pizza);
        rbMakanHotDog = (RadioButton) findViewById(R.id.rb_makan_hot_dog);
        rbMakanKentang = (RadioButton) findViewById(R.id.rb_makan_kentang);
        rbMakanNugget = (RadioButton) findViewById(R.id.rb_makan_nugget);
        rbMakanCilok = (RadioButton) findViewById(R.id.rb_makan_cilok);
        rbMakanSosis = (RadioButton) findViewById(R.id.rb_makan_sosis);
        rbMakanTidak = (RadioButton) findViewById(R.id.rb_makan_tidak);
        rgMinum = (RadioGroup) findViewById(R.id.rg_minum);
        rbMinumYa = (RadioButton) findViewById(R.id.rb_minum_ya);
        rbMinumTidak = (RadioButton) findViewById(R.id.rb_minum_tidak);
        rgIstirahat = (RadioGroup) findViewById(R.id.rg_istirahat);
        rbIstirahatYa = (RadioButton) findViewById(R.id.rb_istirahat_ya);
        rbIstirahatTidak = (RadioButton) findViewById(R.id.rb_istirahat_tidak);
        rbStresMendekatkanDiri = (RadioButton) findViewById(R.id.rb_stres_mendekatkan_diri);
        rbStresTerbuka = (RadioButton) findViewById(R.id.rb_stres_terbuka);
        rbStresPikirPositif = (RadioButton) findViewById(R.id.rb_stres_pikir_positif);
        rbStresMenyalurkanHobi = (RadioButton) findViewById(R.id.rb_stres_menyalurkan_hobi);
        rbStresMenyendiri = (RadioButton) findViewById(R.id.rb_stres_menyendiri);
        rbStresMerenung = (RadioButton) findViewById(R.id.rb_stres_merenung);
        rbStresMenjauh = (RadioButton) findViewById(R.id.rb_stres_menjauh);
        rbStresMerokok = (RadioButton) findViewById(R.id.rb_stres_merokok);
        rbStresTidak = (RadioButton) findViewById(R.id.rb_stres_tidak);
        biodataEdtGlukosaDarah = (EditText) findViewById(R.id.biodata_edt_glukosa_darah);
        rbMakanEskrim = (RadioButton) findViewById(R.id.rb_makan_eskrim);
    }
}
