package com.example.cia.deteksikesehatan.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.model.PayloadQuesioner;
import com.example.cia.deteksikesehatan.model.QuesionerModel;
import com.example.cia.deteksikesehatan.room.AppDatabase;
import com.example.cia.deteksikesehatan.room.Quesioner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LihatQuesionerActivity extends AppCompatActivity {

    private LinearLayout div;
    ArrayList<Quesioner> listQueById = new ArrayList<>();
    private AppDatabase db;
    SharedPref pref;
    String id;
    ArrayList<String> listHarianPasif = new ArrayList<>();
    ArrayList<String> listHarianAktif = new ArrayList<>();
    ArrayList<String> listStresNegatif = new ArrayList<>();

    List<QuesionerModel> listQuesioner = new ArrayList<>();
    private Spinner spnPilih;
    private LinearLayout lyHarian;
    private EditText edtH;
    private LinearLayout lyMinggu;
    private EditText edtMSatu;
    private EditText edtMDua;
    private LinearLayout lyBulan;
    private Spinner spnBulan;
    private Spinner spnTahun;
    private Button btnCari;

    DatePickerDialog datePickerDialog, datePickerDialog2,datePickerDialog3;
    int mYear;
    int mMonth;
    int mDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_quesioner);
        initView();

        awal();

        edtH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal1();
                datePickerDialog.show();
            }
        });

        edtMSatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal2();
                datePickerDialog2.show();
            }
        });

        edtMDua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal3();
                datePickerDialog3.show();
            }
        });

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spnPilih.getSelectedItem().toString().equals("Harian")) {
                    cariHari(true);
                } else if (spnPilih.getSelectedItem().toString().equals("Mingguan")) {
                    cariMinggu(true);
                } else {
                    cariBulan(true);
                }
            }
        });
    }

    private void awal(){
        pref = new SharedPref(this);
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "quesionerdb").allowMainThreadQueries().build();

        Harian();
        id = getIntent().getStringExtra("id_user");

        ArrayList<String> listPilih = new ArrayList<>();
        listPilih.add("Harian");
        listPilih.add("Mingguan");
        listPilih.add("Bulanan");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(LihatQuesionerActivity.this, R.layout.support_simple_spinner_dropdown_item, listPilih);
        spnPilih.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnPilih.setAdapter(adp2);
        spnPilih.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spnPilih.getSelectedItem().toString().equals("Harian")) {
                    lyHarian.setVisibility(View.VISIBLE);
                    lyBulan.setVisibility(View.GONE);
                    lyMinggu.setVisibility(View.GONE);
                } else if (spnPilih.getSelectedItem().toString().equals("Mingguan")) {
                    lyHarian.setVisibility(View.GONE);
                    lyBulan.setVisibility(View.GONE);
                    lyMinggu.setVisibility(View.VISIBLE);
                } else {
                    lyHarian.setVisibility(View.GONE);
                    lyBulan.setVisibility(View.VISIBLE);
                    lyMinggu.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spnBulanan();

    }
    private void spnBulanan() {
        ArrayList<String> bulan = new ArrayList<>();
        for (int i = 1; i < 13; i++) {
            bulan.add("" + i);
        }

        ArrayList<String> tahun = new ArrayList<>();
        for (int i = 2019; i < 2030; i++) {
            tahun.add("" + i);
        }
        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(LihatQuesionerActivity.this, R.layout.support_simple_spinner_dropdown_item, bulan);
        spnBulan.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnBulan.setAdapter(adp2);

        ArrayAdapter<String> adp = new ArrayAdapter<String>(LihatQuesionerActivity.this, R.layout.support_simple_spinner_dropdown_item, tahun);
        spnTahun.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnTahun.setAdapter(adp);

    }

    private void cariHari(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.qPerHari(edtH.getText().toString(),
                id).enqueue(new Callback<PayloadQuesioner>() {
            @Override
            public void onResponse(Call<PayloadQuesioner> call, Response<PayloadQuesioner> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    listQuesioner = response.body().getPayload();
                    if (response.body().getError() == false){
                        for (int i = 0; i <listQuesioner.size() ; i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_history, null);

                            TextView tvNo = view.findViewById(R.id.tv_no);
                            TextView tvId = view.findViewById(R.id.tv_id);
                            TextView tvKet = view.findViewById(R.id.tv_ket);

                            TextView tvBerat = (TextView) view.findViewById(R.id.tv_berat);
                            TextView tvTinggi = (TextView) view.findViewById(R.id.tv_tinggi);
                            TextView tvImt = (TextView) view.findViewById(R.id.tv_imt);
                            TextView tvBeratI = (TextView) view.findViewById(R.id.tv_berat_ideal);
                            TextView tvAsap = (TextView) view.findViewById(R.id.tv_asap);
                            TextView tvRokok = (TextView) view.findViewById(R.id.tv_rokok);
                            TextView tvAlkohol = (TextView) view.findViewById(R.id.tv_alkohol);

                            LinearLayout lyAsapRoko = (LinearLayout) view.findViewById(R.id.ly_asap_roko);
                            LinearLayout lyRoko = (LinearLayout) view.findViewById(R.id.ly_roko);
                            LinearLayout lyAlkohol = (LinearLayout) view.findViewById(R.id.ly_alkohol);
                            TextView tvAktifitas = (TextView) view.findViewById(R.id.tv_aktifitas);
                            LinearLayout lyAktifitas = (LinearLayout) view.findViewById(R.id.ly_aktifitas);
                            TextView tvOlahraga = (TextView) view.findViewById(R.id.tv_olahraga);
                            LinearLayout lyOlahraga = (LinearLayout) view.findViewById(R.id.ly_olahraga);
                            TextView tvMinumAir = (TextView) view.findViewById(R.id.tv_minum_air);
                            LinearLayout lyMinumAir = (LinearLayout) view.findViewById(R.id.ly_minum_air);
                            TextView tvMakan = (TextView) view.findViewById(R.id.tv_makan);
                            LinearLayout lyMakan = (LinearLayout) view.findViewById(R.id.ly_makan);
                            TextView tvLauk = (TextView) view.findViewById(R.id.tv_lauk);
                            LinearLayout lyLauk = (LinearLayout) view.findViewById(R.id.ly_lauk);
                            TextView tvBuah = (TextView) view.findViewById(R.id.tv_buah);
                            LinearLayout lyBuah = (LinearLayout) view.findViewById(R.id.ly_buah);
                            TextView tvSayur = (TextView) view.findViewById(R.id.tv_sayur);
                            LinearLayout lySayur = (LinearLayout) view.findViewById(R.id.ly_sayur);
                            TextView tvTidur = (TextView) view.findViewById(R.id.tv_tidur);
                            LinearLayout lyTidur = (LinearLayout) view.findViewById(R.id.ly_tidur);
                            TextView tvStres = (TextView) view.findViewById(R.id.tv_stres);
                            LinearLayout lyStres = (LinearLayout) view.findViewById(R.id.ly_stres);

//                        tvNo.setText("" + listQuesioner.get(i).getNamaUser());
                            tvId.setText(listQuesioner.get(i).getTQUUSERID());
//                        tvBerat.setText(listQueById.get(i).getBeratBadan());
//                        tvTinggi.setText(listQueById.get(i).getTinggiBadan());

                            tvAsap.setText(listQuesioner.get(i).getTQUASAPROKOK());

                            if (listQuesioner.get(i).getTQUASAPROKOK().equals("Ya")) {
                                lyAsapRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyAsapRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvRokok.setText(listQuesioner.get(i).getTQUPEROKOK());

                            if (listQuesioner.get(i).getTQUPEROKOK().equals("Ya")) {
                                lyRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvAlkohol.setText(listQuesioner.get(i).getTQUALKOHOL());

                            if (listQuesioner.get(i).getTQUALKOHOL().equals("Ya")) {
                                lyAlkohol.setBackgroundColor(Color.RED);
                            } else {
                                lyAlkohol.setBackgroundColor(Color.GREEN);
                            }

                            tvAktifitas.setText(listQuesioner.get(i).getTQUAKTIFITAS());


                            if (listQuesioner.get(i).getTQUAKTIFITAS().equals("Menonton TV > 6 jam")) {
                                lyAktifitas.setBackgroundColor(Color.YELLOW);
                            }else if (listQuesioner.get(i).getTQUAKTIFITAS().equals("Bermain game di komputer > 6jam")){
                                lyAktifitas.setBackgroundColor(Color.YELLOW);
                            } else if (listQuesioner.get(i).getTQUAKTIFITAS().equals("Kurang berolahraga/malas bergerak")){
                                lyAktifitas.setBackgroundColor(Color.YELLOW);
                            } else {
                                lyAktifitas.setBackgroundColor(Color.GREEN);
                            }

                            tvOlahraga.setText(listQuesioner.get(i).getTQUOLAHRAGA());
                            lyOlahraga.setBackgroundColor(Color.GREEN);

                            tvMinumAir.setText(listQuesioner.get(i).getTQUMINUM());
                            if (tvMinumAir.getText().toString().equals("Cairan Seimbang (8 gelas)")) {
                                lyMinumAir.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMinumAir.setBackgroundColor(Color.YELLOW);
                            }

                            tvMakan.setText("kalori : " + listQuesioner.get(i).getTQUMAKAN());
                            int mkn = Integer.parseInt(listQuesioner.get(i).getTQUMAKAN());

                            if (mkn >= 3 && mkn <= 8) {
                                lyMakan.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMakan.setBackgroundColor(Color.YELLOW);
                            }

                            tvLauk.setText("kalori : " + listQuesioner.get(i).getTQULAUK());
                            int lauk = Integer.parseInt(listQuesioner.get(i).getTQULAUK());

                            if (lauk >= 2 && lauk <= 3) {
                                lyLauk.setBackgroundColor(Color.GREEN);
                            } else {
                                lyLauk.setBackgroundColor(Color.YELLOW);
                            }

                            tvBuah.setText("kalori : " + listQuesioner.get(i).getTQUBUAH());
                            int buah = Integer.parseInt(listQuesioner.get(i).getTQUBUAH());

                            if (buah >= 2 && buah <= 3) {
                                lyBuah.setBackgroundColor(Color.GREEN);
                            } else {
                                lyBuah.setBackgroundColor(Color.YELLOW);
                            }

                            tvSayur.setText("kalori : " + listQuesioner.get(i).getTQUSAYUR());
                            int sayur = Integer.parseInt(listQuesioner.get(i).getTQUSAYUR());

                            if (sayur >= 3 && sayur <= 5) {
                                lySayur.setBackgroundColor(Color.GREEN);
                            } else {
                                lySayur.setBackgroundColor(Color.YELLOW);
                            }

                            tvTidur.setText(listQuesioner.get(i).getTQUTIDUR());
                            if (tvTidur.getText().toString().equals("Istirahat/tidur <6jam")) {
                                lyTidur.setBackgroundColor(Color.YELLOW);
                            } else {
                                lyTidur.setBackgroundColor(Color.GREEN);
                            }


                            tvStres.setText(listQuesioner.get(i).getTQUSTRESS());

                            if (tvStres.getText().toString().equals("Merokok")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menjauh dari keluarga dan teman terdekat")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menyendiri")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Merenung")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Marah")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else {
                                lyStres.setBackgroundColor(Color.GREEN);
                            }

                            tvKet.setText("Quesioner");

                            div.addView(view);
                        }
                    }else {
                        Toast.makeText(LihatQuesionerActivity.this, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PayloadQuesioner> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatQuesionerActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cariMinggu(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.qPerMinggu(edtMSatu.getText().toString(),
                edtMDua.getText().toString(),
                id).enqueue(new Callback<PayloadQuesioner>() {
            @Override
            public void onResponse(Call<PayloadQuesioner> call, Response<PayloadQuesioner> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    listQuesioner = response.body().getPayload();
                    if (response.body().getError() == false){
                        for (int i = 0; i <listQuesioner.size() ; i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_history, null);

                            TextView tvNo = view.findViewById(R.id.tv_no);
                            TextView tvId = view.findViewById(R.id.tv_id);
                            TextView tvKet = view.findViewById(R.id.tv_ket);

                            TextView tvBerat = (TextView) view.findViewById(R.id.tv_berat);
                            TextView tvTinggi = (TextView) view.findViewById(R.id.tv_tinggi);
                            TextView tvImt = (TextView) view.findViewById(R.id.tv_imt);
                            TextView tvBeratI = (TextView) view.findViewById(R.id.tv_berat_ideal);
                            TextView tvAsap = (TextView) view.findViewById(R.id.tv_asap);
                            TextView tvRokok = (TextView) view.findViewById(R.id.tv_rokok);
                            TextView tvAlkohol = (TextView) view.findViewById(R.id.tv_alkohol);

                            LinearLayout lyAsapRoko = (LinearLayout) view.findViewById(R.id.ly_asap_roko);
                            LinearLayout lyRoko = (LinearLayout) view.findViewById(R.id.ly_roko);
                            LinearLayout lyAlkohol = (LinearLayout) view.findViewById(R.id.ly_alkohol);
                            TextView tvAktifitas = (TextView) view.findViewById(R.id.tv_aktifitas);
                            LinearLayout lyAktifitas = (LinearLayout) view.findViewById(R.id.ly_aktifitas);
                            TextView tvOlahraga = (TextView) view.findViewById(R.id.tv_olahraga);
                            LinearLayout lyOlahraga = (LinearLayout) view.findViewById(R.id.ly_olahraga);
                            TextView tvMinumAir = (TextView) view.findViewById(R.id.tv_minum_air);
                            LinearLayout lyMinumAir = (LinearLayout) view.findViewById(R.id.ly_minum_air);
                            TextView tvMakan = (TextView) view.findViewById(R.id.tv_makan);
                            LinearLayout lyMakan = (LinearLayout) view.findViewById(R.id.ly_makan);
                            TextView tvLauk = (TextView) view.findViewById(R.id.tv_lauk);
                            LinearLayout lyLauk = (LinearLayout) view.findViewById(R.id.ly_lauk);
                            TextView tvBuah = (TextView) view.findViewById(R.id.tv_buah);
                            LinearLayout lyBuah = (LinearLayout) view.findViewById(R.id.ly_buah);
                            TextView tvSayur = (TextView) view.findViewById(R.id.tv_sayur);
                            LinearLayout lySayur = (LinearLayout) view.findViewById(R.id.ly_sayur);
                            TextView tvTidur = (TextView) view.findViewById(R.id.tv_tidur);
                            LinearLayout lyTidur = (LinearLayout) view.findViewById(R.id.ly_tidur);
                            TextView tvStres = (TextView) view.findViewById(R.id.tv_stres);
                            LinearLayout lyStres = (LinearLayout) view.findViewById(R.id.ly_stres);

//                        tvNo.setText("" + listQuesioner.get(i).getNamaUser());
                            tvId.setText(listQuesioner.get(i).getTQUUSERID());
//                        tvBerat.setText(listQueById.get(i).getBeratBadan());
//                        tvTinggi.setText(listQueById.get(i).getTinggiBadan());

                            tvAsap.setText(listQuesioner.get(i).getTQUASAPROKOK());

                            if (listQuesioner.get(i).getTQUASAPROKOK().equals("Ya")) {
                                lyAsapRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyAsapRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvRokok.setText(listQuesioner.get(i).getTQUPEROKOK());

                            if (listQuesioner.get(i).getTQUPEROKOK().equals("Ya")) {
                                lyRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvAlkohol.setText(listQuesioner.get(i).getTQUALKOHOL());

                            if (listQuesioner.get(i).getTQUALKOHOL().equals("Ya")) {
                                lyAlkohol.setBackgroundColor(Color.RED);
                            } else {
                                lyAlkohol.setBackgroundColor(Color.GREEN);
                            }

                            tvAktifitas.setText(listQuesioner.get(i).getTQUAKTIFITAS());

                            for (int j = 0; j < listHarianAktif.size(); j++) {
                                if (tvAktifitas.getText().toString().contains("" + listHarianAktif.get(j))) {
                                    lyAktifitas.setBackgroundColor(Color.GREEN);
                                } else {
                                    lyAktifitas.setBackgroundColor(Color.YELLOW);
                                }
                            }

                            tvOlahraga.setText(listQuesioner.get(i).getTQUOLAHRAGA());
                            lyOlahraga.setBackgroundColor(Color.GREEN);

                            tvMinumAir.setText(listQuesioner.get(i).getTQUMINUM());
                            if (tvMinumAir.getText().toString().equals("Cairan Seimbang (8 gelas)")) {
                                lyMinumAir.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMinumAir.setBackgroundColor(Color.YELLOW);
                            }

                            tvMakan.setText("kalori : " + listQuesioner.get(i).getTQUMAKAN());
                            int mkn = Integer.parseInt(listQuesioner.get(i).getTQUMAKAN());

                            if (mkn >= 3 && mkn <= 8) {
                                lyMakan.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMakan.setBackgroundColor(Color.YELLOW);
                            }

                            tvLauk.setText("kalori : " + listQuesioner.get(i).getTQULAUK());
                            int lauk = Integer.parseInt(listQuesioner.get(i).getTQULAUK());

                            if (lauk >= 2 && lauk <= 3) {
                                lyLauk.setBackgroundColor(Color.GREEN);
                            } else {
                                lyLauk.setBackgroundColor(Color.YELLOW);
                            }

                            tvBuah.setText("kalori : " + listQuesioner.get(i).getTQUBUAH());
                            int buah = Integer.parseInt(listQuesioner.get(i).getTQUBUAH());

                            if (buah >= 2 && buah <= 3) {
                                lyBuah.setBackgroundColor(Color.GREEN);
                            } else {
                                lyBuah.setBackgroundColor(Color.YELLOW);
                            }

                            tvSayur.setText("kalori : " + listQuesioner.get(i).getTQUSAYUR());
                            int sayur = Integer.parseInt(listQuesioner.get(i).getTQUSAYUR());

                            if (sayur >= 3 && sayur <= 5) {
                                lySayur.setBackgroundColor(Color.GREEN);
                            } else {
                                lySayur.setBackgroundColor(Color.YELLOW);
                            }

                            tvTidur.setText(listQuesioner.get(i).getTQUTIDUR());
                            if (tvTidur.getText().toString().equals("Istirahat/tidur <6jam")) {
                                lyTidur.setBackgroundColor(Color.YELLOW);
                            } else {
                                lyTidur.setBackgroundColor(Color.GREEN);
                            }


                            tvStres.setText(listQuesioner.get(i).getTQUSTRESS());

                            if (tvStres.getText().toString().equals("Merokok")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menjauh dari keluarga dan teman terdekat")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menyendiri")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Merenung")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Marah")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else {
                                lyStres.setBackgroundColor(Color.GREEN);
                            }

                            tvKet.setText("Quesioner");

                            div.addView(view);
                        }
                    }else {
                        Toast.makeText(LihatQuesionerActivity.this, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<PayloadQuesioner> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatQuesionerActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cariBulan(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.qPerBulan(spnBulan.getSelectedItem().toString(),
                spnTahun.getSelectedItem().toString(),
                id).enqueue(new Callback<PayloadQuesioner>() {
            @Override
            public void onResponse(Call<PayloadQuesioner> call, Response<PayloadQuesioner> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    listQuesioner = response.body().getPayload();
                    if (response.body().getError() == false){
                        for (int i = 0; i <listQuesioner.size() ; i++) {
                            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View view = layoutInflater.inflate(R.layout.row_history, null);

                            TextView tvNo = view.findViewById(R.id.tv_no);
                            TextView tvId = view.findViewById(R.id.tv_id);
                            TextView tvKet = view.findViewById(R.id.tv_ket);

                            TextView tvBerat = (TextView) view.findViewById(R.id.tv_berat);
                            TextView tvTinggi = (TextView) view.findViewById(R.id.tv_tinggi);
                            TextView tvImt = (TextView) view.findViewById(R.id.tv_imt);
                            TextView tvBeratI = (TextView) view.findViewById(R.id.tv_berat_ideal);
                            TextView tvAsap = (TextView) view.findViewById(R.id.tv_asap);
                            TextView tvRokok = (TextView) view.findViewById(R.id.tv_rokok);
                            TextView tvAlkohol = (TextView) view.findViewById(R.id.tv_alkohol);

                            LinearLayout lyAsapRoko = (LinearLayout) view.findViewById(R.id.ly_asap_roko);
                            LinearLayout lyRoko = (LinearLayout) view.findViewById(R.id.ly_roko);
                            LinearLayout lyAlkohol = (LinearLayout) view.findViewById(R.id.ly_alkohol);
                            TextView tvAktifitas = (TextView) view.findViewById(R.id.tv_aktifitas);
                            LinearLayout lyAktifitas = (LinearLayout) view.findViewById(R.id.ly_aktifitas);
                            TextView tvOlahraga = (TextView) view.findViewById(R.id.tv_olahraga);
                            LinearLayout lyOlahraga = (LinearLayout) view.findViewById(R.id.ly_olahraga);
                            TextView tvMinumAir = (TextView) view.findViewById(R.id.tv_minum_air);
                            LinearLayout lyMinumAir = (LinearLayout) view.findViewById(R.id.ly_minum_air);
                            TextView tvMakan = (TextView) view.findViewById(R.id.tv_makan);
                            LinearLayout lyMakan = (LinearLayout) view.findViewById(R.id.ly_makan);
                            TextView tvLauk = (TextView) view.findViewById(R.id.tv_lauk);
                            LinearLayout lyLauk = (LinearLayout) view.findViewById(R.id.ly_lauk);
                            TextView tvBuah = (TextView) view.findViewById(R.id.tv_buah);
                            LinearLayout lyBuah = (LinearLayout) view.findViewById(R.id.ly_buah);
                            TextView tvSayur = (TextView) view.findViewById(R.id.tv_sayur);
                            LinearLayout lySayur = (LinearLayout) view.findViewById(R.id.ly_sayur);
                            TextView tvTidur = (TextView) view.findViewById(R.id.tv_tidur);
                            LinearLayout lyTidur = (LinearLayout) view.findViewById(R.id.ly_tidur);
                            TextView tvStres = (TextView) view.findViewById(R.id.tv_stres);
                            LinearLayout lyStres = (LinearLayout) view.findViewById(R.id.ly_stres);

//                        tvNo.setText("" + listQuesioner.get(i).getNamaUser());
                            tvId.setText(listQuesioner.get(i).getTQUUSERID());
//                        tvBerat.setText(listQueById.get(i).getBeratBadan());
//                        tvTinggi.setText(listQueById.get(i).getTinggiBadan());

                            tvAsap.setText(listQuesioner.get(i).getTQUASAPROKOK());

                            if (listQuesioner.get(i).getTQUASAPROKOK().equals("Ya")) {
                                lyAsapRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyAsapRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvRokok.setText(listQuesioner.get(i).getTQUPEROKOK());

                            if (listQuesioner.get(i).getTQUPEROKOK().equals("Ya")) {
                                lyRoko.setBackgroundColor(Color.RED);
                            } else {
                                lyRoko.setBackgroundColor(Color.GREEN);
                            }

                            tvAlkohol.setText(listQuesioner.get(i).getTQUALKOHOL());

                            if (listQuesioner.get(i).getTQUALKOHOL().equals("Ya")) {
                                lyAlkohol.setBackgroundColor(Color.RED);
                            } else {
                                lyAlkohol.setBackgroundColor(Color.GREEN);
                            }

                            tvAktifitas.setText(listQuesioner.get(i).getTQUAKTIFITAS());

                            for (int j = 0; j < listHarianAktif.size(); j++) {
                                if (tvAktifitas.getText().toString().contains("" + listHarianAktif.get(j))) {
                                    lyAktifitas.setBackgroundColor(Color.GREEN);
                                } else {
                                    lyAktifitas.setBackgroundColor(Color.YELLOW);
                                }
                            }

                            tvOlahraga.setText(listQuesioner.get(i).getTQUOLAHRAGA());
                            lyOlahraga.setBackgroundColor(Color.GREEN);

                            tvMinumAir.setText(listQuesioner.get(i).getTQUMINUM());
                            if (tvMinumAir.getText().toString().equals("Cairan Seimbang (8 gelas)")) {
                                lyMinumAir.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMinumAir.setBackgroundColor(Color.YELLOW);
                            }

                            tvMakan.setText("kalori : " + listQuesioner.get(i).getTQUMAKAN());
                            int mkn = Integer.parseInt(listQuesioner.get(i).getTQUMAKAN());

                            if (mkn >= 3 && mkn <= 8) {
                                lyMakan.setBackgroundColor(Color.GREEN);
                            } else {
                                lyMakan.setBackgroundColor(Color.YELLOW);
                            }

                            tvLauk.setText("kalori : " + listQuesioner.get(i).getTQULAUK());
                            int lauk = Integer.parseInt(listQuesioner.get(i).getTQULAUK());

                            if (lauk >= 2 && lauk <= 3) {
                                lyLauk.setBackgroundColor(Color.GREEN);
                            } else {
                                lyLauk.setBackgroundColor(Color.YELLOW);
                            }

                            tvBuah.setText("kalori : " + listQuesioner.get(i).getTQUBUAH());
                            int buah = Integer.parseInt(listQuesioner.get(i).getTQUBUAH());

                            if (buah >= 2 && buah <= 3) {
                                lyBuah.setBackgroundColor(Color.GREEN);
                            } else {
                                lyBuah.setBackgroundColor(Color.YELLOW);
                            }

                            tvSayur.setText("kalori : " + listQuesioner.get(i).getTQUSAYUR());
                            int sayur = Integer.parseInt(listQuesioner.get(i).getTQUSAYUR());

                            if (sayur >= 3 && sayur <= 5) {
                                lySayur.setBackgroundColor(Color.GREEN);
                            } else {
                                lySayur.setBackgroundColor(Color.YELLOW);
                            }

                            tvTidur.setText(listQuesioner.get(i).getTQUTIDUR());
                            if (tvTidur.getText().toString().equals("Istirahat/tidur <6jam")) {
                                lyTidur.setBackgroundColor(Color.YELLOW);
                            } else {
                                lyTidur.setBackgroundColor(Color.GREEN);
                            }


                            tvStres.setText(listQuesioner.get(i).getTQUSTRESS());

                            if (tvStres.getText().toString().equals("Merokok")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menjauh dari keluarga dan teman terdekat")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Menyendiri")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Merenung")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else if (tvStres.getText().toString().equals("Marah")){
                                lyStres.setBackgroundColor(Color.YELLOW);
                            }else {
                                lyStres.setBackgroundColor(Color.GREEN);
                            }

                            tvKet.setText("Quesioner");

                            div.addView(view);
                        }
                    }else {
                        Toast.makeText(LihatQuesionerActivity.this, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<PayloadQuesioner> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(LihatQuesionerActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Harian() {

        listHarianPasif.add("Menonton TV (>6jam)");
        listHarianPasif.add("Bermain game di komputer (>6jam)");
        listHarianPasif.add("Kurang berolahraga/malas bergerak");

        listHarianAktif.add("Berjalan kaki");
        listHarianAktif.add("Berkebun");
        listHarianAktif.add("Mencuci pakaian");
        listHarianAktif.add("Menyetrika pakaian");
        listHarianAktif.add("Mengepel lantai");
        listHarianAktif.add("Naik turun tangga");
        listHarianAktif.add("Bersepeda");

        listStresNegatif.add("Menjauh dari keluarga dan teman terdekat");
        listStresNegatif.add("Menyendiri");
        listStresNegatif.add("Merenung");
        listStresNegatif.add("Merokok");
        listStresNegatif.add("Marah");

//        tampil();

    }

    private void tampil() {
        id = getIntent().getStringExtra("id_user");
//        Toast.makeText(this, ""+id, Toast.LENGTH_SHORT).show();
        listQueById.addAll(Arrays.asList(db.quesionerDAO().lihatQuesionerByUser("" + id)));
        if (listQueById.size() == 0) {
            Toast.makeText(this, "Data Quesioner Kosong", Toast.LENGTH_SHORT).show();
        } else {

            for (int i = 0; i < listQueById.size(); i++) {
                LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = layoutInflater.inflate(R.layout.row_history, null);

                TextView tvNo = view.findViewById(R.id.tv_no);
                TextView tvId = view.findViewById(R.id.tv_id);
                TextView tvKet = view.findViewById(R.id.tv_ket);

                TextView tvBerat = (TextView) view.findViewById(R.id.tv_berat);
                TextView tvTinggi = (TextView) view.findViewById(R.id.tv_tinggi);
                TextView tvImt = (TextView) view.findViewById(R.id.tv_imt);
                TextView tvBeratI = (TextView) view.findViewById(R.id.tv_berat_ideal);
                TextView tvAsap = (TextView) view.findViewById(R.id.tv_asap);
                TextView tvRokok = (TextView) view.findViewById(R.id.tv_rokok);
                TextView tvAlkohol = (TextView) view.findViewById(R.id.tv_alkohol);

                LinearLayout lyAsapRoko = (LinearLayout) view.findViewById(R.id.ly_asap_roko);
                LinearLayout lyRoko = (LinearLayout) view.findViewById(R.id.ly_roko);
                LinearLayout lyAlkohol = (LinearLayout) view.findViewById(R.id.ly_alkohol);
                TextView tvAktifitas = (TextView) view.findViewById(R.id.tv_aktifitas);
                LinearLayout lyAktifitas = (LinearLayout) view.findViewById(R.id.ly_aktifitas);
                TextView tvOlahraga = (TextView) view.findViewById(R.id.tv_olahraga);
                LinearLayout lyOlahraga = (LinearLayout) view.findViewById(R.id.ly_olahraga);
                TextView tvMinumAir = (TextView) view.findViewById(R.id.tv_minum_air);
                LinearLayout lyMinumAir = (LinearLayout) view.findViewById(R.id.ly_minum_air);
                TextView tvMakan = (TextView) view.findViewById(R.id.tv_makan);
                LinearLayout lyMakan = (LinearLayout) view.findViewById(R.id.ly_makan);
                TextView tvLauk = (TextView) view.findViewById(R.id.tv_lauk);
                LinearLayout lyLauk = (LinearLayout) view.findViewById(R.id.ly_lauk);
                TextView tvBuah = (TextView) view.findViewById(R.id.tv_buah);
                LinearLayout lyBuah = (LinearLayout) view.findViewById(R.id.ly_buah);
                TextView tvSayur = (TextView) view.findViewById(R.id.tv_sayur);
                LinearLayout lySayur = (LinearLayout) view.findViewById(R.id.ly_sayur);
                TextView tvTidur = (TextView) view.findViewById(R.id.tv_tidur);
                LinearLayout lyTidur = (LinearLayout) view.findViewById(R.id.ly_tidur);
                TextView tvStres = (TextView) view.findViewById(R.id.tv_stres);
                LinearLayout lyStres = (LinearLayout) view.findViewById(R.id.ly_stres);

                tvNo.setText("" + listQueById.get(i).getNamaUser());
                tvId.setText(listQueById.get(i).getIdUser());
                tvBerat.setText(listQueById.get(i).getBeratBadan());
                tvTinggi.setText(listQueById.get(i).getTinggiBadan());

                tvAsap.setText(listQueById.get(i).getAsapRokok());

                if (listQueById.get(i).getAsapRokok().equals("Ya")) {
                    lyAsapRoko.setBackgroundColor(Color.RED);
                } else {
                    lyAsapRoko.setBackgroundColor(Color.GREEN);
                }

                tvRokok.setText(listQueById.get(i).getMerokok());

                if (listQueById.get(i).getMerokok().equals("Ya")) {
                    lyRoko.setBackgroundColor(Color.RED);
                } else {
                    lyRoko.setBackgroundColor(Color.GREEN);
                }

                tvAlkohol.setText(listQueById.get(i).getAlkohol());

                if (listQueById.get(i).getAlkohol().equals("Ya")) {
                    lyAlkohol.setBackgroundColor(Color.RED);
                } else {
                    lyAlkohol.setBackgroundColor(Color.GREEN);
                }

                tvAktifitas.setText(listQueById.get(i).getAktifitasHarian());

                for (int j = 0; j < listHarianAktif.size(); j++) {
                    if (tvAktifitas.getText().toString().contains("" + listHarianAktif.get(j))) {
                        lyAktifitas.setBackgroundColor(Color.GREEN);
                    } else {
                        lyAktifitas.setBackgroundColor(Color.YELLOW);
                    }
                }

                tvOlahraga.setText(listQueById.get(i).getOlahraga());
                lyOlahraga.setBackgroundColor(Color.GREEN);

                tvMinumAir.setText(listQueById.get(i).getMinum());
                if (tvMinumAir.getText().toString().equals("Cairan Seimbang (8 gelas)")) {
                    lyMinumAir.setBackgroundColor(Color.GREEN);
                } else {
                    lyMinumAir.setBackgroundColor(Color.YELLOW);
                }

                tvMakan.setText("kalori : " + listQueById.get(i).getMakanan());
                int mkn = Integer.parseInt(listQueById.get(i).getMakanan());

                if (mkn >= 3 && mkn <= 8) {
                    lyMakan.setBackgroundColor(Color.GREEN);
                } else {
                    lyMakan.setBackgroundColor(Color.YELLOW);
                }

                tvLauk.setText("kalori : " + listQueById.get(i).getLauk());
                int lauk = Integer.parseInt(listQueById.get(i).getLauk());

                if (lauk >= 2 && mkn <= 3) {
                    lyLauk.setBackgroundColor(Color.GREEN);
                } else {
                    lyLauk.setBackgroundColor(Color.YELLOW);
                }

                tvBuah.setText("kalori : " + listQueById.get(i).getBuah());
                int buah = Integer.parseInt(listQueById.get(i).getBuah());

                if (buah >= 2 && mkn <= 3) {
                    lyBuah.setBackgroundColor(Color.GREEN);
                } else {
                    lyBuah.setBackgroundColor(Color.YELLOW);
                }

                tvSayur.setText("kalori : " + listQueById.get(i).getSayur());
                int sayur = Integer.parseInt(listQueById.get(i).getSayur());

                if (sayur >= 3 && mkn <= 5) {
                    lySayur.setBackgroundColor(Color.GREEN);
                } else {
                    lySayur.setBackgroundColor(Color.YELLOW);
                }

                tvTidur.setText(listQueById.get(i).getTidur());
                if (tvTidur.getText().toString().equals("Istirahat/tidur <6jam")) {
                    lyTidur.setBackgroundColor(Color.YELLOW);
                } else {
                    lyTidur.setBackgroundColor(Color.GREEN);
                }


                tvStres.setText(listQueById.get(i).getStres());

                for (int k = 0; k < listStresNegatif.size(); k++) {
                    if (tvStres.getText().toString().contains("" + listStresNegatif.get(k))) {
                        lyStres.setBackgroundColor(Color.YELLOW);
                    } else {
                        lyStres.setBackgroundColor(Color.GREEN);
                    }
                }

                int berat = Integer.parseInt(listQueById.get(i).getBeratBadan());
                int tingi = Integer.parseInt(listQueById.get(i).getTinggiBadan());

                double tingiD = Double.parseDouble(listQueById.get(i).getTinggiBadan());
                double beratD = Double.parseDouble(listQueById.get(i).getBeratBadan());

                int beratI = (tingi - 100) - ((10 / 100) * (tingi - 100));

                tvBeratI.setText("" + beratI);

                double tinggiDlmM = tingiD / 100;
                double imt = beratD / (tinggiDlmM * tinggiDlmM);

                tvImt.setText("" + imt);

                tvKet.setText("Quesioner");

                div.addView(view);
            }
        }
    }

    private void settingTanggal1() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtH.setText(sdf.format(c.getTime()));

                edtH.setFocusable(false);
                edtH.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void settingTanggal2() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog2 = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtMSatu.setText(sdf.format(c.getTime()));

                edtMSatu.setFocusable(false);
                edtMSatu.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void settingTanggal3() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog3 = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtMDua.setText(sdf.format(c.getTime()));

                edtMDua.setFocusable(false);
                edtMDua.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }


    private void initView() {
        div = (LinearLayout) findViewById(R.id.div);
        spnPilih = (Spinner) findViewById(R.id.spn_pilih);
        lyHarian = (LinearLayout) findViewById(R.id.ly_harian);
        edtH = (EditText) findViewById(R.id.edt_h);
        lyMinggu = (LinearLayout) findViewById(R.id.ly_minggu);
        edtMSatu = (EditText) findViewById(R.id.edt_m_satu);
        edtMDua = (EditText) findViewById(R.id.edt_m_dua);
        lyBulan = (LinearLayout) findViewById(R.id.ly_bulan);
        spnBulan = (Spinner) findViewById(R.id.spn_bulan);
        spnTahun = (Spinner) findViewById(R.id.spn_tahun);
        btnCari = (Button) findViewById(R.id.btn_cari);
    }
}
