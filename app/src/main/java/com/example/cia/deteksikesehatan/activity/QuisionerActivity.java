package com.example.cia.deteksikesehatan.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.room.AppDatabase;
import com.example.cia.deteksikesehatan.room.Quesioner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuisionerActivity extends AppCompatActivity {

    private ImageView quisionerIvClose;

    private AppDatabase db;
    SharedPref pref;
    private TextView quisionerTvTitle;
    private EditText edtBerat;
    private EditText edtTinggi;
    private RadioGroup rgRokok1;
    private RadioButton quisionerRbYamerokok;
    private RadioButton quisionerRbTidakmerokok;
    private RadioGroup rgRokok2;
    private RadioButton rbTerpaparAsapRokok;
    private RadioButton rbTidakTerpaparAsap;
    private RadioGroup rgMinumAlkohol;
    private RadioButton rbMinumAlkohol;
    private RadioButton rbTidakMinumAlkohol;

    private Spinner spnOlahraga;
    private Spinner spnMinumAir;

    private Button btnInput;
    String harian, stres;
    private Spinner spnAktifitasHarian;
    private Spinner spnStres;
    private RadioButton rbNasi;
    private EditText edtNasi;
    private RadioButton rbBiskuit;
    private EditText edtBiskuit;
    private RadioButton rbRoti;
    private EditText edtRoti;
    private RadioButton rbJagung;
    private EditText edtJagung;
    private RadioButton rbKentang;
    private EditText edtKentang;
    private RadioButton rbSingkong;
    private EditText edtSingkong;
    private RadioButton rbMi;
    private EditText edtMi;
    private RadioButton rbAyam;
    private EditText edtAyam;
    private RadioButton rbIkan;
    private EditText edtIkan;
    private RadioButton rbDaging;
    private EditText edtDaging;
    private RadioButton rbTelur;
    private EditText edtTelur;
    private RadioButton rbUdang;
    private EditText edtUdang;
    private RadioButton rbBakso;
    private EditText edtBakso;
    private RadioButton rbTempe;
    private EditText edtTempe;
    private RadioButton rbTahu;
    private EditText edtTahu;
    private RadioButton rbSemangka;
    private EditText edtSemangka;
    private RadioButton rbJeruk;
    private EditText edtJeruk;
    private RadioButton rbMelon;
    private EditText edtMelon;
    private RadioButton rbPisang;
    private EditText edtPisang;
    private RadioButton rbPepaya;
    private EditText edtPepaya;
    private RadioButton rbMangga;
    private EditText edtMangga;
    private RadioButton rbApel;
    private EditText edtApel;
    private RadioButton rbAlpukat;
    private EditText edtAlpukat;
    private RadioButton rbNanas;
    private EditText edtNanas;
    private RadioButton rbSop;
    private EditText edtSop;
    private RadioButton rbBening;
    private EditText edtBening;
    private RadioButton rbLodeh;
    private EditText edtLodeh;
    private RadioButton rbTumis;
    private EditText edtTumis;
    private RadioButton rbGudeng;
    private EditText edtGudeng;

    int kaloriMakanan, kaloriLauk, kaloriBuah, kaloriSayur;
    private RadioGroup rgTidur;
    private RadioButton rbTidur1;
    private RadioButton rbTidur2;

    List<String> makan = new ArrayList<>();
    List<String> lauk = new ArrayList<>();
    List<String> buah = new ArrayList<>();
    List<String> sayur = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quisioner);
        getSupportActionBar().hide();
        initView();

        awal();

        btnInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                posData();
            }
        });


    }

    private void awal() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "quesionerdb").allowMainThreadQueries().build();
        pref = new SharedPref(this);


        spnAktifitasHarian();
        spnOlahraga();
        spnMinumAir();
        spnStres();
    }

    private void ambilMakanan() {
        int nasi = Integer.parseInt(edtNasi.getText().toString());
        int biskuit = Integer.parseInt(edtBiskuit.getText().toString());
        int roti = Integer.parseInt(edtRoti.getText().toString());
        int jagung = Integer.parseInt(edtJagung.getText().toString());
        int kentang = Integer.parseInt(edtKentang.getText().toString());
        int singkong = Integer.parseInt(edtSingkong.getText().toString());
        int mi = Integer.parseInt(edtMi.getText().toString());

        kaloriMakanan = nasi + biskuit + roti + jagung + kentang + singkong + mi;
        makan.add(""+kaloriMakanan);
    }

    private void ambilLauk() {
        int ayam = Integer.parseInt(edtAyam.getText().toString());
        int ikan = Integer.parseInt(edtIkan.getText().toString());
        int daging = Integer.parseInt(edtDaging.getText().toString());
        int telur = Integer.parseInt(edtTahu.getText().toString());
        int udang = Integer.parseInt(edtUdang.getText().toString());
        int bakso = Integer.parseInt(edtBakso.getText().toString());
        int tempe = Integer.parseInt(edtTempe.getText().toString());
        int tahu = Integer.parseInt(edtTahu.getText().toString());

        kaloriLauk = ayam + ikan + daging + telur + udang + bakso + tempe + tahu;
        lauk.add(""+kaloriLauk);
    }

    private void ambilBuah() {
        int semangka = Integer.parseInt(edtSemangka.getText().toString());
        int jeruk = Integer.parseInt(edtJeruk.getText().toString());
        int melon = Integer.parseInt(edtMelon.getText().toString());
        int pisang = Integer.parseInt(edtPisang.getText().toString());
        int pepaya = Integer.parseInt(edtPepaya.getText().toString());
        int mangga = Integer.parseInt(edtMangga.getText().toString());
        int apel = Integer.parseInt(edtApel.getText().toString());
        int alpukat = Integer.parseInt(edtAlpukat.getText().toString());
        int nanas = Integer.parseInt(edtNanas.getText().toString());

        kaloriBuah = semangka + jeruk + melon + pisang + pepaya + mangga + apel + alpukat + nanas;
        buah.add(""+kaloriBuah);
    }

    private void ambilSayur() {
        int sop = Integer.parseInt(edtSop.getText().toString());
        int bening = Integer.parseInt(edtBening.getText().toString());
        int lodeh = Integer.parseInt(edtLodeh.getText().toString());
        int tumis = Integer.parseInt(edtTumis.getText().toString());
        int gudeng = Integer.parseInt(edtGudeng.getText().toString());

        kaloriSayur = sop + bening + lodeh + tumis + gudeng;
        sayur.add(""+kaloriSayur);
    }

    private void spnAktifitasHarian() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Menonton TV >6jam");
        list.add("Bermain game di komputer >6jam");
        list.add("Kurang berolahraga/malas bergerak");
        list.add("Berjalan kaki");
        list.add("Berkebun");
        list.add("Mencuci pakaian");
        list.add("Menyetrika pakaian");
        list.add("Mengepel lantai");
        list.add("Naik turun tangga");
        list.add("Bersepeda");


        ArrayAdapter<String> adp = new ArrayAdapter<String>(QuisionerActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnAktifitasHarian.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnAktifitasHarian.setAdapter(adp);

    }

    private void spnOlahraga() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Jogging");
        list.add("Futsal");
        list.add("Fitness");
        list.add("Senam");
        list.add("Renang");
        list.add("Basket");
        list.add("Sepak Bola");
        list.add("Badminton");
        list.add("Tennis");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(QuisionerActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnOlahraga.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnOlahraga.setAdapter(adp2);
    }

    private void spnMinumAir() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Cairan Berlebihan (>9 gelas)");
        list.add("Cairan Kurang (<4 gelas)");
        list.add("Cairan Seimbang (8 gelas)");

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(QuisionerActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnMinumAir.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnMinumAir.setAdapter(adp2);
    }

    private void spnStres() {
        ArrayList<String> list = new ArrayList<>();

        list.add("Menjauh dari keluarga dan teman terdekat");
        list.add("Menyendiri");
        list.add("Merenung");
        list.add("Merokok");
        list.add("Marah");

        list.add("Mendekatkan diri pada Allah SWT");
        list.add("Komunikasi dengan keluarga dan teman terdekat");
        list.add("Berpikiran positif");
        list.add("Melakukan kegiatan yang bermanfaat");
        list.add("Menenangkan pikiran dengan relaksasi");
        list.add("Menyalurkan hobi yang bermanfaat/positif");


        ArrayAdapter<String> adp = new ArrayAdapter<String>(QuisionerActivity.this, R.layout.support_simple_spinner_dropdown_item, list);
        spnStres.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnStres.setAdapter(adp);
    }

    private void setData() {
        RadioButton rbMerokok = findViewById(rgRokok1.getCheckedRadioButtonId());
        String merokok = rbMerokok.getText().toString();

        RadioButton rbAsap = findViewById(rgRokok2.getCheckedRadioButtonId());
        String asap = rbAsap.getText().toString();

        RadioButton rbAlkohol = findViewById(rgMinumAlkohol.getCheckedRadioButtonId());
        String alkohol = rbAlkohol.getText().toString();

        RadioButton rbTidur= findViewById(rgTidur.getCheckedRadioButtonId());
        String tidur = rbTidur.getText().toString();


        ambilMakanan();
        ambilLauk();
        ambilSayur();
        ambilBuah();


        Quesioner q = new Quesioner();
        q.setIdUser(pref.getIdUser());
        q.setNamaUser(pref.getName());
        q.setBeratBadan("0");
        q.setTinggiBadan("0");
        q.setMerokok(merokok);
        q.setAsapRokok(asap);
        q.setAlkohol(alkohol);
        q.setAktifitasHarian(spnAktifitasHarian.getSelectedItem().toString());
        q.setOlahraga(spnOlahraga.getSelectedItem().toString());
        q.setMinum(spnMinumAir.getSelectedItem().toString());
        q.setMakanan("" + kaloriMakanan);
        q.setLauk("" + kaloriLauk);
        q.setBuah("" + kaloriBuah);
        q.setSayur("" + kaloriSayur);
        q.setTidur(tidur);
        q.setStres(spnStres.getSelectedItem().toString());
        insertData(q);
    }

    private void posData(){
        RadioButton rbMerokok = findViewById(rgRokok1.getCheckedRadioButtonId());
        String merokok = rbMerokok.getText().toString();

        RadioButton rbAsap = findViewById(rgRokok2.getCheckedRadioButtonId());
        String asap = rbAsap.getText().toString();

        RadioButton rbAlkohol = findViewById(rgMinumAlkohol.getCheckedRadioButtonId());
        String alkohol = rbAlkohol.getText().toString();

        RadioButton rbTidur= findViewById(rgTidur.getCheckedRadioButtonId());
        String tidur = rbTidur.getText().toString();


        ambilMakanan();
        ambilLauk();
        ambilSayur();
        ambilBuah();


        final ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle("Loading ...");
        pd.setCancelable(false);
        pd.show();
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.posQuesioner(pref.getIdUser(),
                ""+asap,
                ""+merokok,
                ""+alkohol,
                spnAktifitasHarian.getSelectedItem().toString(),
                ""+spnOlahraga.getSelectedItem().toString(),
                ""+spnMinumAir.getSelectedItem().toString(),
                makan,
                lauk,
                buah,
                sayur,
                tidur,
                spnStres.getSelectedItem().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")) {

                            startActivity(new Intent(getApplicationContext(), MainUserActivity.class));
                            Toast.makeText(QuisionerActivity.this, "Berhasil Menambah data", Toast.LENGTH_SHORT).show();

                        } else {

                            String msg = jsonObject.optString("msg");

                            Toast.makeText(QuisionerActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(QuisionerActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @SuppressLint("StaticFieldLeak")
    private void insertData(final Quesioner quesioner) {

        new AsyncTask<Void, Void, Long>() {
            @Override
            protected Long doInBackground(Void... voids) {
                // melakukan proses insert data
                long status = db.quesionerDAO().insertQuesioner(quesioner);
//                Toast.makeText(DetailActivity.this, ""+status, Toast.LENGTH_SHORT).show();
                return status;
            }

            @Override
            protected void onPostExecute(Long status) {
                Toast.makeText(QuisionerActivity.this, "Quesioner berhasil ditambahkan ", Toast.LENGTH_SHORT).show();
            }
        }.execute();
//        db.pesananDAO().insertPesanan(pesanan);
//        Toast.makeText(this, "Barang Berhasil ditambah", Toast.LENGTH_SHORT).show();
    }

    private void initView() {
        quisionerIvClose = findViewById(R.id.quisioner_iv_close);
        quisionerTvTitle = findViewById(R.id.quisioner_tv_title);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        edtTinggi = (EditText) findViewById(R.id.edt_tinggi);
        rgRokok1 = (RadioGroup) findViewById(R.id.rg_rokok1);
        quisionerRbYamerokok = (RadioButton) findViewById(R.id.quisioner_rb_yamerokok);
        quisionerRbTidakmerokok = (RadioButton) findViewById(R.id.quisioner_rb_tidakmerokok);
        rgRokok2 = (RadioGroup) findViewById(R.id.rg_rokok2);
        rbTerpaparAsapRokok = (RadioButton) findViewById(R.id.rb_terpapar_asap_rokok);
        rbTidakTerpaparAsap = (RadioButton) findViewById(R.id.rb_tidak_terpapar_asap);
        rgMinumAlkohol = (RadioGroup) findViewById(R.id.rg_minum_alkohol);
        rbMinumAlkohol = (RadioButton) findViewById(R.id.rb_minum_alkohol);
        rbTidakMinumAlkohol = (RadioButton) findViewById(R.id.rb_tidak_minum_alkohol);
        spnOlahraga = (Spinner) findViewById(R.id.spn_olahraga);
        spnMinumAir = (Spinner) findViewById(R.id.spn_minum_air);
        btnInput = (Button) findViewById(R.id.btn_input);
        quisionerTvTitle = (TextView) findViewById(R.id.quisioner_tv_title);
        edtBerat = (EditText) findViewById(R.id.edt_berat);
        edtTinggi = (EditText) findViewById(R.id.edt_tinggi);
        rgRokok1 = (RadioGroup) findViewById(R.id.rg_rokok1);
        quisionerRbYamerokok = (RadioButton) findViewById(R.id.quisioner_rb_yamerokok);
        quisionerRbTidakmerokok = (RadioButton) findViewById(R.id.quisioner_rb_tidakmerokok);
        rgRokok2 = (RadioGroup) findViewById(R.id.rg_rokok2);
        rbTerpaparAsapRokok = (RadioButton) findViewById(R.id.rb_terpapar_asap_rokok);
        rbTidakTerpaparAsap = (RadioButton) findViewById(R.id.rb_tidak_terpapar_asap);
        rgMinumAlkohol = (RadioGroup) findViewById(R.id.rg_minum_alkohol);
        rbMinumAlkohol = (RadioButton) findViewById(R.id.rb_minum_alkohol);
        rbTidakMinumAlkohol = (RadioButton) findViewById(R.id.rb_tidak_minum_alkohol);


        spnOlahraga = (Spinner) findViewById(R.id.spn_olahraga);
        spnMinumAir = (Spinner) findViewById(R.id.spn_minum_air);


        spnAktifitasHarian = (Spinner) findViewById(R.id.spn_aktifitas_harian);
        spnStres = (Spinner) findViewById(R.id.spn_stres);
        rbNasi = (RadioButton) findViewById(R.id.rb_nasi);
        edtNasi = (EditText) findViewById(R.id.edt_nasi);
        rbBiskuit = (RadioButton) findViewById(R.id.rb_biskuit);
        edtBiskuit = (EditText) findViewById(R.id.edt_biskuit);
        rbRoti = (RadioButton) findViewById(R.id.rb_roti);
        edtRoti = (EditText) findViewById(R.id.edt_roti);
        rbJagung = (RadioButton) findViewById(R.id.rb_jagung);
        edtJagung = (EditText) findViewById(R.id.edt_jagung);
        rbKentang = (RadioButton) findViewById(R.id.rb_kentang);
        edtKentang = (EditText) findViewById(R.id.edt_kentang);
        rbSingkong = (RadioButton) findViewById(R.id.rb_singkong);
        edtSingkong = (EditText) findViewById(R.id.edt_singkong);
        rbMi = (RadioButton) findViewById(R.id.rb_mi);
        edtMi = (EditText) findViewById(R.id.edt_mi);
        rbAyam = (RadioButton) findViewById(R.id.rb_ayam);
        edtAyam = (EditText) findViewById(R.id.edt_ayam);
        rbIkan = (RadioButton) findViewById(R.id.rb_ikan);
        edtIkan = (EditText) findViewById(R.id.edt_ikan);
        rbDaging = (RadioButton) findViewById(R.id.rb_daging);
        edtDaging = (EditText) findViewById(R.id.edt_daging);
        rbTelur = (RadioButton) findViewById(R.id.rb_telur);
        edtTelur = (EditText) findViewById(R.id.edt_telur);
        rbUdang = (RadioButton) findViewById(R.id.rb_udang);
        edtUdang = (EditText) findViewById(R.id.edt_udang);
        rbBakso = (RadioButton) findViewById(R.id.rb_bakso);
        edtBakso = (EditText) findViewById(R.id.edt_bakso);
        rbTempe = (RadioButton) findViewById(R.id.rb_tempe);
        edtTempe = (EditText) findViewById(R.id.edt_tempe);
        rbTahu = (RadioButton) findViewById(R.id.rb_tahu);
        edtTahu = (EditText) findViewById(R.id.edt_tahu);
        rbSemangka = (RadioButton) findViewById(R.id.rb_semangka);
        edtSemangka = (EditText) findViewById(R.id.edt_semangka);
        rbJeruk = (RadioButton) findViewById(R.id.rb_jeruk);
        edtJeruk = (EditText) findViewById(R.id.edt_jeruk);
        rbMelon = (RadioButton) findViewById(R.id.rb_melon);
        edtMelon = (EditText) findViewById(R.id.edt_melon);
        rbPisang = (RadioButton) findViewById(R.id.rb_pisang);
        edtPisang = (EditText) findViewById(R.id.edt_pisang);
        rbPepaya = (RadioButton) findViewById(R.id.rb_pepaya);
        edtPepaya = (EditText) findViewById(R.id.edt_pepaya);
        rbMangga = (RadioButton) findViewById(R.id.rb_mangga);
        edtMangga = (EditText) findViewById(R.id.edt_mangga);
        rbApel = (RadioButton) findViewById(R.id.rb_apel);
        edtApel = (EditText) findViewById(R.id.edt_apel);
        rbAlpukat = (RadioButton) findViewById(R.id.rb_alpukat);
        edtAlpukat = (EditText) findViewById(R.id.edt_alpukat);
        rbNanas = (RadioButton) findViewById(R.id.rb_nanas);
        edtNanas = (EditText) findViewById(R.id.edt_nanas);
        rbSop = (RadioButton) findViewById(R.id.rb_sop);
        edtSop = (EditText) findViewById(R.id.edt_sop);
        rbBening = (RadioButton) findViewById(R.id.rb_bening);
        edtBening = (EditText) findViewById(R.id.edt_bening);
        rbLodeh = (RadioButton) findViewById(R.id.rb_lodeh);
        edtLodeh = (EditText) findViewById(R.id.edt_lodeh);
        rbTumis = (RadioButton) findViewById(R.id.rb_tumis);
        edtTumis = (EditText) findViewById(R.id.edt_tumis);
        rbGudeng = (RadioButton) findViewById(R.id.rb_gudeng);
        edtGudeng = (EditText) findViewById(R.id.edt_gudeng);
        rgTidur = (RadioGroup) findViewById(R.id.rg_tidur);
        rbTidur1 = (RadioButton) findViewById(R.id.rb_tidur1);
        rbTidur2 = (RadioButton) findViewById(R.id.rb_tidur2);
    }
}
