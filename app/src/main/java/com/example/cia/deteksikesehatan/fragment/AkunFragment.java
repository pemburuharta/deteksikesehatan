package com.example.cia.deteksikesehatan.fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.MainActivity;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.activity.BiodataActivity;
import com.example.cia.deteksikesehatan.activity.LihatQuesionerActivity;
import com.example.cia.deteksikesehatan.activity.ListUserActivity;
import com.example.cia.deteksikesehatan.activity.QuisionerActivity;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AkunFragment extends Fragment {


    private CircleImageView akunIvAvatar;
    private TextView akunTvUsername;
    private LinearLayout akunDivIsibiodata;
    SharedPref pref;
    private LinearLayout btnKeluar;
    private LinearLayout lyHarian;
    private LinearLayout lyHistori;
    private LinearLayout btnDetial;

    public AkunFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        initView(view);

        pref = new SharedPref(getActivity());

        akunDivIsibiodata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), BiodataActivity.class));
            }
        });

        lyHarian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), QuisionerActivity.class));
            }
        });

        lyHistori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getActivity(), HistoryUserActivity.class));
                Intent intent = new Intent(getActivity(), LihatQuesionerActivity.class);
                intent.putExtra("id_user", pref.getIdUser());
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                startActivity(new Intent(getActivity(), LihatQuesionerActivity.class));
            }
        });


        akunTvUsername.setText("" + pref.getFullname());
        btnKeluar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setCancelable(true);
                dialog.setMessage("Apakah anda yakin ingin keluar ?");
                dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        pref.savePrefBoolean(SharedPref.STATUS_LOGIN, false);
                        Intent startMain = new Intent(getActivity(), MainActivity.class);
                        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(startMain);
                        getActivity().finish();
                    }
                });

                dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog alert = dialog.create();
                alert.show();
            }
        });

        btnDetial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ListUserActivity.class);
                intent.putExtra("data","user");
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        return view;
    }

    private void initView(View view) {
        akunIvAvatar = view.findViewById(R.id.akun_iv_avatar);
        akunTvUsername = view.findViewById(R.id.akun_tv_username);
        akunDivIsibiodata = view.findViewById(R.id.akun_div_isibiodata);
        btnKeluar = (LinearLayout) view.findViewById(R.id.btn_keluar);
        lyHarian = (LinearLayout) view.findViewById(R.id.ly_harian);
        lyHistori = (LinearLayout) view.findViewById(R.id.ly_histori);
        btnDetial = (LinearLayout) view.findViewById(R.id.btn_detial);
    }
}
