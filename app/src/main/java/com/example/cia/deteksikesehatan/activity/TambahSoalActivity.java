package com.example.cia.deteksikesehatan.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TambahSoalActivity extends AppCompatActivity {

    private LinearLayout addsoalDivSpinnerkategori;
    private Spinner addsoalSpinnerkategori;
    private LinearLayout addsoalDivSpinnermateri;
    private Spinner addsoalSpinnermateri;
    private EditText addsoalEdtSoal;
    private EditText addsoalEdtA;
    private EditText addsoalEdtB;
    private EditText addsoalEdtC;
    private EditText addsoalEdtD;
    private EditText addsoalEdtJawaban;
    private LinearLayout addberitaDivSimpan;

    ArrayList<String> listKategori = new ArrayList<>();
    ArrayList<String> listMateri = new ArrayList<>();

    SharedPref pref;
    ProgressDialog pd;

    String ID_KATEGORI;
    String ID_MATERI;
    String STATUS;
    private ImageView addsoalIvClose;
    private TextView addsoalTvActionbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_soal);
        getSupportActionBar().hide();
        initView();

        addsoalIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

        getKategori();

        addberitaDivSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addSoal();
            }
        });

    }

    private void getKategori() {
        listKategori.removeAll(listKategori);
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getKategori().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        final JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("list data news kosong")) {

                        } else {
                            final JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String nama_kategori = jsonObject1.optString("TK_NAMA");
                                listKategori.add(nama_kategori);
                            }

                            ArrayAdapter<String> adapterKategori = new ArrayAdapter<String>(TambahSoalActivity.this, android.R.layout.simple_list_item_1, listKategori);
                            adapterKategori.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            addsoalSpinnerkategori.setAdapter(adapterKategori);
                            addsoalSpinnerkategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    ID_KATEGORI = jsonArray.optJSONObject(i).optString("TK_BIGID");
                                    addsoalDivSpinnermateri.setVisibility(View.VISIBLE);
                                    getMateri(ID_KATEGORI);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahSoalActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void getMateri(String id_kategori) {
        listMateri.removeAll(listMateri);
        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.getMateri(id_kategori).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        final JSONObject jsonObject = new JSONObject(response.body().string());
                        String msg = jsonObject.optString("msg");
                        if (msg.equals("list data news kosong")) {

                        } else {
                            final JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                final String nama_kategori = jsonObject1.optString("TS_NAMA");
                                listMateri.add(nama_kategori);
                            }

                            ArrayAdapter<String> adapterKategori = new ArrayAdapter<String>(TambahSoalActivity.this, android.R.layout.simple_list_item_1, listMateri);
                            adapterKategori.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                            addsoalSpinnermateri.setAdapter(adapterKategori);
                            addsoalSpinnermateri.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                    ID_MATERI = jsonArray.optJSONObject(i).optString("TS_BIGID");
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> adapterView) {

                                }
                            });

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TambahSoalActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void addSoal() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.addSoal(ID_KATEGORI,
                ID_MATERI,
                addsoalEdtSoal.getText().toString(),
                addsoalEdtA.getText().toString(),
                addsoalEdtB.getText().toString(),
                addsoalEdtC.getText().toString(),
                addsoalEdtD.getText().toString(),
                addsoalEdtJawaban.getText().toString(),
                pref.getIdUser()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pd.dismiss();
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");

                        if (error.equals("false")) {
                            Intent intent = new Intent(getApplicationContext(), AddActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            Toast.makeText(TambahSoalActivity.this, "Berhasil Menambahkan Soal", Toast.LENGTH_SHORT).show();
                        } else {

                            String msg = jsonObject.optString("msg");
                            Toast.makeText(TambahSoalActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(getApplicationContext(), "Periksa Koneksi Anda...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initView() {
        addsoalDivSpinnerkategori = findViewById(R.id.addsoal_div_spinnerkategori);
        addsoalSpinnerkategori = findViewById(R.id.addsoal_spinnerkategori);
        addsoalDivSpinnermateri = findViewById(R.id.addsoal_div_spinnermateri);
        addsoalSpinnermateri = findViewById(R.id.addsoal_spinnermateri);
        addsoalEdtSoal = findViewById(R.id.addsoal_edt_soal);
        addsoalEdtA = findViewById(R.id.addsoal_edt_a);
        addsoalEdtB = findViewById(R.id.addsoal_edt_b);
        addsoalEdtC = findViewById(R.id.addsoal_edt_c);
        addsoalEdtD = findViewById(R.id.addsoal_edt_d);
        addsoalEdtJawaban = findViewById(R.id.addsoal_edt_jawaban);
        addberitaDivSimpan = findViewById(R.id.addberita_div_simpan);
        addsoalIvClose = findViewById(R.id.addsoal_iv_close);
        addsoalTvActionbar = findViewById(R.id.addsoal_tv_actionbar);
    }
}
