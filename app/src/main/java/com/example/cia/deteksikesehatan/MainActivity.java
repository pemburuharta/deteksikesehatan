package com.example.cia.deteksikesehatan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;
import com.example.cia.deteksikesehatan.activity.MainAdminActivity;
import com.example.cia.deteksikesehatan.activity.MainUserActivity;
import com.example.cia.deteksikesehatan.activity.RegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    private ScrollView scroll;
    private LinearLayout ly;
    private EditText edtEmail;
    private EditText edtPassword;
    private Button btnLogin;
    private TextView btnTidakPunyaAkun;

    SharedPref pref;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);
        pd = new ProgressDialog(this);

        if (pref.getStatusLogin()) {
            if (pref.getRule().equals("ADMIN")){
                startActivity(new Intent(getApplicationContext(), MainAdminActivity.class));
                finish();
            } else {
                startActivity(new Intent(getApplicationContext(), MainUserActivity.class));
                finish();
            }
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnTidakPunyaAkun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

    }

    private void login() {

        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.login(edtEmail.getText().toString(), edtPassword.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        if (error.equals("false")){

                            String id_user = jsonObject.getString("bigId");
                            String name = jsonObject.getString("name");
                            String fullname = jsonObject.getString("fullname");
                            String email = jsonObject.getString("email");
                            String noTelp= jsonObject.getString("noTelp");
                            String status_user = jsonObject.getString("status_user");
                            String avatar= jsonObject.getString("avatar");
                            String rule= jsonObject.getString("rule");

                            pref.savePrefBoolean(SharedPref.STATUS_LOGIN, true);
                            pref.savePrefString(SharedPref.ID_USER, id_user);
                            pref.savePrefString(SharedPref.NAME, name);
                            pref.savePrefString(SharedPref.FULLNAME, fullname);
                            pref.savePrefString(SharedPref.EMAIL, email);
                            pref.savePrefString(SharedPref.NOTELP, noTelp);
                            pref.savePrefString(SharedPref.STATUS_USER, status_user);
                            pref.savePrefString(SharedPref.AVATAR, avatar);
                            pref.savePrefString(SharedPref.RULE, rule);

                            if (rule.equals("ADMIN")){
                                Intent intent = new Intent(getApplicationContext(), MainAdminActivity.class);
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(getApplicationContext(), MainUserActivity.class);
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                finish();
                            }

                        }else {
                            String msg = jsonObject.optString("msg");

                            Toast.makeText(MainActivity.this, ""+msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else {
                    Toast.makeText(MainActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(MainActivity.this, "Periksa Koneksi Anda ...", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(true);
        dialog.setMessage("Apakah anda yakin ingin keluar ?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                Intent startMain = new Intent(Intent.ACTION_MAIN);
//                startMain.addCategory(Intent.CATEGORY_HOME);
//                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(startMain);
                finish();
            }
        });

        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alert = dialog.create();
        alert.show();
    }

    private void initView() {
        img = (ImageView) findViewById(R.id.img);
        scroll = (ScrollView) findViewById(R.id.scroll);
        ly = (LinearLayout) findViewById(R.id.ly);
        edtEmail = (EditText) findViewById(R.id.edt_email);
        edtPassword = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btn_login);
        btnTidakPunyaAkun = (TextView) findViewById(R.id.btn_tidak_punya_akun);
    }
}
