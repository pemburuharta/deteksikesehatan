package com.example.cia.deteksikesehatan.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.model.ListUserModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailUserActivity extends AppCompatActivity {

    private ImageView detailuserIvClose;
    private TextView detailuserTvActionbar;
    private CircleImageView detailuserIvAvatar;
    private TextView detailuserTvUsernam;
    private TextView detailuserTvEmail;
    private TextView detailuserTvTelpon;
    private TextView detailuserTvSekolah;
    private TextView detailuserTvKelas;
    private TextView detailuserTvKota;
    private TextView detailuserTvKecamatan;
    private TextView detailuserTvKelurahan;
    private TextView detailuserTvRtrw;
    private TextView detailuserTvStudiayah;
    private TextView detailuserTvWorkayah;
    private TextView detailuserTvStudiibu;
    private TextView detailuserTvWorkibu;
    private TextView detailuserTvIncome;
    private TextView detailuserTvMonitoring;

    ArrayList<ListUserModel> user = new ArrayList<>();
    int posisi;
    SharedPref pref;
    private TextView tvKegiatan;
    private TextView tvAplikasi;
    private TextView tvPencegahan;
    private TextView tvInformasi;
    private TextView tvOrangRumah;
    private TextView tvPeriksa;
    private TextView tvRokok;
    private TextView tvRokokPasif;
    private TextView tvAlkohol;
    private TextView tvOlahraga;
    private TextView tvMakan;
    private TextView tvMinum;
    private TextView tvIstirahat;
    private TextView tvStres;
    private TextView tvBerat;
    private TextView tvTinggi;
    private TextView tvBeratIdeal;
    private TextView tvImt;
    private TextView tvLingkar;
    private TextView tvSistolik;
    private TextView tvDiastolik;
    private TextView tvGlukosa;
    private LinearLayout lyBerat;
    private LinearLayout lyImt;
    private LinearLayout lyLingkar;
    private LinearLayout lyTekDarah;
    private LinearLayout lyGlukosa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);
        getSupportActionBar().hide();
        initView();

        pref = new SharedPref(this);

        if (pref.getRule().equals("ADMIN")) {
            detailuserTvMonitoring.setVisibility(View.VISIBLE);
        } else {
            detailuserTvMonitoring.setVisibility(View.GONE);
        }

        detailuserIvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        user = getIntent().getParcelableArrayListExtra("model");
        posisi = Integer.parseInt(getIntent().getStringExtra("posisi"));

        detailuserTvMonitoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(getApplicationContext(), HistoryUserActivity.class);
//                intent.putExtra("id_user", user.get(posisi).getTUBIGID());
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);

                Intent intent = new Intent(getApplicationContext(), LihatQuesionerActivity.class);
                intent.putExtra("id_user", user.get(posisi).getTUBIGID());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        if (user.get(posisi).getTUAVATAR().equals("")) {

            detailuserIvAvatar.setImageResource(R.drawable.ic_profil);

        } else {

            Picasso.with(this)
                    .load("http://dk.mitraredex.com/assets/gambar/" + user.get(posisi).getTUAVATAR())
                    .into(detailuserIvAvatar);

        }

        detailuserTvActionbar.setText(user.get(posisi).getTUFULLNAME());
        detailuserTvUsernam.setText(user.get(posisi).getTUNAME() + " | " + user.get(posisi).getTUUMUR() + " Tahun");
        detailuserTvEmail.setText(user.get(posisi).getTUEMAIL());
        detailuserTvTelpon.setText(user.get(posisi).getTUTELP());

        detailuserTvSekolah.setText(user.get(posisi).getTUDASALSEKOLAH());
        detailuserTvKelas.setText(user.get(posisi).getTUDKELAS());

        detailuserTvKota.setText(user.get(posisi).getTUDKOTA());
        detailuserTvKecamatan.setText(user.get(posisi).getTUDKECAMATAN());
        detailuserTvKelurahan.setText(user.get(posisi).getTUDKELURAHAN());
        detailuserTvRtrw.setText(user.get(posisi).getTUDRT() + " / " + user.get(posisi).getTUDRW());

        if (user.get(posisi).getTUDINCOME().equals("1")) {
            detailuserTvIncome.setText("Tidak Ada");
        } else if (user.get(posisi).getTUDINCOME().equals("2")) {
            detailuserTvIncome.setText("&lt; Rp.2.400.000,-");
        } else if (user.get(posisi).getTUDINCOME().equals("3")) {
            detailuserTvIncome.setText("&gt; Rp.2.400.000,-");
        }

        if (user.get(posisi).getTUDWORKAYAH().equals("1")) {
            detailuserTvWorkayah.setText("Tidak Bekerja");
        } else if (user.get(posisi).getTUDWORKAYAH().equals("2")) {
            detailuserTvWorkayah.setText("PNS/TNI/POLRI");
        } else if (user.get(posisi).getTUDWORKAYAH().equals("3")) {
            detailuserTvWorkayah.setText("Pegawai/Swasta");
        } else if (user.get(posisi).getTUDWORKAYAH().equals("4")) {
            detailuserTvWorkayah.setText("Wiraswasta/Pedagang");
        }

        if (user.get(posisi).getTUDWORKIBU().equals("1")) {
            detailuserTvWorkibu.setText("Tidak Bekerja");
        } else if (user.get(posisi).getTUDWORKIBU().equals("2")) {
            detailuserTvWorkibu.setText("PNS/TNI/POLRI");
        } else if (user.get(posisi).getTUDWORKIBU().equals("3")) {
            detailuserTvWorkibu.setText("Pegawai/Swasta");
        } else if (user.get(posisi).getTUDWORKIBU().equals("4")) {
            detailuserTvWorkibu.setText("Wiraswasta/Pedagang");
        }

        if (user.get(posisi).getTUDSTUDIAYAH().equals("1")) {
            detailuserTvStudiayah.setText("Tidak Tamat SD");
        } else if (user.get(posisi).getTUDSTUDIAYAH().equals("2")) {
            detailuserTvStudiayah.setText("SD");
        } else if (user.get(posisi).getTUDSTUDIAYAH().equals("3")) {
            detailuserTvStudiayah.setText("SMP");
        } else if (user.get(posisi).getTUDSTUDIAYAH().equals("4")) {
            detailuserTvStudiayah.setText("SMA");
        } else if (user.get(posisi).getTUDSTUDIAYAH().equals("5")) {
            detailuserTvStudiayah.setText("SMA");
        }

        if (user.get(posisi).getTUDSTUDIIBU().equals("1")) {
            detailuserTvStudiibu.setText("Tidak Tamat SD");
        } else if (user.get(posisi).getTUDSTUDIIBU().equals("2")) {
            detailuserTvStudiibu.setText("SD");
        } else if (user.get(posisi).getTUDSTUDIIBU().equals("3")) {
            detailuserTvStudiibu.setText("SMP");
        } else if (user.get(posisi).getTUDSTUDIIBU().equals("4")) {
            detailuserTvStudiibu.setText("SMA");
        } else if (user.get(posisi).getTUDSTUDIIBU().equals("5")) {
            detailuserTvStudiibu.setText("SMA");
        }

        setData();

    }

    private void setData() {
        tvKegiatan.setText(user.get(posisi).getTDKEGIATANLUAR());
        tvAplikasi.setText(user.get(posisi).getTDGUNAAPPS());
        tvPencegahan.setText(user.get(posisi).getTDJOINEVENT());
        tvInformasi.setText(user.get(posisi).getTDINFORM());
        tvOrangRumah.setText(user.get(posisi).getTDTURUNAN());
        tvPeriksa.setText(user.get(posisi).getTDPERIKSA());
        tvRokok.setText(user.get(posisi).getTDPEROKOK() + " tahun");
        tvRokokPasif.setText(user.get(posisi).getTDLINGKUNGAN());
        tvAlkohol.setText(user.get(posisi).getTDMIRAS() + " tahun");
        tvOlahraga.setText(user.get(posisi).getTDOLAHRAGA());
        tvMakan.setText(user.get(posisi).getTDSNACK());
        tvMinum.setText(user.get(posisi).getTDSOFTDRINK());
        tvIstirahat.setText(user.get(posisi).getTDTIDUR());
        tvStres.setText(user.get(posisi).getTDDEKATKAN());
        tvBerat.setText(user.get(posisi).getTDBERATBDN() + " kg");
        tvTinggi.setText(user.get(posisi).getTDTINGGIBDN() + " cm");
        tvLingkar.setText(user.get(posisi).getTDLINGKARPRT());
        tvSistolik.setText(user.get(posisi).getTDSISTOLIK() + " mmHg");
        tvDiastolik.setText(user.get(posisi).getTDDISTOLIK() + " mmHg");
        tvGlukosa.setText(user.get(posisi).getTDGULADARAH() + " mg/dl");

        int berat = Integer.parseInt(user.get(posisi).getTDBERATBDN());
        int tingi = Integer.parseInt(user.get(posisi).getTDTINGGIBDN());

        double tingiD = Double.parseDouble(user.get(posisi).getTDTINGGIBDN());
        double beratD = Double.parseDouble(user.get(posisi).getTDBERATBDN());

        int beratI = (tingi - 100) - ((10 / 100) * (tingi - 100));
        String jk = user.get(posisi).getTUDJK();

        int lingkar = Integer.parseInt(user.get(posisi).getTDLINGKARPRT());

        tvBeratIdeal.setText("" + beratI);

        double tinggiDlmM = tingiD / 100;
        double imt = beratD / (tinggiDlmM * tinggiDlmM);

        if (berat > beratI){
            lyBerat.setBackgroundColor(Color.RED);
        }else if (berat < beratI){
            lyBerat.setBackgroundColor(Color.YELLOW);
        }else if (berat == beratI){
            lyBerat.setBackgroundColor(Color.GREEN);
        }

        if (imt >= 30){
            lyImt.setBackgroundColor(Color.MAGENTA);
        }else if (imt >= 25.0 && imt <= 29.9){
            lyImt.setBackgroundColor(Color.RED);
        }else if (imt >= 23.0 && imt <= 24.9){
            lyImt.setBackgroundColor(Color.YELLOW);
        }else if (imt >= 18.5 && imt <= 22.9){
            lyImt.setBackgroundColor(Color.GREEN);
        }else if (imt < 18.5){
            lyImt.setBackgroundColor(Color.GREEN);
        }

        if (jk.equals("0")){
            if (lingkar > 90 ){
                lyLingkar.setBackgroundColor(Color.RED);
            }else {
                lyLingkar.setBackgroundColor(Color.GREEN);
            }
        }else {
            if (lingkar > 80 ){
                lyLingkar.setBackgroundColor(Color.RED);
            }else {
                lyLingkar.setBackgroundColor(Color.GREEN);
            }
        }

        int sis = Integer.parseInt(user.get(posisi).getTDSISTOLIK());
        int dis = Integer.parseInt(user.get(posisi).getTDDISTOLIK());

        if (sis >= 160 && dis == 100){
            lyTekDarah.setBackgroundColor(Color.MAGENTA);
        }else if ((sis >= 140 && sis <= 159) && (dis >= 90 && dis <= 99)){
            lyTekDarah.setBackgroundColor(Color.RED);
        }else if ((sis >= 120 && sis <= 139) && (dis >= 80 && dis <= 89)){
            lyTekDarah.setBackgroundColor(Color.YELLOW);
        }else if ((sis >= 90 && sis <= 119) && (dis >= 60 && dis <= 79)){
            lyTekDarah.setBackgroundColor(Color.GREEN);
        }else if (sis < 90 && dis < 60){
            lyTekDarah.setBackgroundColor(Color.GREEN);
        }

        int gluk = Integer.parseInt(user.get(posisi).getTDGULADARAH());

        if (gluk >= 200 ){
            lyGlukosa.setBackgroundColor(Color.RED);
        }else if (gluk < 140){
            lyGlukosa.setBackgroundColor(Color.GREEN);
        }else {
            lyGlukosa.setBackgroundColor(Color.YELLOW);
        }

        tvImt.setText("" + imt);
    }

    private void initView() {
        detailuserIvClose = findViewById(R.id.detailuser_iv_close);
        detailuserTvActionbar = findViewById(R.id.detailuser_tv_actionbar);
        detailuserIvAvatar = findViewById(R.id.detailuser_iv_avatar);
        detailuserTvUsernam = findViewById(R.id.detailuser_tv_usernam);
        detailuserTvEmail = findViewById(R.id.detailuser_tv_email);
        detailuserTvTelpon = findViewById(R.id.detailuser_tv_telpon);
        detailuserTvSekolah = findViewById(R.id.detailuser_tv_sekolah);
        detailuserTvKelas = findViewById(R.id.detailuser_tv_kelas);
        detailuserTvKota = findViewById(R.id.detailuser_tv_kota);
        detailuserTvKecamatan = findViewById(R.id.detailuser_tv_kecamatan);
        detailuserTvKelurahan = findViewById(R.id.detailuser_tv_kelurahan);
        detailuserTvRtrw = findViewById(R.id.detailuser_tv_rtrw);
        detailuserTvStudiayah = findViewById(R.id.detailuser_tv_studiayah);
        detailuserTvWorkayah = findViewById(R.id.detailuser_tv_workayah);
        detailuserTvStudiibu = findViewById(R.id.detailuser_tv_studiibu);
        detailuserTvWorkibu = findViewById(R.id.detailuser_tv_workibu);
        detailuserTvIncome = findViewById(R.id.detailuser_tv_income);
        detailuserTvMonitoring = findViewById(R.id.detailuser_tv_monitoring);
        tvKegiatan = (TextView) findViewById(R.id.tv_kegiatan);
        tvAplikasi = (TextView) findViewById(R.id.tv_aplikasi);
        tvPencegahan = (TextView) findViewById(R.id.tv_pencegahan);
        tvInformasi = (TextView) findViewById(R.id.tv_informasi);
        tvOrangRumah = (TextView) findViewById(R.id.tv_orang_rumah);
        tvPeriksa = (TextView) findViewById(R.id.tv_periksa);
        tvRokok = (TextView) findViewById(R.id.tv_rokok);
        tvRokokPasif = (TextView) findViewById(R.id.tv_rokok_pasif);
        tvAlkohol = (TextView) findViewById(R.id.tv_alkohol);
        tvOlahraga = (TextView) findViewById(R.id.tv_olahraga);
        tvMakan = (TextView) findViewById(R.id.tv_makan);
        tvMinum = (TextView) findViewById(R.id.tv_minum);
        tvIstirahat = (TextView) findViewById(R.id.tv_istirahat);
        tvStres = (TextView) findViewById(R.id.tv_stres);
        tvBerat = (TextView) findViewById(R.id.tv_berat);
        tvTinggi = (TextView) findViewById(R.id.tv_tinggi);
        tvBeratIdeal = (TextView) findViewById(R.id.tv_berat_ideal);
        tvImt = (TextView) findViewById(R.id.tv_imt);
        tvLingkar = (TextView) findViewById(R.id.tv_lingkar);
        tvSistolik = (TextView) findViewById(R.id.tv_sistolik);
        tvDiastolik = (TextView) findViewById(R.id.tv_diastolik);
        tvGlukosa = (TextView) findViewById(R.id.tv_glukosa);
        lyBerat = (LinearLayout) findViewById(R.id.ly_berat);
        lyImt = (LinearLayout) findViewById(R.id.ly_imt);
        lyLingkar = (LinearLayout) findViewById(R.id.ly_lingkar);
        lyTekDarah = (LinearLayout) findViewById(R.id.ly_tek_darah);
        lyGlukosa = (LinearLayout) findViewById(R.id.ly_glukosa);
    }
}
