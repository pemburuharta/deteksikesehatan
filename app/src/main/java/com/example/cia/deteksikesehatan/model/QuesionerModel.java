package com.example.cia.deteksikesehatan.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuesionerModel implements Parcelable {
    @SerializedName("TQU_BIGID")
    @Expose
    private String tQUBIGID;
    @SerializedName("TQU_USERID")
    @Expose
    private String tQUUSERID;
    @SerializedName("TQU_JAWABAN")
    @Expose
    private String tQUJAWABAN;
    @SerializedName("TQU_TRUE")
    @Expose
    private String tQUTRUE;
    @SerializedName("TQU_KET")
    @Expose
    private String tQUKET;
    @SerializedName("TQU_NILAI")
    @Expose
    private String tQUNILAI;
    @SerializedName("TQU_PEROKOK")
    @Expose
    private String tQUPEROKOK;
    @SerializedName("TQU_ASAP_ROKOK")
    @Expose
    private String tQUASAPROKOK;
    @SerializedName("TQU_ALKOHOL")
    @Expose
    private String tQUALKOHOL;
    @SerializedName("TQU_AKTIFITAS")
    @Expose
    private String tQUAKTIFITAS;
    @SerializedName("TQU_OLAHRAGA")
    @Expose
    private String tQUOLAHRAGA;
    @SerializedName("TQU_MINUM")
    @Expose
    private String tQUMINUM;
    @SerializedName("TQU_MAKAN")
    @Expose
    private String tQUMAKAN;
    @SerializedName("TQU_LAUK")
    @Expose
    private String tQULAUK;
    @SerializedName("TQU_BUAH")
    @Expose
    private String tQUBUAH;
    @SerializedName("TQU_SAYUR")
    @Expose
    private String tQUSAYUR;
    @SerializedName("TQU_TIDUR")
    @Expose
    private String tQUTIDUR;
    @SerializedName("TQU_STRESS")
    @Expose
    private String tQUSTRESS;
    @SerializedName("TQU_TYPE")
    @Expose
    private String tQUTYPE;
    @SerializedName("TQU_TOKEN")
    @Expose
    private String tQUTOKEN;
    @SerializedName("TQU_STATUS")
    @Expose
    private String tQUSTATUS;
    @SerializedName("TQU_TANGGAL")
    @Expose
    private String tQUTANGGAL;
    @SerializedName("TQU_CREATED_BY")
    @Expose
    private String tQUCREATEDBY;
    @SerializedName("TQU_CREATED_AT")
    @Expose
    private String tQUCREATEDAT;

    protected QuesionerModel(Parcel in) {
        tQUBIGID = in.readString();
        tQUUSERID = in.readString();
        tQUJAWABAN = in.readString();
        tQUTRUE = in.readString();
        tQUKET = in.readString();
        tQUNILAI = in.readString();
        tQUPEROKOK = in.readString();
        tQUASAPROKOK = in.readString();
        tQUALKOHOL = in.readString();
        tQUAKTIFITAS = in.readString();
        tQUOLAHRAGA = in.readString();
        tQUMINUM = in.readString();
        tQUMAKAN = in.readString();
        tQULAUK = in.readString();
        tQUBUAH = in.readString();
        tQUSAYUR = in.readString();
        tQUTIDUR = in.readString();
        tQUSTRESS = in.readString();
        tQUTYPE = in.readString();
        tQUTOKEN = in.readString();
        tQUSTATUS = in.readString();
        tQUTANGGAL = in.readString();
        tQUCREATEDBY = in.readString();
        tQUCREATEDAT = in.readString();
    }

    public static final Creator<QuesionerModel> CREATOR = new Creator<QuesionerModel>() {
        @Override
        public QuesionerModel createFromParcel(Parcel in) {
            return new QuesionerModel(in);
        }

        @Override
        public QuesionerModel[] newArray(int size) {
            return new QuesionerModel[size];
        }
    };

    public String getTQUBIGID() {
        return tQUBIGID;
    }

    public void setTQUBIGID(String tQUBIGID) {
        this.tQUBIGID = tQUBIGID;
    }

    public String getTQUUSERID() {
        return tQUUSERID;
    }

    public void setTQUUSERID(String tQUUSERID) {
        this.tQUUSERID = tQUUSERID;
    }

    public String getTQUJAWABAN() {
        return tQUJAWABAN;
    }

    public void setTQUJAWABAN(String tQUJAWABAN) {
        this.tQUJAWABAN = tQUJAWABAN;
    }

    public String getTQUTRUE() {
        return tQUTRUE;
    }

    public void setTQUTRUE(String tQUTRUE) {
        this.tQUTRUE = tQUTRUE;
    }

    public String getTQUKET() {
        return tQUKET;
    }

    public void setTQUKET(String tQUKET) {
        this.tQUKET = tQUKET;
    }

    public String getTQUNILAI() {
        return tQUNILAI;
    }

    public void setTQUNILAI(String tQUNILAI) {
        this.tQUNILAI = tQUNILAI;
    }

    public String getTQUPEROKOK() {
        return tQUPEROKOK;
    }

    public void setTQUPEROKOK(String tQUPEROKOK) {
        this.tQUPEROKOK = tQUPEROKOK;
    }

    public String getTQUASAPROKOK() {
        return tQUASAPROKOK;
    }

    public void setTQUASAPROKOK(String tQUASAPROKOK) {
        this.tQUASAPROKOK = tQUASAPROKOK;
    }

    public String getTQUALKOHOL() {
        return tQUALKOHOL;
    }

    public void setTQUALKOHOL(String tQUALKOHOL) {
        this.tQUALKOHOL = tQUALKOHOL;
    }

    public String getTQUAKTIFITAS() {
        return tQUAKTIFITAS;
    }

    public void setTQUAKTIFITAS(String tQUAKTIFITAS) {
        this.tQUAKTIFITAS = tQUAKTIFITAS;
    }

    public String getTQUOLAHRAGA() {
        return tQUOLAHRAGA;
    }

    public void setTQUOLAHRAGA(String tQUOLAHRAGA) {
        this.tQUOLAHRAGA = tQUOLAHRAGA;
    }

    public String getTQUMINUM() {
        return tQUMINUM;
    }

    public void setTQUMINUM(String tQUMINUM) {
        this.tQUMINUM = tQUMINUM;
    }

    public String getTQUMAKAN() {
        return tQUMAKAN;
    }

    public void setTQUMAKAN(String tQUMAKAN) {
        this.tQUMAKAN = tQUMAKAN;
    }

    public String getTQULAUK() {
        return tQULAUK;
    }

    public void setTQULAUK(String tQULAUK) {
        this.tQULAUK = tQULAUK;
    }

    public String getTQUBUAH() {
        return tQUBUAH;
    }

    public void setTQUBUAH(String tQUBUAH) {
        this.tQUBUAH = tQUBUAH;
    }

    public String getTQUSAYUR() {
        return tQUSAYUR;
    }

    public void setTQUSAYUR(String tQUSAYUR) {
        this.tQUSAYUR = tQUSAYUR;
    }

    public String getTQUTIDUR() {
        return tQUTIDUR;
    }

    public void setTQUTIDUR(String tQUTIDUR) {
        this.tQUTIDUR = tQUTIDUR;
    }

    public String getTQUSTRESS() {
        return tQUSTRESS;
    }

    public void setTQUSTRESS(String tQUSTRESS) {
        this.tQUSTRESS = tQUSTRESS;
    }

    public String getTQUTYPE() {
        return tQUTYPE;
    }

    public void setTQUTYPE(String tQUTYPE) {
        this.tQUTYPE = tQUTYPE;
    }

    public String getTQUTOKEN() {
        return tQUTOKEN;
    }

    public void setTQUTOKEN(String tQUTOKEN) {
        this.tQUTOKEN = tQUTOKEN;
    }

    public String getTQUSTATUS() {
        return tQUSTATUS;
    }

    public void setTQUSTATUS(String tQUSTATUS) {
        this.tQUSTATUS = tQUSTATUS;
    }

    public String getTQUTANGGAL() {
        return tQUTANGGAL;
    }

    public void setTQUTANGGAL(String tQUTANGGAL) {
        this.tQUTANGGAL = tQUTANGGAL;
    }

    public String getTQUCREATEDBY() {
        return tQUCREATEDBY;
    }

    public void setTQUCREATEDBY(String tQUCREATEDBY) {
        this.tQUCREATEDBY = tQUCREATEDBY;
    }

    public String getTQUCREATEDAT() {
        return tQUCREATEDAT;
    }

    public void setTQUCREATEDAT(String tQUCREATEDAT) {
        this.tQUCREATEDAT = tQUCREATEDAT;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(tQUBIGID);
        parcel.writeString(tQUUSERID);
        parcel.writeString(tQUJAWABAN);
        parcel.writeString(tQUTRUE);
        parcel.writeString(tQUKET);
        parcel.writeString(tQUNILAI);
        parcel.writeString(tQUPEROKOK);
        parcel.writeString(tQUASAPROKOK);
        parcel.writeString(tQUALKOHOL);
        parcel.writeString(tQUAKTIFITAS);
        parcel.writeString(tQUOLAHRAGA);
        parcel.writeString(tQUMINUM);
        parcel.writeString(tQUMAKAN);
        parcel.writeString(tQULAUK);
        parcel.writeString(tQUBUAH);
        parcel.writeString(tQUSAYUR);
        parcel.writeString(tQUTIDUR);
        parcel.writeString(tQUSTRESS);
        parcel.writeString(tQUTYPE);
        parcel.writeString(tQUTOKEN);
        parcel.writeString(tQUSTATUS);
        parcel.writeString(tQUTANGGAL);
        parcel.writeString(tQUCREATEDBY);
        parcel.writeString(tQUCREATEDAT);
    }
}
