package com.example.cia.deteksikesehatan.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cia.deteksikesehatan.Helper.SharedPref;
import com.example.cia.deteksikesehatan.R;
import com.example.cia.deteksikesehatan.Retrofit.APIClient;
import com.example.cia.deteksikesehatan.Retrofit.APIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryUserActivity extends AppCompatActivity {

    private Spinner spnPilih;
    private LinearLayout lyHarian;
    private EditText edtH;
    private LinearLayout lyMinggu;
    private EditText edtMSatu;
    private EditText edtMDua;
    private LinearLayout lyBulan;
    private Spinner spnBulan;
    private Spinner spnTahun;
    private LinearLayout div;
    private Button btnCari;
    DatePickerDialog datePickerDialog, datePickerDialog2,datePickerDialog3;
    int mYear;
    int mMonth;
    int mDay;

    String USERID;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_user);
        initView();

        awal();

        btnCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spnPilih.getSelectedItem().toString().equals("Harian")) {
                    cariHari(true);
                } else if (spnPilih.getSelectedItem().toString().equals("Mingguan")) {
                   cariMinggu(true);
                } else {
                    cariBulan(true);
                }
            }
        });

        edtH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal1();
                datePickerDialog.show();
            }
        });

        edtMSatu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal2();
                datePickerDialog2.show();
            }
        });

        edtMDua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingTanggal3();
                datePickerDialog3.show();
            }
        });
    }

    private void awal() {
        ArrayList<String> listPilih = new ArrayList<>();
        listPilih.add("Harian");
        listPilih.add("Mingguan");
        listPilih.add("Bulanan");

        pref = new SharedPref(this);

        if (pref.getRule().equals("ADMIN")){
            USERID = getIntent().getStringExtra("id_user");
        } else {
            USERID = pref.getIdUser();
        }

        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(HistoryUserActivity.this, R.layout.support_simple_spinner_dropdown_item, listPilih);
        spnPilih.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnPilih.setAdapter(adp2);
        spnPilih.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spnPilih.getSelectedItem().toString().equals("Harian")) {
                    lyHarian.setVisibility(View.VISIBLE);
                    lyBulan.setVisibility(View.GONE);
                    lyMinggu.setVisibility(View.GONE);
                } else if (spnPilih.getSelectedItem().toString().equals("Mingguan")) {
                    lyHarian.setVisibility(View.GONE);
                    lyBulan.setVisibility(View.GONE);
                    lyMinggu.setVisibility(View.VISIBLE);
                } else {
                    lyHarian.setVisibility(View.GONE);
                    lyBulan.setVisibility(View.VISIBLE);
                    lyMinggu.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spnBulanan();
    }

    private void spnBulanan() {
        ArrayList<String> bulan = new ArrayList<>();
        for (int i = 1; i < 13; i++) {
            bulan.add("" + i);
        }

        ArrayList<String> tahun = new ArrayList<>();
        for (int i = 2019; i < 2030; i++) {
            tahun.add("" + i);
        }
        ArrayAdapter<String> adp2 = new ArrayAdapter<String>(HistoryUserActivity.this, R.layout.support_simple_spinner_dropdown_item, bulan);
        spnBulan.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnBulan.setAdapter(adp2);

        ArrayAdapter<String> adp = new ArrayAdapter<String>(HistoryUserActivity.this, R.layout.support_simple_spinner_dropdown_item, tahun);
        spnTahun.setDropDownHorizontalOffset(android.R.layout.simple_spinner_dropdown_item);
        spnTahun.setAdapter(adp);

    }

    private void cariHari(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.historiPerHari(edtH.getText().toString(),
                USERID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("msg");
                        if (error.equals("false")){
                            div.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                String id = jsonObject1.optString("TQU_USERID");
                                String jawaban = jsonObject1.optString("TQU_JAWABAN");

                                LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_history,null);

                                final TextView tvId = view.findViewById(R.id.tv_id);
                                final TextView tvKet = view.findViewById(R.id.tv_ket);

                                tvId.setText(id);
                                tvKet.setText(jawaban);

                                div.addView(view);

                            }
                        }else {
                            div.setVisibility(View.GONE);
                            Toast.makeText(HistoryUserActivity.this, ""+msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(HistoryUserActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cariMinggu(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.historiPerMinggu(edtMSatu.getText().toString(),
                edtMDua.getText().toString(),
                USERID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("msg");
                        if (error.equals("false")){
                            div.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                String id = jsonObject1.optString("TQU_USERID");
                                String jawaban = jsonObject1.optString("TQU_JAWABAN");

                                LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_history,null);

                                final TextView tvId = view.findViewById(R.id.tv_id);
                                final TextView tvKet = view.findViewById(R.id.tv_ket);

                                tvId.setText(id);
                                tvKet.setText(jawaban);

                                div.addView(view);

                            }
                        }else {
                            div.setVisibility(View.GONE);
                            Toast.makeText(HistoryUserActivity.this, ""+msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(HistoryUserActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cariBulan(boolean rm){
        if (rm) {
            if (div.getChildCount() > 0) div.removeAllViews();
        }
        final ProgressDialog pd =new ProgressDialog(this);
        pd.setTitle("Cari ...");
        pd.setCancelable(false);
        pd.show();

        APIService apiService = APIClient.getRetrofit().create(APIService.class);
        apiService.historiPerBulan(spnBulan.getSelectedItem().toString(),
                spnTahun.getSelectedItem().toString(),
                USERID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pd.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String error = jsonObject.optString("error");
                        String msg = jsonObject.optString("msg");
                        if (error.equals("false")){
                            div.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.optJSONArray("payload");
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject1 = jsonArray.optJSONObject(i);
                                String id = jsonObject1.optString("TQU_USERID");
                                String jawaban = jsonObject1.optString("TQU_JAWABAN");

                                LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View view = layoutInflater.inflate(R.layout.row_history,null);

                                final TextView tvId = view.findViewById(R.id.tv_id);
                                final TextView tvKet = view.findViewById(R.id.tv_ket);

                                tvId.setText(id);
                                tvKet.setText(jawaban);

                                div.addView(view);

                            }
                        }else {
                            div.setVisibility(View.GONE);
                            Toast.makeText(HistoryUserActivity.this, ""+msg, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pd.dismiss();
                Toast.makeText(HistoryUserActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void settingTanggal1() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtH.setText(sdf.format(c.getTime()));

                edtH.setFocusable(false);
                edtH.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void settingTanggal2() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog2 = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtMSatu.setText(sdf.format(c.getTime()));

                edtMSatu.setFocusable(false);
                edtMSatu.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void settingTanggal3() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog3 = new
                DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;

                GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

                edtMDua.setText(sdf.format(c.getTime()));

                edtMDua.setFocusable(false);
                edtMDua.setCursorVisible(false);
                //edtTanggal.setText(tanggal);

            }
        }, year, month, day);
    }

    private void initView() {
        spnPilih = (Spinner) findViewById(R.id.spn_pilih);
        lyHarian = (LinearLayout) findViewById(R.id.ly_harian);
        edtH = (EditText) findViewById(R.id.edt_h);
        lyMinggu = (LinearLayout) findViewById(R.id.ly_minggu);
        edtMSatu = (EditText) findViewById(R.id.edt_m_satu);
        edtMDua = (EditText) findViewById(R.id.edt_m_dua);
        lyBulan = (LinearLayout) findViewById(R.id.ly_bulan);
        spnBulan = (Spinner) findViewById(R.id.spn_bulan);
        spnTahun = (Spinner) findViewById(R.id.spn_tahun);
        div = (LinearLayout) findViewById(R.id.div);
        btnCari = (Button) findViewById(R.id.btn_cari);
    }
}
